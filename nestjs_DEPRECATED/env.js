const path = require('path');
const fs = require('fs');
const util = require('util');
const packageJson = require('../package.json');

const version = packageJson.version;
const prefix = 'export const envVars = ';

const env = process.argv[2]?.toLowerCase();
if (env == undefined) {
  console.error('no environment param provided!');
  return;
}

const envtspath = path.join(__dirname, 'src/environments/environment.ts');
if (fs.existsSync(envtspath)) {
  fs.unlinkSync(envtspath);
}

const envpath = path.join(__dirname, 'src/environments');
if (!fs.existsSync(envpath)) {
  fs.mkdirSync(envpath, { recursive: true });
}

const newenvtspath = path.join(
  __dirname,
  'src/environments/environment.' + env + '.ts',
);
if (!fs.existsSync(newenvtspath)) {
  console.error(newenvtspath + ' does not exist!');
  return;
}

const content = fs.readFileSync(newenvtspath)?.toString()?.replace(prefix, '');
let contentObj;
eval('contentObj=' + content);
contentObj.VERSION = 'v' + version;

const newFileContent = prefix + util.inspect(contentObj) + ';';
fs.writeFileSync(envtspath, newFileContent);

console.info(newenvtspath + ' copied!');
