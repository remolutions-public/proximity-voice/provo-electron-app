import { Controller, Get, Post, Body } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHealthcheck(): string {
    return this.appService.getHealthcheck();
  }

  @Post()
  receiveKeyInput(@Body() body: any): any {
    return this.appService.receiveKeyInput(body);
  }
}
