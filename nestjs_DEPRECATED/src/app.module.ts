import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { GlobalProvidersModule } from './providers/global-providers.module';
import { PlayerModule } from './routes/player/player.module';

const MODULES = [PlayerModule, GlobalProvidersModule];

const SERVICES = [AppService];

@Module({
  imports: [...MODULES],
  controllers: [AppController],
  providers: [...SERVICES],
})
export class AppModule {}
