const debounceRefs = {};

const refIsValid = (name: string): boolean => {
  if (
    !(name == undefined) &&
    name?.length &&
    !(debounceRefs[name] == undefined) &&
    !(debounceRefs[name].ref == undefined)
  ) {
    return true;
  }
  return false;
};

export const debounce = async (
  name: string,
  delay: number,
  timeoutCallback: any,
) => {
  if (refIsValid(name)) {
    clearTimeout(debounceRefs[name].ref);
  }
  if (!debounceRefs[name]) {
    debounceRefs[name] = {};
  }
  debounceRefs[name].ref = setTimeout(timeoutCallback, delay || 10);
  debounceRefs[name].startTime = new Date().valueOf();
  debounceRefs[name].delay = delay;
};

export const hasDebounceRef = (name: string): boolean => {
  if (refIsValid(name)) {
    const nowTime = new Date().valueOf();
    if (
      !(debounceRefs[name].startTime == undefined) &&
      !(debounceRefs[name].delay == undefined)
    ) {
      if (nowTime - debounceRefs[name].startTime <= debounceRefs[name].delay) {
        return true;
      }
    }
  }
  return false;
};

export const stopDebounceRef = (name: string) => {
  if (refIsValid(name)) {
    clearTimeout(debounceRefs[name].ref);
    delete debounceRefs[name];
  }
};
