export const encodeBase64 = (text: string) => {
  return Buffer.from(text).toString('base64');
};

export const decodeBase64 = (b64Encoded: string) => {
  return Buffer.from(b64Encoded, 'base64').toString();
};
