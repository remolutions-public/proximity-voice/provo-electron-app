import { Injectable } from '@nestjs/common';
import { IpcService } from '../ipc/ipc.service';
import { typeIsString } from '../../libraries/parser.lib';
import * as util from 'util';

@Injectable()
export class LoggerService {
  constructor(private readonly ipcService: IpcService) {}

  info(rendererid: string, msg: any) {
    if (!typeIsString(msg)) {
      msg = util.inspect(msg);
    }
    if (
      !this.ipcService.send('logging', rendererid, {
        type: 'info',
        msg: 'api: ' + msg,
      })
    ) {
      console.info(msg);
    }
  }

  log(rendererid: string, msg: any) {
    this.info(rendererid, msg);
  }

  debug(rendererid: string, msg: any) {
    this.info(rendererid, msg);
  }

  warn(rendererid: string, msg: any) {
    if (!typeIsString(msg)) {
      msg = util.inspect(msg);
    }
    if (
      !this.ipcService.send('logging', rendererid, {
        type: 'warn',
        msg: 'api: ' + msg,
      })
    ) {
      console.error(msg);
    }
  }

  error(rendererid: string, msg: any) {
    if (!typeIsString(msg)) {
      msg = util.inspect(msg);
    }
    if (
      !this.ipcService.send('logging', rendererid, {
        type: 'error',
        msg: 'api: ' + msg,
      })
    ) {
      console.error(msg);
    }
  }
}
