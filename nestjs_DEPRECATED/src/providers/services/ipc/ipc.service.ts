import { Injectable } from '@nestjs/common';
import { debounce } from '../../libraries/debounce.lib';
import { Subject } from 'rxjs';
import { encodeBase64, decodeBase64 } from '../../libraries/base64.lib';
import { typeIsJSON, typeIsString } from 'src/providers/libraries/parser.lib';

@Injectable()
export class IpcService {
  messageSub: Subject<any> = new Subject();

  offsetCalculatedRemotePlayers = {};
  voiceBaseSettingsCache = {};

  constructor() {
    this.initListener();
  }

  initListener() {
    debounce('initListener', 50, () => {
      if (process?.send == undefined) {
        this.initListener();
        return;
      }

      if (!(process?.send == undefined) && process?.connected) {
        process.on('message', (message: any) => {
          if (typeIsString(message)) {
            // sanitize string
            const msgStr = decodeBase64(message.replace('[d]:', ''));
            if (typeIsJSON(msgStr)) {
              const msgObj = JSON.parse(msgStr);
              switch (msgObj?.data?.type) {
                case 'apiProcessHealthcheck':
                  this.send('apiProcessHealthcheck', undefined, {});
                  return;
                case 'debugRemotePositions':
                  this.offsetCalculatedRemotePlayers = msgObj?.data?.data;
                  return;
                case 'voiceBaseSettingsCache':
                  this.voiceBaseSettingsCache = msgObj?.data;
                  return;
              }
              this.messageSub.next(msgObj);
            }
          }
        });
      }
    });
  }

  send(
    channel: string,
    rendererid: string,
    data: any,
    requestid?: string,
  ): boolean {
    if (!(process?.send == undefined) && process?.connected) {
      const payload =
        '[d]:' +
        encodeBase64(
          JSON.stringify({
            type: channel,
            requestid,
            rendererid,
            data,
            stamp: new Date().valueOf(),
          }),
        );
      process.send(payload);
      return true;
    }
  }
}
