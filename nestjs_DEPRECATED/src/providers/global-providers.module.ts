import { Module } from '@nestjs/common';
import { LoggerService } from './services/logger/logger.service';
import { IpcService } from './services/ipc/ipc.service';

const MODULES = [];

const SERVICES = [LoggerService, IpcService];

@Module({
  imports: [...MODULES],
  providers: [...SERVICES],
  exports: [...SERVICES],
})
export class GlobalProvidersModule {}
