import { Module } from '@nestjs/common';
import { GlobalProvidersModule } from '../../providers/global-providers.module';
import { PlayerController } from './player.controller';
import { PlayerService } from './player.service';

@Module({
  imports: [GlobalProvidersModule],
  controllers: [PlayerController],
  providers: [PlayerService],
})
export class PlayerModule {}
