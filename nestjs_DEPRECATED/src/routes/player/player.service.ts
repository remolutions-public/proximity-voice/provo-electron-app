import { Injectable } from '@nestjs/common';
import { IpcService } from '../../providers/services/ipc/ipc.service';
import { LoggerService } from '../../providers/services/logger/logger.service';

@Injectable()
export class PlayerService {
  constructor(
    private readonly ipcService: IpcService,
    private readonly logger: LoggerService,
  ) {}

  updatePlayerData(body: any): any {
    body.timestamp = new Date().valueOf();
    const isDebug = body.isDebug;
    const responseKey = body.responseKey;

    this.ipcService.send('PlayerData', body.clientid, body);

    if (isDebug) {
      const offsets = [];
      for (const playerid of Object.keys(
        this.ipcService.offsetCalculatedRemotePlayers,
      )) {
        offsets.push(this.ipcService.offsetCalculatedRemotePlayers[playerid]);
      }
      return {
        responseKey,
        offsets,
        voiceBaseSettings: this.ipcService.voiceBaseSettingsCache,
      };
    }
    return { message: 'healthy' };
  }
}
