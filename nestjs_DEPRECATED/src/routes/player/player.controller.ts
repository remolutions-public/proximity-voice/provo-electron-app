import { Controller, Get, Post, Body } from '@nestjs/common';
import { PlayerService } from './player.service';

@Controller('player')
export class PlayerController {
  constructor(private readonly playerService: PlayerService) {}

  @Post()
  updatePlayerData(@Body() body: any): any {
    return this.playerService.updatePlayerData(body);
  }
}
