import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const port = 3741;
  app.enableCors({
    origin: true,
    allowedHeaders: 'Authorization,Content-Type,Accept,secret',
    credentials: true,
    preflightContinue: false,
    methods: 'GET,PATCH,PUT,POST,DELETE,OPTIONS',
    optionsSuccessStatus: 204,
  });
  await app.listen(port);
}
bootstrap();
