import { Injectable } from '@nestjs/common';
import { IpcService } from './providers/services/ipc/ipc.service';

@Injectable()
export class AppService {
  constructor(private readonly ipcService: IpcService) {}

  getHealthcheck(): any {
    this.ipcService.send('healthcheck', undefined, undefined);
    return { message: 'healthy' };
  }

  receiveKeyInput(body: any): any {
    this.ipcService.send('downKeys', undefined, body);
    return;
  }
}
