# What is this?
An Electron app with coturn, nest.js and angular that utilizes WebRTC to connect ingame players to voice with each other depending on relative proximity to each other in the game. This also removes the load on game servers that already have a proximity voice solution implemented, since it won't need to go through the game server in a star architectural manner but instead it will connect players directly in a peer to peer fashion.


# Official Provo website
https://provo.remolutions.com/

[![alt text](https://provo.remolutions.com/assets/logos/Provo_64p.png "Provo")](https://provo.remolutions.com/)



# How to install?
```shell
# install npm packages
npm run install:all
npm i

```


# How to run the application in dev mode?
```shell
npm start

```



# How to build the application
- https://stackoverflow.com/questions/37113815/electron-builder-vs-electron-packager
- https://www.electron.build/
```shell
npm run package:staging

npm run package:prod

```


# How to create a release candidate
- update the version in the /package.json
- run in powershell, or vsc console...
```shell
npm run release:staging

npm run release:prod

```



# How to setup sourcemap upload for sentry
```shell
npx @sentry/wizard@latest -i sourcemaps
```



# Download links of prebuild Provo apps
- https://provo.remolutions.com/#/landing/downloads




# Expected Client Data and Format
## Localhost electron app routes:
- healthcheck route: GET http://localhost:3741/
- expected answer (status code: 200):
```json
{ "message": "healthy" }
```

## the provo app expects to receive a player data package every ~100ms
- player data route: POST http://localhost:3741/player
- attributes with a "?" are optional
  - the vehicle and rPlayers attributes are only reported if the player is based on/driving a vehicle in this example
  - the rPlayer (remote Players) attribute is used to solve the issue of missplaced panner nodes for fast moving vehicles. It bypasses/eleviates the need of relying on the api reported remote player positions. Those rPlayer positions get filtered out before this data package is reported to the api, in order to preserve network throughput.
- attributes in [] are dynamic and of type string
- expected data form game client:
```json
{
  "name": "PLAYER_NAME",
  "id": "STEAM_ID_OR_ANY_UNIQUE_ID",
  "realm": "SERVER_IDENTIFICATION_LIKE_SERVER_CELL",
  "pos": {
    "x": 40714.26171875,
    "y": 29280.259765625,
    "z": 108.1282730102539
  },
  "fVector": {
    "x": -0.25079697370529175,
    "y": -0.9680396318435669,
    "z": 0
  },
  "uVector": {
    "x": 0,
    "y": 0,
    "z": 0.9999999403953552
  },
  "serverid?": "localhost",
  "vehicle?": {
    "velocity": {
        "x": 0.00135511823464185,
        "y": -0.0027102364692837,
        "z": 0
    }
  },
  "rPlayers?": {
    "[STEAM_ID_OR_ANY_UNIQUE_ID]": {
      "pos": {
        "x": 40714.26171875,
        "y": 29280.259765625,
        "z": 108.1282730102539
      },
      "fVector": {
        "x": -0.25079697370529175,
        "y": -0.9680396318435669,
        "z": 0
      }
    }
  }
}
```
- expected answer for player data package (status code: 200):
```json
{}
```

## Keypress package
- keypress route: POST http://localhost:3741/
- it should send a pressed=true package for a key press regularly and one false if released
- key "reconnect" does not need a pressed attribute
- expected keypress package form game client:
```json
{
    "key": "talk|whisper|yell|reconnect",
    "pressed": true|false
}
```
  - expected answer for player keypress package (status code: 200):
```json
{}
```


## Discord Server
- https://discord.gg/NJTFTgNvzG



## Donation/Support
If you like my work and want to support further development or just to spend me a coffee please

[![alt text](https://i.imgur.com/Y0XkUcd.png "Paypal $")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=S3WQNNSVY8VAL)

[![alt text](https://i.imgur.com/xezX26q.png "Paypal €")](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=VQRPA46YADD9J)



# Darkside ingame debugging
- script command for darkside mode to display talky spheres
```
admincheat scriptcommand DSRPTalkySpheres

```

