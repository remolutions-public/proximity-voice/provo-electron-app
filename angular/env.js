const path = require("path");
const fs = require("fs");
const util = require("util");

const env = process.argv[2]?.toLowerCase();
if (env == undefined) {
  console.error("no environment param provided!");
  return;
}

const envtspath = path.join(
  __dirname,
  "src/environments/environment." + env + ".ts"
);
if (fs.existsSync(envtspath)) {
  fs.unlinkSync(envtspath);
}

const envpath = path.join(__dirname, "src/environments");
if (!fs.existsSync(envpath)) {
  throw new Error(
    "your environment files are not at the expected path: " + envpath
  );
}

const envtemplatefile = path.join(
  __dirname,
  "src/environments/environment." + env + ".TEMPLATE.ts"
);
if (!fs.existsSync(envtemplatefile)) {
  throw new Error("environment file " + envtemplatefile + " does not exist");
}

const dotenvfile = path.join(__dirname, "../.env." + env);
if (!fs.existsSync(dotenvfile)) {
  throw new Error(dotenvfile + " file not found in the project root");
}

const prefix = "export const environment = ";
const templateFileContent = fs
  .readFileSync(envtemplatefile)
  ?.toString()
  ?.replace(prefix, "");
let envFileContentObj;
eval("envFileContentObj=" + templateFileContent);

const dotEnvFileContent = fs
  .readFileSync(dotenvfile)
  ?.toString()
  ?.replace(/\r/g, "")
  ?.split("\n");

const envVarMapping = {
  GUARD_SECRET: "guardSecret"
};

for (const entry of dotEnvFileContent) {
  for (const varName of Object.keys(envVarMapping)) {
    if (entry.split('=')[0] === varName) {
      const attrName = envVarMapping[varName];
      envFileContentObj[attrName] = entry.split('=').slice(1).join('=');
      break;
    }
  }
}



const newFileContent = prefix + util.inspect(envFileContentObj) + ';';
fs.writeFileSync(envtspath, newFileContent);

console.info(envtspath + " copied!");
