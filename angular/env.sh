#!/bin/sh

BASE_PATH=$(dirname \"$0\" | tr -d '"')

if [ -z "$1" ]; then
  ENVIRONMENT="staging"
else
  ENVIRONMENT=$1
fi


echo "ENVIRONMENT: $ENVIRONMENT"
if [ -f .env.$ENVIRONMENT ]
then
  export $(cat $BASE_PATH/../.env.$ENVIRONMENT | xargs)
fi


node env.js $ENVIRONMENT
ng build --configuration $ENVIRONMENT
npm run sentry:sourcemaps
