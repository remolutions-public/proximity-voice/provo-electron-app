// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  staging: false,

  backendName: 'DarksideRP',
  backendUrl: 'http://localhost:3201',
  guardSecret: 'testing',

  updateServer: 'https://provo.remolutions.com',
  provoApis: ['https://provo.remolutions.com', 'http://localhost:3301'],

  localClientApi: 'http://localhost:3741',
  serverId: 'localhost',
  storageSalt: 'NAINFsVC65FC1bhV',
  debug1: {
    name: 'Debug Nomad',
    id: 'debug1',
    pos: {
      x: -1004.2,
      y: -12750.0,
      z: 108.346,
    },
    fVector: {
      x: 0.9999303817749023,
      y: -0.011803830042481422,
      z: 0,
    },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: 'assets/sounds/brown.mp3',
  },
  debug2: {
    name: 'Debug Tester',
    id: 'debug2',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: 'assets/sounds/sin250hz-10dB.mp3',
  },
  debug3: {
    name: 'Debug Tester3',
    id: 'debug3',
    pos: { x: 24200, y: 27650, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: 'assets/sounds/brown.mp3',
  },
  debug4: {
    name: 'Debug Tester4',
    id: 'debug4',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug5: {
    name: 'Debug Tester5',
    id: 'debug5',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug6: {
    name: 'Debug Tester6',
    id: 'debug6',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug7: {
    name: 'Debug Tester7',
    id: 'debug7',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug8: {
    name: 'Debug Tester8',
    id: 'debug8',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug9: {
    name: 'Debug Tester9',
    id: 'debug9',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
  debug10: {
    name: 'Debug Tester10',
    id: 'debug10',
    pos: { x: 40310, y: 25660, z: 108.1282730102539 },
    fVector: { x: -0.3831726312637329, y: -0.9236767888069153, z: 0 },
    uVector: { x: 0, y: 0, z: 1 },
    realm: 'DK',
    serverid: 'localhost',
    type: 'debug',
    audio: '',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
