export const environment = {
  production: true,
  staging: false,
  backendName: 'DarksideRP',
  backendUrl: 'https://provo.darkside-cluster.com',
  guardSecret: 'PLACEHOLDER',
  updateServer: 'https://provo.remolutions.com',
  provoApis: ['https://provo.remolutions.com'],
  localClientApi: 'http://localhost:3741',
  serverId: 'provo',
  storageSalt: 'tD8tV8rmoCS3wx8Q',
  debug1: {},
  debug2: {}
};
