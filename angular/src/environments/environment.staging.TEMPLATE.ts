export const environment = {
  production: false,
  staging: true,
  backendName: 'DarksideRP',
  backendUrl: 'https://provo-staging.darkside-cluster.com',
  guardSecret: 'PLACEHOLDER',
  updateServer: 'https://provo.remolutions.com',
  provoApis: ['https://provo.remolutions.com'],
  localClientApi: 'http://localhost:3741',
  serverId: 'provo',
  storageSalt: 'beHKI6Y3mcSPcTqw',
  debug1: {},
  debug2: {}
};
