export enum WebRTCRole {
  HOST = 'host',
  CLIENT = 'client',
}
