export enum WebRTCState {
  CALLING = 'calling',
  CONNECTING = 'connecting',
  CONNECTED = 'connected',
  DISCONNECTED = 'disconnected',
  TIMEOUT = 'timeout',
  CANCELED = 'canceled',
  FAILED = 'failed',
  CLOSED = 'closed',
}
