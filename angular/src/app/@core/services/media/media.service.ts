import { Injectable } from '@angular/core';
import { Player } from '../../models/player.model';
import { WebRTCState } from '../../enums/webrtc-state.enum';
import { debounce } from '../../libraries/debounce.lib';
import { sleep } from '../../libraries/sleep.lib';
import {
  ConfigService,
  VoiceModes,
  VoiceModeEntry,
} from '../config/config.service';
import { StorageService } from '../storage/storage.service';
import { NotificationService } from '../toast-notification/toast-notification.service';
import { environment } from '../../../../environments/environment';
import { Subject } from 'rxjs';
import { RTCSession } from '../../models/rtc.model';
import { distance3DTo } from '../../libraries/distance.lib';

export class DevicesObj {
  availableAudioObjs: any;
  defaultAudioObj: any;
  communicationAudioObj: any;
}

@Injectable()
export class MediaService {
  isInitPhase = false;
  ownAudioStream: MediaStream;
  isPreparingAudio = false;
  isAttachingAudio = false;

  audioOptions: RTCOfferOptions = {
    offerToReceiveAudio: true,
    offerToReceiveVideo: false,
    iceRestart: true,
  };

  lastKnownAvailableDevices: any;
  lastKnownCommunicationDevice: any;
  lastKnownDefaultDevice: any;
  lastKnownSelectedDevice: any; // currently active audio device
  // listened by webrtc.service.ts
  inputDevicesChangedSub: Subject<DevicesObj> = new Subject();
  remoteAudioInputMeasurementFailedSub: Subject<Player> = new Subject();

  get useMonoAudio(): boolean {
    return this.configService.getUseMonoAudio();
  }

  constructor(
    private readonly note: NotificationService,
    private readonly configService: ConfigService,
    private readonly storageService: StorageService
  ) {
    this.checkInputDeviceChange();
  }

  checkInputDeviceChange() {
    debounce('checkInputDeviceChange', 500, async () => {
      if (!this.isInitPhase) {
        const devicesObj = (await this.getAudioDevices().catch((err) => {
          console.error(err);
        })) as DevicesObj;

        let hasChanged = false;
        if (
          !(this.lastKnownAvailableDevices == undefined) &&
          Object.keys(this.lastKnownAvailableDevices).length !==
            Object.keys(devicesObj.availableAudioObjs).length
        ) {
          hasChanged = true;
        }

        if (hasChanged) {
          this.ownAudioStream == undefined;
          this.inputDevicesChangedSub.next(devicesObj);
        }

        this.lastKnownCommunicationDevice = devicesObj.communicationAudioObj;
        this.lastKnownDefaultDevice = devicesObj.defaultAudioObj;
        this.lastKnownAvailableDevices = devicesObj.availableAudioObjs;
      }
      this.checkInputDeviceChange();
    });
  }

  async handleOnRemoteTrack(event: RTCTrackEvent, player: Player) {
    if (!(player?.rtc == undefined)) {
      if (event['type'] === 'track') {
        const stream = new MediaStream();
        stream.addTrack(event['track']);
        player.rtc.remoteAudioStream = stream;
      } else {
        player.rtc.remoteAudioStream = event.streams[0];
      }
      console.info(
        '[handleOnRemoteTrack] attached remote track of player ' + player.id
      );
      player.rtc.state = WebRTCState.CONNECTED;
      if (player?.rtc?.remoteAudioApi == undefined) {
        player.rtc.remoteAudioApi = {};
        const ctx = new AudioContext({
          sampleRate: this.configService.globalSamplingRate,
        });
        player.rtc.remoteAudioApi.audioContext = ctx;
        const src = ctx.createMediaStreamSource(player.rtc.remoteAudioStream);
        player.rtc.remoteAudioApi.mediaStreamSourceNode = src;
        const gainNode = ctx.createGain();

        player.rtc.remoteAudioApi.gainNode = gainNode;
        // we use this compressor node to limit the gain and to mitigate possible distortions and artifacts in the output

        // const compressorNode = ctx.createDynamicsCompressor();
        // compressorNode.threshold.value = -3; // Set the threshold to -3 dB
        // compressorNode.ratio.value = 20; // Set the ratio to 20:1
        // compressorNode.attack.value = 0; // Set the attack time to 0 ms
        // compressorNode.release.value = 10; // Set the release time to 10 ms

        const analyserNode = ctx.createAnalyser();
        player.rtc.remoteAudioApi.analyserNode = analyserNode;
        // Since we only want to detect if sound is being played at all, we reduce accuracy
        // drastically to gain performance. Usually analyserNode.fftSize should be used here, which is 2048
        const pcmData = new Float32Array(1);
        player.rtc.remoteAudioApi.pcmData = pcmData;
        player.rtc.remoteAudioApi.audioListener = ctx.listener;
        let pannerNode: PannerNode;
        if (!this.useMonoAudio) {
          pannerNode = new PannerNode(ctx, {
            panningModel: 'equalpower',
            distanceModel: 'linear',
            maxDistance: parseFloat(
              (this.configService.voiceBaseSettings
                ?.maxVoiceConnectionDistance || 0) as any
            ),
            refDistance: 1,
            rolloffFactor: 1,
            coneInnerAngle: 60,
            coneOuterAngle: 120,
            coneOuterGain: 0.5,
          });
          player.rtc.remoteAudioApi.pannerNode = pannerNode;
        }

        // it has to be ctx.destination, otherwise the panner won't work
        // also don't use autoplay or it will play the sound twice...
        let audioNode = src.connect(analyserNode);
        audioNode = audioNode.connect(gainNode);
        // audioNode = audioNode.connect(compressorNode);
        if (!this.useMonoAudio) {
          audioNode = audioNode.connect(pannerNode);
        }
        audioNode = audioNode.connect(ctx.destination);
      }

      this.attachTrackToHTMLElement(player);
      this.muteRemoteAudio(player);
    } else {
      console.error('[handleOnRemoteTrack] webrtc is missing');
    }
  }

  async getAudioDevices(): Promise<{
    availableAudioObjs: any;
    defaultAudioObj: any;
    communicationAudioObj: any;
  }> {
    const audioDevices = navigator.mediaDevices
      ? ((await navigator.mediaDevices.enumerateDevices().catch((err) => {
          console.error(err);
        })) as MediaDeviceInfo[])
      : undefined;
    const availableAudioObjs: any = {};
    let defaultAudioObj: any;
    let communicationAudioObj: any;
    for (const device of audioDevices) {
      let deviceId: string;
      if (device['kind'] === 'audioinput') {
        deviceId = device['deviceId'];
      }
      if (!(deviceId == undefined) && deviceId?.length) {
        device['constraints'] = {
          audio: {
            deviceId,
          },
          video: false,
        };
        switch (deviceId) {
          case 'default':
            defaultAudioObj = device;
            break;
          case 'communications':
            communicationAudioObj = device;
            break;
          default:
            availableAudioObjs[deviceId] = device;
        }
      }
    }
    return { availableAudioObjs, defaultAudioObj, communicationAudioObj };
  }

  async getAudioStream(device: any): Promise<MediaStream> {
    return new Promise((resolve, reject) => {
      if (!(device == undefined) && !(navigator?.mediaDevices == undefined)) {
        navigator.mediaDevices
          .getUserMedia(device.constraints)
          .then((stream: MediaStream) => {
            if (stream.active) {
              const tracks = stream.getAudioTracks();
              if (!(tracks == undefined) && tracks?.length) {
                const track = tracks[0];
                if (track.enabled && track.readyState === 'live') {
                  return resolve(stream);
                }
              }
            }
            resolve(undefined);
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve(undefined);
      }
    });
  }

  private async triggerDevicePermissionRequest() {
    return navigator.mediaDevices.getUserMedia({
      audio: true,
      video: false,
    });
  }

  async prepareOwnAudioStream(ownPlayer: Player) {
    if (!(ownPlayer == undefined) && ownPlayer.type === 'debug') {
      await this.prepareDebugAudioTrack(ownPlayer).catch((err) => {
        console.error(err, undefined, 'prepareDebugAudioTrack');
      });
      return;
    }

    if (
      !(this.ownAudioStream?.active == undefined) &&
      this.ownAudioStream.active === false
    ) {
      console.warn(
        '[prepareOwnAudioStream] own audio stream is inactive, stopping...'
      );
      const tracks: MediaStreamTrack =
        this.ownAudioStream?.getAudioTracks()?.[0];
      if (!(tracks == undefined)) {
        tracks.stop();
      }
      this.ownAudioStream = undefined;
    }

    if (this.ownAudioStream == undefined && !this.isPreparingAudio) {
      this.isInitPhase = true;
      this.isPreparingAudio = true;
      await this.triggerDevicePermissionRequest().catch((err) => {
        if (!environment.production) {
          console.error(err, undefined, 'triggerDevicePermissionRequest');
        }
        this.note.overrideTimeoutInMs(15000);
        this.note.showNotification(
          'warning',
          undefined,
          'NOTIFICATIONS.DEVICE_PERMISSION_REQUEST_FAILED'
        );
        this.note.resetTimeoutDelayed();
      });
      const devicesObj = (await this.getAudioDevices().catch((err) => {
        if (!environment.production) {
          console.error(err, undefined, 'getAudioDevices');
        }
        this.note.showNotification(
          'error',
          undefined,
          'NOTIFICATIONS.FAILED_TO_GET_AUDIO_DEVICES'
        );
      })) as DevicesObj;

      let audioStream: MediaStream;
      if (!(devicesObj?.availableAudioObjs == undefined)) {
        let hasValidSelectedDevice = false;
        let device = this.configService.getSelectedDevice();
        if (
          device == undefined ||
          devicesObj?.availableAudioObjs?.[device?.deviceId] == undefined
        ) {
          const deviceIds = Object.keys(devicesObj.availableAudioObjs);
          device = devicesObj.availableAudioObjs[deviceIds[0]];
        }

        audioStream = (await this.getAudioStream(device).catch((err) => {
          console.error(err);
        })) as MediaStream;
        if (
          !(audioStream?.active == undefined) &&
          audioStream?.active === true
        ) {
          hasValidSelectedDevice = true;
        }

        if (!hasValidSelectedDevice) {
          for (const deviceId of Object.keys(devicesObj.availableAudioObjs)) {
            audioStream = (await this.getAudioStream(
              devicesObj.availableAudioObjs[deviceId]
            ).catch((err) => {
              console.error(err);
            })) as MediaStream;
            if (
              !(audioStream?.active == undefined) &&
              audioStream?.active === true
            ) {
              this.lastKnownSelectedDevice =
                devicesObj.availableAudioObjs[deviceId];
              break;
            }
          }
        }
      }

      if (!(audioStream == undefined)) {
        if (ownPlayer.rtc == undefined) {
          ownPlayer.rtc = new RTCSession();
        }

        this.prepareOwnAudioApiNodes(ownPlayer, undefined, audioStream);
      }

      this.isPreparingAudio = false;
      debounce(
        'isInitPhase-prepareOwnAudioStream',
        Math.random() * 500 + 2000,
        () => {
          this.isInitPhase = false;
        }
      );
    }
  }

  async prepareDebugAudioTrack(ownPlayer: Player) {
    return new Promise((resolve) => {
      if (this.ownAudioStream == undefined && !this.isPreparingAudio) {
        this.isInitPhase = true;
        this.isPreparingAudio = true;
        const debugAudio = ownPlayer.audio;

        if (ownPlayer.rtc == undefined) {
          ownPlayer.rtc = new RTCSession();
        }

        if (!(debugAudio == undefined) && debugAudio?.length) {
          const audioElem = new Audio(debugAudio);
          this.prepareOwnAudioApiNodes(ownPlayer, audioElem);
          audioElem.loop = true;
          audioElem.play();
        }

        this.isPreparingAudio = false;
        debounce(
          'isInitPhase-prepareOwnAudioStream',
          Math.random() * 500 + 2000,
          () => {
            this.isInitPhase = false;
          }
        );
      }
      resolve(this.ownAudioStream);
    });
  }

  prepareOwnAudioApiNodes(
    ownPlayer: Player,
    audioElem?: HTMLAudioElement,
    audioStream?: MediaStream
  ) {
    if (audioElem == undefined && audioStream == undefined) {
      console.error('prepareOwnAudioApiNodes - no audio source provided...');
      return;
    }

    if (ownPlayer?.rtc?.ownAudioApi == undefined) {
      ownPlayer.rtc.ownAudioApi = {};
    }
    const ctx = new AudioContext({
      sampleRate: this.configService.globalSamplingRate,
    });
    ownPlayer.rtc.ownAudioApi.audioContext = ctx;
    let src: any;
    if (!(audioElem == undefined)) {
      src = ctx.createMediaElementSource(audioElem);
    } else if (!(audioStream == undefined)) {
      src = ctx.createMediaStreamSource(audioStream);
    }
    ownPlayer.rtc.ownAudioApi.mediaElementSourceNode = src;
    const dst = ctx.createMediaStreamDestination();
    ownPlayer.rtc.ownAudioApi.mediaStreamDestinationNode = dst;
    const gainNode = ctx.createGain();
    ownPlayer.rtc.ownAudioApi.gainNode = gainNode;
    // if ctx.destination is used, it will play own audio element even if it's not attached to the DOM
    // that ctx.destination is an actual output device and cannot be streamed via rtc
    // https://stackoverflow.com/questions/73739359/whats-the-difference-between-audiocontext-destination-and-audiocontext-createme
    src.connect(gainNode).connect(dst);

    this.ownAudioStream = dst.stream;

    ownPlayer.rtc.ownAudioStream = this.ownAudioStream;

    this.muteOwnAudio(ownPlayer);
  }

  async attachOwnAudioTrack(
    remotePlayer: Player,
    ownPlayer: Player,
    breakCounter = 0
  ) {
    if (!(remotePlayer?.rtc == undefined)) {
      remotePlayer.rtc.ownAudioStream = this.ownAudioStream;
    }

    if (remotePlayer?.rtc?.ownAudioStream == undefined) {
      console.warn(
        'ownAudioStream is missing, cannot attach to rtc pc, retrying...'
      );
      // retry attaching own audio in case of temporary deadlock
      breakCounter++;

      await this.prepareOwnAudioStream(ownPlayer).catch((err) => {
        console.error(err);
      });

      if (breakCounter < 5) {
        await sleep(50);
        await this.attachOwnAudioTrack(
          remotePlayer,
          ownPlayer,
          breakCounter
        ).catch((err) => {
          console.error(err, undefined, 'attachOwnAudioTrack');
        });
      }

      return;
    }

    const tracks = remotePlayer.rtc.ownAudioStream?.getAudioTracks();
    if (!(tracks == undefined) && tracks?.length) {
      if (
        remotePlayer.rtc.state !== WebRTCState.CLOSED &&
        !(remotePlayer.rtc.pc == undefined) &&
        remotePlayer.rtc.pc.signalingState !== 'closed' &&
        remotePlayer.rtc.pc.getSenders()[0] == undefined
      ) {
        console.info(
          `attaching audio track, signaling state ${remotePlayer.rtc.pc.signalingState}`
        );
        remotePlayer.rtc.pc.addTrack(tracks[0]);
      }
    }
  }

  attachTrackToHTMLElement(remotePlayer: Player): HTMLAudioElement {
    if (remotePlayer == undefined) {
      console.warn(
        '[attachTrackToHTMLElement] remotePlayer is invalid, skipping...'
      );
      return;
    }

    if (
      remotePlayer.rtc.remoteAudioElement == undefined &&
      !(remotePlayer?.rtc?.remoteAudioStream == undefined)
    ) {
      const recvAudio = new Audio();
      recvAudio.srcObject = remotePlayer.rtc.remoteAudioStream;
      remotePlayer.rtc.remoteAudioElement = recvAudio;

      console.info(
        '[attachTrackToHTMLElement] attached audio element of remote player ' +
          remotePlayer.id
      );
      return recvAudio;
    } else {
      return remotePlayer.rtc.remoteAudioElement;
    }
  }

  removeTrackFromHTMLElement(remotePlayer: Player) {
    if (remotePlayer == undefined) {
      console.warn(
        '[removeTrackFromHTMLElement] remotePlayer is invalid, skipping...'
      );
      return;
    }

    const containerElem = document.getElementById(remotePlayer.id);

    if (!(containerElem == undefined)) {
      for (let i = 0; i < (containerElem?.children?.length || 0); i++) {
        const audioElem = containerElem.children.item(i);
        containerElem.removeChild(audioElem);
      }
    }
  }

  muteOwnAudio(ownPlayer: Player) {
    if (!(ownPlayer?.rtc?.ownAudioApi?.gainNode == undefined)) {
      ownPlayer.rtc.ownAudioApi.gainNode.gain.value = 0;
      ownPlayer.rtc.ownAudioApi.audioContext.suspend();
    }
  }

  activateOwnAudio(ownPlayer: Player) {
    if (!(ownPlayer?.rtc?.ownAudioApi?.gainNode == undefined)) {
      ownPlayer.rtc.ownAudioApi.gainNode.gain.value = 1;
      ownPlayer.rtc.ownAudioApi.audioContext.resume();
    }
  }

  muteRemoteAudio(remotePlayer: Player) {
    if (!(remotePlayer?.rtc?.remoteAudioApi?.gainNode == undefined)) {
      remotePlayer.rtc.remoteAudioApi.gainNode.gain.value = 0;
    }
  }

  async activateRemoteAudio(
    remotePlayer: Player,
    ownPlayer: Player,
    locallyKnownRemotePlayers?: {
      [id: string]: {
        pos: { x: number; y: number; z: number };
        fVector: { x: number; y: number; z: number };
      };
    },
    lastLocallyKnownDataUpdate?: number
  ): Promise<{
    [id: string]: { id: string; x: number; y: number; z: number };
  }> {
    if (!(remotePlayer?.rtc?.remoteAudioApi?.gainNode == undefined)) {
      const voiceModes = this.configService.voiceBaseSettings?.voiceModes;
      let voiceMode: VoiceModeEntry;
      let voiceModeKey: string;
      if (
        !(remotePlayer.inputKeys == undefined) &&
        !(voiceModes == undefined)
      ) {
        for (const key of Object.keys(voiceModes)) {
          if (remotePlayer.inputKeys[key] === true) {
            voiceMode = voiceModes[key];
            voiceModeKey = key;
          }
        }
      }
      if (voiceMode == undefined || this.configService.isGlobalMuted) {
        this.muteRemoteAudio(remotePlayer);
        return;
      }

      let volumeLevelSetting = 1;
      if (!(remotePlayer.volumeLevelSetting == undefined)) {
        volumeLevelSetting = Math.max(
          0,
          Math.min(1, remotePlayer.volumeLevelSetting)
        );
      }

      const gainNode = remotePlayer.rtc.remoteAudioApi.gainNode;
      gainNode.gain.value = volumeLevelSetting;

      // Directional sound
      const audioListener = remotePlayer?.rtc?.remoteAudioApi?.audioListener;
      if (!(audioListener == undefined)) {
        audioListener.positionX.value = ownPlayer.pos.x;
        audioListener.positionY.value = ownPlayer.pos.y;
        audioListener.positionZ.value = ownPlayer.pos.z;

        audioListener.forwardX.value = ownPlayer.fVector.x * -1;
        audioListener.forwardY.value = ownPlayer.fVector.y * -1;
        audioListener.forwardZ.value = ownPlayer.fVector.z * -1;

        audioListener.upX.value = ownPlayer.uVector.x;
        audioListener.upY.value = ownPlayer.uVector.y;
        audioListener.upZ.value = ownPlayer.uVector.z;
      }

      if (this.configService.voiceBaseSettings == undefined) {
        await this.configService.getVoiceBaseSettings();
      }

      const velocityOffsets =
        this.configService.voiceBaseSettings?.velocityOffsets;
      const pannerNode = remotePlayer?.rtc?.remoteAudioApi?.pannerNode;

      let offsetX = 0;
      let offestY = 0;
      let offestZ = 0;
      let positionXOffset = 0;
      let positionYOffset = 0;
      let positionZOffset = 0;

      let positionOffsetMultiplier =
        velocityOffsets?.positionTalkOffsetMultiplier || 0;
      let radiusExtensionMultiplier =
        velocityOffsets?.radiusTalkExtensionMultiplier || 0;
      switch (voiceModeKey) {
        case 'whisper':
          positionOffsetMultiplier =
            velocityOffsets?.positionWhisperOffsetMultiplier || 0;
          radiusExtensionMultiplier =
            velocityOffsets?.radiusWhisperExtensionMultiplier || 0;
          break;
        case 'yell':
          positionOffsetMultiplier =
            velocityOffsets?.positionYellOffsetMultiplier || 0;
          radiusExtensionMultiplier =
            velocityOffsets?.radiusYellExtensionMultiplier || 0;
          break;
      }

      let vehicleSpeed = 0;
      if (!(ownPlayer.vehicle?.velocity == undefined)) {
        vehicleSpeed = Math.sqrt(
          ownPlayer.vehicle?.velocity?.x * ownPlayer.vehicle?.velocity?.x +
            ownPlayer.vehicle?.velocity?.y * ownPlayer.vehicle?.velocity?.y +
            ownPlayer.vehicle?.velocity?.z * ownPlayer.vehicle?.velocity?.z
        );
      }

      if (!(pannerNode == undefined)) {
        // we get about every 100ms an updated data package
        const packageDelayPercent = Math.max(
          0,
          Math.min(1, (new Date().valueOf() - lastLocallyKnownDataUpdate) / 100)
        );

        // inverted vector since it seems to apply the offset 180° to the wrong direction
        positionXOffset =
          (ownPlayer.vehicle?.velocity?.x || 0) *
            positionOffsetMultiplier *
            packageDelayPercent *
            velocityOffsets?.positionOffsetDirection || 0;
        positionYOffset =
          (ownPlayer.vehicle?.velocity?.y || 0) *
            positionOffsetMultiplier *
            packageDelayPercent *
            velocityOffsets?.positionOffsetDirection || 0;
        positionZOffset =
          (ownPlayer.vehicle?.velocity?.z || 0) *
            positionOffsetMultiplier *
            packageDelayPercent *
            velocityOffsets?.positionOffsetDirection || 0;

        // use locally known player positions instead of reported ones from api, this approach
        // reduces missplacements of the panner node drastically - for moving vehicles
        const remotePos =
          locallyKnownRemotePlayers?.[remotePlayer.id]?.pos || remotePlayer.pos;
        const remoteFVector =
          locallyKnownRemotePlayers?.[remotePlayer.id]?.fVector ||
          remotePlayer.fVector;

        offsetX = remotePos.x + positionXOffset;
        offestY = remotePos.y + positionYOffset;
        offestZ = remotePos.z + positionZOffset;

        pannerNode.positionX.value = offsetX;
        pannerNode.positionY.value = offestY;
        pannerNode.positionZ.value = offestZ;

        pannerNode.orientationX.value = remoteFVector.x;
        pannerNode.orientationY.value = remoteFVector.y;
        pannerNode.orientationZ.value = remoteFVector.z;

        pannerNode.maxDistance =
          this.configService.voiceBaseSettings?.maxVoiceConnectionDistance *
            voiceMode.maxDistancePercent +
          vehicleSpeed * radiusExtensionMultiplier;
      } else if (this.useMonoAudio) {
        const maxDistance =
          this.configService.voiceBaseSettings?.maxVoiceConnectionDistance *
            voiceMode.maxDistancePercent +
          vehicleSpeed * radiusExtensionMultiplier;

        const remotePos =
          locallyKnownRemotePlayers?.[remotePlayer.id]?.pos || remotePlayer.pos;
        const currentDistance = distance3DTo(remotePos, ownPlayer.pos);

        gainNode.gain.value = Math.max(
          0,
          Math.min(1, volumeLevelSetting * (1 - currentDistance / maxDistance))
        );
      }

      // measure volume level
      remotePlayer.rtc.remoteAudioApi.analyserNode.getFloatTimeDomainData(
        remotePlayer.rtc.remoteAudioApi.pcmData
      );
      let sumSquares = 0.0;
      for (const amplitude of remotePlayer.rtc.remoteAudioApi.pcmData) {
        sumSquares += amplitude * amplitude;
      }
      if (remotePlayer?.rtc?.remoteAudioLevel == undefined) {
        remotePlayer.rtc.remoteAudioLevel = {
          measuredVolume: 0,
          noVolumeCounter: 0,
        };
      }
      remotePlayer.rtc.remoteAudioLevel.measuredVolume = Math.sqrt(
        sumSquares / remotePlayer.rtc.remoteAudioApi.pcmData.length
      );
      if (remotePlayer.rtc.remoteAudioLevel.measuredVolume > 0) {
        remotePlayer.rtc.remoteAudioLevel.noVolumeCounter = 0;
      } else {
        remotePlayer.rtc.remoteAudioLevel.noVolumeCounter++;
      }

      if (
        remotePlayer.rtc.remoteAudioLevel.noVolumeCounter >
          this.configService.remoteAudioIsFailingThreshold &&
        volumeLevelSetting > 0
      ) {
        this.remoteAudioInputMeasurementFailedSub.next(remotePlayer);
      }

      return {
        [remotePlayer.id]: {
          id: remotePlayer.id,
          x: offsetX,
          y: offestY,
          z: offestZ,
        },
        ['self']: {
          id: 'self',
          x: ownPlayer.pos.x * positionXOffset,
          y: ownPlayer.pos.y * positionYOffset,
          z: ownPlayer.pos.z * positionZOffset,
        },
      };
    }
  }
}
