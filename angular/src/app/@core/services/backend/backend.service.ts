import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable()
export class BackendService {
  limit = 10;
  offset = 0;
  filter = '';

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient
  ) {}

  async getBackends(
    limit?: number,
    offset?: number,
    filter?: string
  ): Promise<any> {
    let env = 'DEV';
    if (environment.staging) {
      env = 'STAGING';
    } else if (environment.production) {
      env = 'PROD';
    }
    const promises = [];
    for (const provoApi of environment.provoApis) {
      let url = provoApi + '/api/v1/backends?env=' + env;
      if (!(limit == undefined) && limit > 0) {
        url += '&limit=' + limit;
      }
      if (!(offset == undefined) && offset > 0) {
        url += '&offset=' + offset;
      }
      if (!(filter == undefined) && filter?.length > 0) {
        url += '&filter=' + filter;
      }
      promises.push(
        firstValueFrom(this.http.get(url)).catch((err) => {
          if (!environment.production) {
            console.error(err);
          }
        })
      );
    }
    const results = await Promise.all(promises);
    return results?.[0];
  }
}
