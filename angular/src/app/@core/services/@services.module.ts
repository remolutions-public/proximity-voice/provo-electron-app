import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { ConfigService } from './config/config.service';
import { RoutingService } from './routing/routing.service';
import { SidebarService } from './sidebar/sidebar.service';
import { LanguageService } from './language/language.service';
import { StorageService } from './storage/storage.service';
import { ThemeService } from './theme/theme.service';
import { IPCService } from './ipc/ipc.service';
import { WebRTCService } from './webrtc/webrtc.service';
import { VoiceService } from './voice/voice.service';
import { VoiceControllerService } from './voice/voice-controller.service';
import { MediaService } from './media/media.service';
import { NotificationService } from './toast-notification/toast-notification.service';
import { MaterialIconsService } from './theme/material-icons/material-icons.service';
import { BackdropService } from './backdrop/backdrop.service';
import { ModalService } from './modal/modal.service';
import { UpdateService } from './update/update.service';
import { MqttService } from './mqtt/mqtt.service';
import { ElectronService } from './electron/electron.service';
import { TelemetryService } from './telemetry/telemetry.service';
import { BackendService } from './backend/backend.service';

const MODULES = [CommonModule, TranslateModule];

const SERVICES = [
  ConfigService,
  RoutingService,
  SidebarService,
  LanguageService,
  ThemeService,
  IPCService,
  VoiceService,
  VoiceControllerService,
  WebRTCService,
  MediaService,
  NotificationService,
  MaterialIconsService,
  BackdropService,
  ModalService,
  StorageService,
  UpdateService,
  MqttService,
  ElectronService,
  TelemetryService,
  BackendService,
];

@NgModule({
    declarations: [],
    imports: [...MODULES],
    providers: [...SERVICES]
})
export class ServicesModule {
  static forRoot(): ModuleWithProviders<ServicesModule> {
    return {
      ngModule: ServicesModule,
      providers: [...SERVICES],
    };
  }
}
