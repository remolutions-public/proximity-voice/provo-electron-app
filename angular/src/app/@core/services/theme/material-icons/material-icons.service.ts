import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

@Injectable()
export class MaterialIconsService {
  constructor(
    private readonly iconRegistry: MatIconRegistry,
    private readonly sanitizer: DomSanitizer,
  ) { }

  registerIcons() {
    // icons from https://material.io/tools/icons
    this.iconRegistry.addSvgIcon('arrow_drop_down', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-arrow_drop_down-24px.svg'));
    this.iconRegistry.addSvgIcon('arrow_drop_up', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-arrow_drop_up-24px.svg'));
    this.iconRegistry.addSvgIcon('open_menu', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-view_headline-24px.svg'));
    this.iconRegistry.addSvgIcon('user', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-person-24px.svg'));
    this.iconRegistry.addSvgIcon('message', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-mail_outline-24px.svg'));
    this.iconRegistry.addSvgIcon('mail', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-alternate_email-24px.svg'));
    this.iconRegistry.addSvgIcon('send', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-send-24px.svg'));
    this.iconRegistry.addSvgIcon('close', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/baseline-close-24px.svg'));
    this.iconRegistry.addSvgIcon('visible', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/baseline-visibility-24px.svg'));
    this.iconRegistry.addSvgIcon('hidden', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/baseline-visibility_off-24px.svg'));
    this.iconRegistry.addSvgIcon('password', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/baseline-vpn_key-24px.svg'));
    this.iconRegistry.addSvgIcon('chevron_left', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-chevron_left-24px.svg'));
    this.iconRegistry.addSvgIcon('chevron_right', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-chevron_right-24px.svg'));
    this.iconRegistry.addSvgIcon('chevron_up', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-expand_less-24px.svg'));
    this.iconRegistry.addSvgIcon('chevron_down', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-expand_more-24px.svg'));
    this.iconRegistry.addSvgIcon('refresh', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-refresh-24px.svg'));
    this.iconRegistry.addSvgIcon('edit', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/outline-edit-24px.svg'));
    this.iconRegistry.addSvgIcon('delete', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/outline-delete-24px.svg'));
    this.iconRegistry.addSvgIcon('search', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-search-24px.svg'));
    this.iconRegistry.addSvgIcon('reset', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/baseline-reply-24px.svg'));
    this.iconRegistry.addSvgIcon('install', this.sanitizer.bypassSecurityTrustResourceUrl('assets/icon/sharp-archive-24px.svg'));
  }
}
