import { Injectable } from '@angular/core';
import { NotificationService } from '../toast-notification/toast-notification.service';
import { IPCService } from '../ipc/ipc.service';
import { MqttService } from '../mqtt/mqtt.service';
import {
  debounce,
  stopDebounceRef,
} from '../../libraries/debounce.lib';
import { v4 as uuidv4 } from 'uuid';
import { StorageService } from '../storage/storage.service';
import { parseBoolean } from '../../libraries/parser.lib';
import { BackendConfig } from '../../models/backend.model';

export class VoiceModeEntry {
  voiceLevel: number;
  maxDistancePercent: number;
}

export class VoiceModes {
  [mode: string]: VoiceModeEntry;
}

export class VelocityOffsets {
  positionWhisperOffsetMultiplier?: number = 1.0;
  radiusWhisperExtensionMultiplier?: number = 1.5;
  positionTalkOffsetMultiplier?: number = 1.5;
  radiusTalkExtensionMultiplier?: number = 1.0;
  positionYellOffsetMultiplier?: number = 1.0;
  radiusYellExtensionMultiplier?: number = 1.0;
  positionOffsetDirection?: number = 1;
}

export class VoiceBaseSettings {
  voiceModes: VoiceModes;
  maxVoiceConnectionDistance?: number = 15000;
  velocityOffsets?: VelocityOffsets = new VelocityOffsets();
}

@Injectable()
export class ConfigService {
  isDebugMode = false;

  isGlobalMuted = false;

  private useMonoAudio: boolean;
  get isMonoAudio(): boolean {
    return this.useMonoAudio;
  }

  private selectedDevice: any;

  maxAllowedSimultaneousConnectionAttempts = 5;

  pressedButtonReleaseDelay = 800;

  remoteAudioIsFailingThreshold = 300; // about 30 seconds of failed audio measurements

  // sampleRate of 48000 is default; https://gyazo.com/35c31c63bdb8527e73f5bd74b36fb587
  globalSamplingRate = 28000;

  autoDisconnectTime = 10000;
  retryDelayTime = 2500;
  disconnectCooldownTime = 500;

  // voice config
  pullConfigDataTimeout = 10000;
  pullConfigDataDelay = 30000;
  pullCoturnDataDelay = 120000;
  lastCoturnDataPullTime: number;
  cachedCoturnData: any;
  lastVoiceBaseSettingsPullTime: number;
  lastMaskingConfigPullTime: number;

  voiceBaseSettings: VoiceBaseSettings;

  maskingConfig: any;

  // player config
  maxRandomPlayerID = 1000;

  constructor(
    private readonly note: NotificationService,
    private readonly ipcService: IPCService,
    private readonly mqttService: MqttService,
    private readonly storageService: StorageService
  ) {}

  getUseMonoAudio(): boolean {
    if (this.useMonoAudio == undefined) {
      const storedVal = this.storageService.get('useMonoAudio', 'local');
      if (!(storedVal == undefined)) {
        this.useMonoAudio = parseBoolean(storedVal);
      }
    }
    return this.useMonoAudio;
  }

  setUseMonoAudio(val: boolean) {
    this.useMonoAudio = val;
    this.storageService.set('useMonoAudio', val, 'local');
  }

  getSelectedDevice(): any {
    if (this.selectedDevice == undefined) {
      const storedVal = this.storageService.get('selectedInputDevice', 'local');
      if (!(storedVal == undefined)) {
        this.selectedDevice = storedVal;
      }
    }
    return this.selectedDevice;
  }

  setSelectedDevice(device: any) {
    this.selectedDevice = device;
    try {
      if (!(device == undefined)) {
        const storageVal = JSON.stringify(device);
        this.storageService.set('selectedInputDevice', storageVal, 'local');
      }
    } catch (e) {
      console.error(e);
    }
  }

  async getVoiceBaseSettings(): Promise<VoiceBaseSettings> {
    return new Promise((resolve) => {
      if (
        !(this.lastVoiceBaseSettingsPullTime == undefined) &&
        new Date().valueOf() - this.lastVoiceBaseSettingsPullTime <=
          this.pullConfigDataDelay &&
        !(this.voiceBaseSettings == undefined)
      ) {
        return resolve(this.voiceBaseSettings);
      }
      if (this.voiceBaseSettings == undefined) {
        this.voiceBaseSettings = new VoiceBaseSettings();
      }
      this.lastVoiceBaseSettingsPullTime = new Date().valueOf();
      const requestid = uuidv4();
      const sub = this.mqttService.voiceBaseSettingsSub.subscribe((data) => {
        sub.unsubscribe();
        stopDebounceRef('getVoiceBaseSettings-fallback-' + requestid);
        if (!(data?.maxDistance == undefined)) {
          this.voiceBaseSettings.maxVoiceConnectionDistance = data?.maxDistance;
        }
        if (!(data?.voiceModeKeys == undefined)) {
          this.voiceBaseSettings.voiceModes = data?.voiceModeKeys;
        }
        if (!(data?.velocityOffsets == undefined)) {
          for (const key of Object.keys(data?.velocityOffsets)) {
            this.voiceBaseSettings.velocityOffsets[key] =
              data?.velocityOffsets[key];
          }
        }
        resolve(this.voiceBaseSettings);
      });
      this.mqttService.produceMessage({
        type: 'fetch-voiceBaseSettings',
        clientid: this.ipcService.clientid,
        requestid,
        time: new Date().valueOf(),
      });
      debounce(
        'getVoiceBaseSettings-fallback-' + requestid,
        this.pullConfigDataTimeout,
        () => {
          resolve(this.voiceBaseSettings);
        }
      );
    });
  }

  async getCoturnData(): Promise<any> {
    return new Promise((resolve) => {
      if (
        !(this.lastCoturnDataPullTime == undefined) &&
        new Date().valueOf() - this.lastCoturnDataPullTime <=
          this.pullCoturnDataDelay &&
        !(this.cachedCoturnData == undefined)
      ) {
        return resolve(this.cachedCoturnData);
      }
      const requestid = uuidv4();
      const sub = this.ipcService.receiveCotrunDataSub.subscribe((message) => {
        const data = message.data;
        if (data?.request?.requestid === requestid) {
          sub.unsubscribe();
          stopDebounceRef('getCoturnData-fallback-' + requestid);
          const response = {
            data: data?.coturnData,
            latency:
              Math.round(
                (new Date().valueOf() - data?.request?.time) * 0.5 * 100
              ) / 100,
          };
          this.cachedCoturnData = response;
          this.lastCoturnDataPullTime = new Date().valueOf();
          resolve(response);
        }
      });

      this.mqttService.produceMessage({
        type: 'fetch-coturn-data',
        clientid: this.ipcService.clientid,
        requestid,
        time: new Date().valueOf(),
      });

      debounce(
        'getCoturnData-fallback-' + requestid,
        this.pullConfigDataTimeout,
        () => {
          resolve(this.cachedCoturnData);
        }
      );
    });
  }

  async getMaskingConfig(): Promise<any> {
    return new Promise((resolve) => {
      if (
        !(this.lastMaskingConfigPullTime == undefined) &&
        new Date().valueOf() - this.lastMaskingConfigPullTime <=
          this.pullConfigDataDelay &&
        !(this.maskingConfig == undefined)
      ) {
        return resolve(this.maskingConfig);
      }
      this.lastMaskingConfigPullTime = new Date().valueOf();
      const requestid = uuidv4();
      const sub = this.ipcService.receiveMaskingconfigSub.subscribe(
        (message) => {
          const data = message.data;
          if (data?.request?.requestid === requestid) {
            sub.unsubscribe();
            stopDebounceRef('getMaskingConfig-fallback-' + requestid);
            this.maskingConfig = data.maskConfig;
            resolve(this.maskingConfig);
          }
        }
      );

      this.mqttService.produceMessage({
        type: 'fetch-maskingConfig',
        clientid: this.ipcService.clientid,
        requestid,
        time: new Date().valueOf(),
      });

      debounce(
        'getMaskingConfig-fallback-' + requestid,
        this.pullConfigDataTimeout,
        () => {
          resolve(this.maskingConfig);
        }
      );
    });
  }

  async getBackendConfig(): Promise<BackendConfig> {
    return this.mqttService.getBackendConfig();
  }

  debugLog(
    logLevel: 'log' | 'info' | 'warn' | 'error',
    message?: any,
    ...args: any[]
  ) {
    if (this.isDebugMode) {
      console[logLevel](message, ...args);

      if (logLevel === 'error') {
        this.note.showNotification('error', undefined, message);
      }
    }
  }
}
