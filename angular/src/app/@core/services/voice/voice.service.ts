import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { IPCService } from '../ipc/ipc.service';
import { MqttService } from '../mqtt/mqtt.service';
import { Subject } from 'rxjs';
import {
  debounce,
  hasDebounceRef,
  stopDebounceRef,
} from '../../libraries/debounce.lib';
import { Player } from '../../models/player.model';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Injectable()
export class VoiceService {
  static wasInit = false;
  currentLatency = -1;
  currentAPI = '';
  namespace = 'voice/';
  isOwnVoiceActive = false;
  lastVoiceToggleKey: string;
  lastVoiceToggleTime: number;

  lastEmulateDebugPlayerDataTime: number;

  ownPlayer: Player = {};

  lastPlayerRandomID = 0;
  cachedPlayerRandomIDs = {};

  nearbyPlayers = {};
  // bypasses the communication to the api and back, but caches player locations directly from the local game client
  locallyKnownRemotePlayers = {};
  lastLocallyKnownDataUpdate = new Date().valueOf();
  offsetCalculatedRemotePlayers = {};

  updateOwnStateSub: Subject<any> = new Subject();
  reconnectToAllSub: Subject<any> = new Subject();
  updateLocationSub: Subject<any> = new Subject();
  updatedNearbyPlayersSub: Subject<any> = new Subject();

  lastReconnectAllTriggerTime = 0;
  lastRemotePlayerReconnectTriggerTime = {};
  reconnectAllTriggerCooldown = 30 * 1000;

  telemetryKeyDownPackageCounter = 0;
  telemetryPlayerDataPackageCounter = 0;

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient,
    private readonly ipcService: IPCService,
    private readonly mqttService: MqttService
  ) {}

  init() {
    if (!VoiceService.wasInit) {
      VoiceService.wasInit = true;
      this.ipcService.downKeysSub.subscribe((msg: any) => {
        if (!(msg?.data == undefined)) {
          this.telemetryKeyDownPackageCounter++;
          if (msg.data.key === 'reconnect') {
            this.reconnectToAllSub.next(true);
            return;
          }
          this.isOwnVoiceActive = msg.data.pressed || false;
          this.lastVoiceToggleKey = msg.data.key;
          this.lastVoiceToggleTime = msg.stamp;
          this.updateOwnStateSub.next(this.isOwnVoiceActive);
        }
      });
      this.ipcService.playerDataSub.subscribe((msg: any) => {
        if (!(msg?.data == undefined)) {
          this.telemetryPlayerDataPackageCounter++;
          const data: any = msg.data;
          this.locallyKnownRemotePlayers = data.rPlayer;
          this.lastLocallyKnownDataUpdate = data.timestamp;

          if (data.isDebug) {
            for (const playerid of Object.keys(
              this.offsetCalculatedRemotePlayers
            )) {
              if (
                new Date().valueOf() -
                  this.offsetCalculatedRemotePlayers[playerid].time >
                2500
              ) {
                delete this.offsetCalculatedRemotePlayers[playerid];
              }
            }
            this.ipcService.sendApiMessage({
              type: 'debugRemotePositions',
              clientid: this.ipcService.clientid,
              data: this.offsetCalculatedRemotePlayers,
            });
          }

          // DEBUGGING
          // data.serverid = environment.serverId;

          this.updateLocationSub.next(data);
          const updateStateDebounce = () => {
            debounce('updateStateDebounce', 100, () => {
              this.updateOwnStateSub.next(true);
            });
          };

          if (
            !(data?.name == undefined) &&
            this.ownPlayer?.name !== data.name
          ) {
            this.ownPlayer.name = data.name;
            updateStateDebounce();
          }
          if (!(data?.id == undefined) && this.ownPlayer?.id !== data.id) {
            this.ownPlayer.id = data.id;
            updateStateDebounce();
          }
          if (
            !(data?.type == undefined) &&
            this.ownPlayer?.type !== data.type
          ) {
            this.ownPlayer.type = data.type;
            updateStateDebounce();
          }
          if (
            !(data?.audio == undefined) &&
            this.ownPlayer?.audio !== data.audio
          ) {
            this.ownPlayer.audio = data.audio;
            updateStateDebounce();
          }
          debounce(
            'fallback-disconnect-client',
            this.configService.autoDisconnectTime,
            () => {
              this.ownPlayer.id = undefined;
              updateStateDebounce();
            }
          );
        }
        this.randomizeDebugKeypress();
        this.emulateDebugPlayerData();
        this.updatedNearbyPlayersSub.next(true);
      });
      this.updateLocationSub.subscribe((data) => {
        this.configService.getVoiceBaseSettings();
        this.ownPlayer.pos = data.pos;
        this.ownPlayer.fVector = data.fVector;
        this.ownPlayer.uVector = data.uVector;
        this.ownPlayer.realm = data.realm;
        this.ownPlayer.clientid = data.clientid;
        this.ownPlayer.vehicle = data.vehicle;
        this.updatedNearbyPlayersSub.next(true);
      });
      this.ipcService.nearbyPlayersSub.subscribe(async (message) => {
        if (this.configService.maskingConfig == undefined) {
          await this.configService.getMaskingConfig();
        }
        this.updateNearbyPlayers(message?.data);
      });
      this.checkOwnPlayerData();
    }
  }

  checkOwnPlayerData() {
    debounce('checkOwnPlayerData', 2000, () => {
      if (this.ownPlayer?.id == undefined) {
        this.updateOwnStateSub.next(true);
      }

      // periodically pulling configs and settings
      this.configService.getVoiceBaseSettings();
      this.configService.getMaskingConfig();

      this.checkOwnPlayerData();
    });
  }

  emulateDebugPlayerData() {
    if (
      !(this.ownPlayer == undefined) &&
      this.ownPlayer.type === 'debug' &&
      !(this.ipcService.clientid == undefined)
    ) {
      if (!hasDebounceRef('emulateDebugPlayerData')) {
        debounce(
          'emulateDebugPlayerData',
          this.configService.retryDelayTime * 0.05,
          () => {
            const debugData = environment[this.ownPlayer.id];
            if (debugData == undefined) {
              return;
            }
            debugData.clientid = this.ipcService.clientid;

            // TESTING
            // console.info(debugData);

            firstValueFrom(
              this.http.post(
                environment.localClientApi + '/player',
                debugData,
                {
                  withCredentials: true,
                }
              )
            ).catch((err) => console.error(err));
          }
        );
      }
    }
  }

  randomizeDebugKeypress() {
    if (this.ownPlayer?.type === 'debug') {
      const nowTime = new Date().valueOf();
      if (
        this.isOwnVoiceActive &&
        nowTime - (this.lastVoiceToggleTime || 0) >
          this.configService.retryDelayTime * 5
      ) {
        this.isOwnVoiceActive = false;
        this.lastVoiceToggleTime = nowTime;
      }

      if (
        !this.isOwnVoiceActive &&
        nowTime - (this.lastVoiceToggleTime || 0) >
          this.configService.retryDelayTime * 3 + Math.random() * 2500
      ) {
        this.isOwnVoiceActive = true;
        this.lastVoiceToggleKey = 'talk';
        this.lastVoiceToggleTime = nowTime;
      }

      this.updateOwnStateSub.next(this.isOwnVoiceActive);
    }
  }

  getApiHealthcheck() {
    return new Promise(async (resolve, reject) => {
      const backendConfig = await this.configService.getBackendConfig();
      firstValueFrom(
        this.http.get(backendConfig.BACKEND_URL + '/api/v1/healthcheck', {
          withCredentials: true,
        })
      )
        .then((result) => resolve(result))
        .catch((err) => reject(err));
    });
  }

  updateNearbyPlayers(data: any) {
    if (!(data?.players == undefined)) {
      for (const player of data.players) {
        if (player?.id == undefined || !player?.id?.length) {
          continue;
        }
        if (this.nearbyPlayers[player.id] == undefined) {
          this.nearbyPlayers[player.id] = player;
          if (this.cachedPlayerRandomIDs[player.id] == undefined) {
            const rndName = `Player #${
              this.lastPlayerRandomID++ % this.configService.maxRandomPlayerID
            }`;
            this.nearbyPlayers[player.id].randomPlayerName = rndName;
            this.cachedPlayerRandomIDs[player.id] = rndName;
          } else {
            this.nearbyPlayers[player.id].randomPlayerName =
              this.cachedPlayerRandomIDs[player.id];
          }
        } else {
          Object.assign(this.nearbyPlayers[player.id], player);
        }
        this.nearbyPlayers[player.id].lastUpdate = new Date().valueOf();
      }
      this.updatedNearbyPlayersSub.next(true);
    }
  }

  //////////////////// TARGET API MQTT CHANNEL ////////////////////////
  sendTryInitRTCHandshake(remotePlayer: Player, ownPlayerid: string) {
    this.mqttService.produceMessage({
      type: 'init-rtc-handshake',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: <Player>{
        serverid: environment.serverId,
        realm: this.ownPlayer.realm,
        id: ownPlayerid,
        clientid: this.ipcService.clientid,
      },
    });
  }

  sendResolveRTCHandshake(remotePlayer: Player, ownPlayerid: string) {
    this.mqttService.produceMessage({
      type: 'resolve-rtc-handshake',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: <Player>{
        serverid: environment.serverId,
        realm: this.ownPlayer.realm,
        id: ownPlayerid,
        clientid: this.ipcService.clientid,
      },
    });
  }

  sendRTCIceCandidate(data: any) {
    const dataBase64 = btoa(JSON.stringify(data));
    this.mqttService.produceMessage({
      type: 'send-ice-candidate',
      clientid: data.remote?.clientid,
      dataBase64,
    });
  }

  async sendRTCOffer(remotePlayer: Player, offer: RTCSessionDescriptionInit) {
    const offerBase64 = btoa(JSON.stringify(offer));
    this.mqttService.produceMessage({
      type: 'send-rtc-offer',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: this.ownPlayer,
      offerBase64,
    });
  }

  async sendRTCAnswer(remotePlayer: Player, answer: RTCSessionDescriptionInit) {
    const answerBase64 = btoa(JSON.stringify(answer));
    this.mqttService.produceMessage({
      type: 'send-rtc-answer',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: this.ownPlayer,
      answerBase64,
    });
  }

  async sendRTCClosing(remotePlayer: Player) {
    this.mqttService.produceMessage({
      type: 'send-rtc-closing',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: this.ownPlayer,
    });
  }

  async sendAudioCheck(remotePlayer: Player) {
    this.mqttService.produceMessage({
      type: 'audiocheck',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: this.ownPlayer,
    });
  }

  async sendReconnect(remotePlayer: Player) {
    this.mqttService.produceMessage({
      type: 'reconnect',
      clientid: remotePlayer.clientid,
      remote: remotePlayer,
      own: this.ownPlayer,
    });
  }
}
