import { Injectable } from '@angular/core';
import { VoiceService } from './voice.service';
import { WebRTCService } from '../webrtc/webrtc.service';
import { IPCService } from '../ipc/ipc.service';

@Injectable()
export class VoiceControllerService {
  constructor(
    private readonly voiceService: VoiceService,
    private readonly webrtcService: WebRTCService,
    private readonly ipcService: IPCService
  ) {}

  registerRoutes() {
    this.ipcService.initRtcHandshakeSub.subscribe((msg) => {
      this.webrtcService.receiveInitRTCHandshake(msg.data);
    });

    this.ipcService.resolveRtcHandshakeSub.subscribe((msg) => {
      this.webrtcService.receiveResolveRTCHandshake(msg.data);
    });

    this.ipcService.sendIceCandidateSub.subscribe((msg) => {
      const dataBase64 = msg.data?.dataBase64;
      if (dataBase64 == undefined) {
        return;
      }
      this.webrtcService.receiveIceCandidate(JSON.parse(atob(dataBase64)));
    });

    this.ipcService.sendRtcOfferSub.subscribe((msg) => {
      const data = msg.data;
      const offerBase64 = data?.offerBase64;
      if (offerBase64 == undefined) {
        return;
      }
      data.offer = JSON.parse(atob(offerBase64));
      this.webrtcService.receiveRtcOffer(msg.data);
    });

    this.ipcService.sendRtcAnswerSub.subscribe((msg) => {
      const data = msg.data;
      const answerBase64 = data?.answerBase64;
      if (answerBase64 == undefined) {
        return;
      }
      data.answer = JSON.parse(atob(answerBase64));
      this.webrtcService.receiveRtcAnswer(msg.data);
    });

    this.ipcService.sendRtcClosingSub.subscribe((msg) => {
      this.webrtcService.receiveRtcClosing(msg.data);
    });

    this.ipcService.sendAudioCheckSub.subscribe((msg) => {
      this.webrtcService.receiveAudioCheck(msg.data);
    });

    this.ipcService.sendReconnectSub.subscribe((msg) => {
      this.webrtcService.receiveReconnect(msg.data);
    });
  }
}
