import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config/config.service';
import { IPCService } from '../ipc/ipc.service';
import { Subject } from 'rxjs';
import {
  debounce,
  hasDebounceRef,
  stopDebounceRef,
} from '../../libraries/debounce.lib';
import { environment } from '../../../../environments/environment';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class UpdateService {
  latestAvailableVersion: string;

  get hasUpdateAvailable(): boolean {
    const sanitizedLocalVersion = this.appVersion?.replace('v', '');
    const sanitizedRemoteVersion = this.latestAvailableVersion?.replace('v', '');
    if (
      !(this.latestAvailableVersion == undefined) &&
      sanitizedLocalVersion !== sanitizedRemoteVersion
    ) {
      const localVersionSplit = sanitizedLocalVersion.split('.');
      const remoteVersionSplit = sanitizedRemoteVersion.split('.');
      for (let i = 0; i < localVersionSplit.length; i++) {
        if (localVersionSplit[i] > remoteVersionSplit[i]) {
          return false;
        }
        if (localVersionSplit[i] < remoteVersionSplit[i]) {
          return true;
        }
      }
    }
    return false;
  }

  get appVersion(): string {
    return this.ipcService.getAppVersion();
  }

  constructor(
    private readonly configService: ConfigService,
    private readonly http: HttpClient,
    private readonly ipcService: IPCService
  ) {}

  async fetchLatestClientInfo(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const params: any = {};
      if (environment.staging) {
        params.mode = 'staging';
      }
      firstValueFrom(
        this.http.get(environment.updateServer + '/api/v1/media/latest', {
          withCredentials: true,
          params,
        })
      )
        .then((result) => {
          resolve(result);
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  async clientUpdateCheckCycle(overrideCycle?: number) {
    debounce(
      'dashboard-fetchLatestClientInfo',
      overrideCycle || 2500,
      async () => {
        const result = await this.fetchLatestClientInfo().catch((err) => {
          if (!environment.production) {
            console.error(err);
          }
        });
        if (result?.version == undefined) {
          this.latestAvailableVersion = undefined;
        } else {
          this.latestAvailableVersion = `v${result?.version}`;
        }
        this.clientUpdateCheckCycle(60000);
      }
    );
  }

  async openUpdateLink() {
    debounce('onDownloadUpdate', 100, () => {
      this.ipcService.openExternalBrowserLink(
        environment.updateServer +
          '/#/downloads/list' +
          (environment.staging ? '?mode=staging' : '')
      );
    });
  }
}
