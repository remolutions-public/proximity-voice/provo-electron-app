import { Injectable } from '@angular/core';
import { ConfigService } from '../config/config.service';
import { VoiceService } from '../voice/voice.service';
import { MediaService } from '../media/media.service';
import { Player } from '../../models/player.model';
import { RTCSession } from '../../models/rtc.model';
import { WebRTCRole } from '../../enums/webrtc-role.enum';
import { WebRTCState } from '../../enums/webrtc-state.enum';
import { copyObjFilterAttributes } from '../../libraries/obj.lib';
import { distance3DTo } from '../../libraries/distance.lib';
import {
  debounce,
  hasDebounceRef,
  stopDebounceRef,
} from '../../libraries/debounce.lib';
import { sleep } from '../../libraries/sleep.lib';
import { NotificationService } from '../toast-notification/toast-notification.service';
import { StorageService } from '../storage/storage.service';

@Injectable()
export class WebRTCService {
  static wasInit = false;
  lastDisconnects = {};

  lastCoturnData: any;
  lastSelectedCoturn: any;

  activeConnectingCount: number = 0;

  constructor(
    private readonly configService: ConfigService,
    private readonly voiceService: VoiceService,
    private readonly mediaService: MediaService,
    private readonly note: NotificationService,
    private readonly storageService: StorageService
  ) {
    this.init();

    this.mediaService.inputDevicesChangedSub.subscribe(async (devicesObj) => {
      this.reconnectAllPlayers(true);
    });

    this.mediaService.remoteAudioInputMeasurementFailedSub.subscribe(
      (player: Player) => {
        this.clearWebRTCSession(player);
      }
    );
  }

  init() {
    if (!WebRTCService.wasInit) {
      WebRTCService.wasInit = true;
      this.voiceService.updateOwnStateSub.subscribe(async () => {
        if (this.mediaService.isInitPhase) {
          this.configService.debugLog(
            'info',
            'is in init phase, skipping connection attempts...'
          );
          return;
        }

        if (
          this.voiceService.isOwnVoiceActive &&
          !this.configService.isGlobalMuted
        ) {
          stopDebounceRef('updateOwnStateSub-muteOwnAudio');
          this.mediaService.activateOwnAudio(this.voiceService.ownPlayer);
        } else {
          if (!hasDebounceRef('updateOwnStateSub-muteOwnAudio')) {
            debounce(
              'updateOwnStateSub-muteOwnAudio',
              this.configService.pressedButtonReleaseDelay * 0.5,
              () => {
                this.mediaService.muteOwnAudio(this.voiceService.ownPlayer);
              }
            );
          }
        }

        if (
          this.voiceService.isOwnVoiceActive &&
          this.configService.isGlobalMuted
        ) {
          this.note.showNotification(
            'default',
            undefined,
            'NOTIFICATIONS.AUDIO_MUTED_WARNING'
          );
        }

        for (const playerid of Object.keys(this.voiceService.nearbyPlayers)) {
          const remotePlayer: Player =
            this.voiceService.nearbyPlayers[playerid];
          if (
            !(remotePlayer?.rtc?.dataSendChannel == undefined) &&
            remotePlayer.rtc.dataSendChannel.readyState === 'open'
          ) {
            remotePlayer.rtc.dataSendChannel.send(
              JSON.stringify({
                type: 'keypress',
                key: this.voiceService.lastVoiceToggleKey,
                pressed: this.voiceService.isOwnVoiceActive,
              })
            );
          } else {
            // retry to create a data channel
            const result =
              (await this.createRTCDataChannel(remotePlayer).catch()) || false;
            if (
              !result &&
              !(remotePlayer?.rtc == undefined) &&
              remotePlayer?.rtc?.state !== WebRTCState.CONNECTING &&
              remotePlayer?.rtc?.state !== WebRTCState.CONNECTED
            ) {
              this.configService.debugLog(
                'warn',
                `updateOwnStateSub, dataSendChannel is broken, reconnecting...`,
                remotePlayer
              );
              remotePlayer.rtc.state = WebRTCState.FAILED;
              this.clearWebRTCSession(remotePlayer);
            }
          }
        }

        if (this.voiceService?.ownPlayer?.id == undefined) {
          this.voiceService.ownPlayer.pos = undefined;
          const playerids = [];
          for (const playerid of Object.keys(this.voiceService.nearbyPlayers)) {
            const remotePlayer = this.voiceService.nearbyPlayers[playerid];
            if (!(remotePlayer == undefined)) {
              playerids.push(playerid);
              if (!(remotePlayer?.rtc == undefined)) {
                remotePlayer.rtc.state = WebRTCState.DISCONNECTED;
              }
              this.clearWebRTCSession(remotePlayer);
            }
          }
          for (const playerid of playerids) {
            delete this.voiceService.nearbyPlayers[playerid];
          }
        }
      });

      this.voiceService.reconnectToAllSub.subscribe((state) => {
        if (
          new Date().valueOf() - this.voiceService.lastReconnectAllTriggerTime >
          this.voiceService.reconnectAllTriggerCooldown
        ) {
          this.voiceService.lastReconnectAllTriggerTime = new Date().valueOf();
          this.reconnectAllPlayers(state);
        }
      });

      this.voiceService.updatedNearbyPlayersSub.subscribe(() => {
        // we want to prevent
        if (hasDebounceRef('updatedNearbyPlayersSub')) {
          return;
        }
        debounce(
          'updatedNearbyPlayersSub',
          Math.random() * 500 + 1000,
          () => {}
        );
        this.activeConnectingCount = 0;
        const playerids = this.sortNearbyPlayerIDsByClosestFirst();
        for (const playerid of playerids) {
          const player: Player = this.voiceService.nearbyPlayers[playerid];
          const playerDistance = distance3DTo(
            player.pos,
            this.voiceService.ownPlayer.pos
          );

          if (
            !(player == undefined) &&
            player.volumeLevelSetting == undefined
          ) {
            const playerStorageObj = this.storageService.get(player.id);
            if (!(playerStorageObj == undefined)) {
              if (playerStorageObj.volumeLevelSetting == undefined) {
                player.volumeLevelSetting = 1;
              } else {
                player.volumeLevelSetting = Math.max(
                  0,
                  Math.min(1, parseFloat(playerStorageObj.volumeLevelSetting))
                );
              }
            }
          }

          const nowTime = new Date().valueOf();
          if (
            nowTime - player.lastUpdate >
            this.configService.autoDisconnectTime
          ) {
            // PRUNE OUT EXPIRED SESSIONS
            if (this.hasValidWebRTCSession(player)) {
              if (!(player?.rtc == undefined)) {
                player.rtc.state = WebRTCState.TIMEOUT;
              }
              this.clearWebRTCSession(player);
            }

            this.configService.debugLog(
              'info',
              `player ${player.id} timed out, disconnecting...`
            );

            delete this.voiceService.nearbyPlayers[playerid];
          } else if (
            playerDistance >
              parseFloat(
                (this.configService.voiceBaseSettings
                  ?.maxVoiceConnectionDistance || 0) as any
              ) ||
            playerDistance === -1
          ) {
            // PRUNE OUT SESSIONS THAT ARE TOO FAR AWAY
            if (this.hasValidWebRTCSession(player)) {
              if (!(player?.rtc == undefined)) {
                player.rtc.state = WebRTCState.DISCONNECTED;
              }
              this.clearWebRTCSession(player);

              this.configService.debugLog(
                'info',
                `player ${player.id} is out of range, disconnecting...`
              );
            }

            delete this.voiceService.nearbyPlayers[playerid];
          } else if (this.hasValidWebRTCSession(player)) {
            const remoteAudioStream =
              player?.rtc?.remoteAudioStream?.getAudioTracks()?.[0];
            // when remote audio was not attached correctly, check if remote player is currently trying to talk and has a valid audio input
            // if remote player does indeed currently talk, the probability of a failed connection is high and we want to trigger a reconnect
            if (
              remoteAudioStream == undefined &&
              player?.rtc?.state === WebRTCState.CONNECTED
            ) {
              this.voiceService.sendAudioCheck(player);
            }
          }

          if (
            !this.hasValidWebRTCSession(player) &&
            (this.lastDisconnects[player.id] == undefined ||
              nowTime - this.lastDisconnects[player.id] >
                this.configService.disconnectCooldownTime)
          ) {
            if (!this.mediaService.isInitPhase) {
              // increase propability of one side being first, so that not both start at the same time
              // should not be same interval as player state update speed, so it needs to be max. 499ms
              debounce(
                'startRTCHandshake-' + player.id,
                Math.random() * 475,
                async () => {
                  switch (player?.rtc?.state) {
                    case WebRTCState.CALLING:
                    case WebRTCState.CONNECTING:
                      this.activeConnectingCount++;
                    default:
                      if (
                        this.activeConnectingCount <=
                        this.configService
                          .maxAllowedSimultaneousConnectionAttempts
                      ) {
                        const isCalling = await this.startRTCHandshake(player);
                        if (isCalling) {
                          this.activeConnectingCount++;
                        }
                      }
                  }
                }
              );
            }
          }
        }
      });
    }
  }

  reconnectAllPlayers(isInputDeviceChange?: boolean) {
    this.note.showNotification(
      'info',
      undefined,
      'NOTIFICATIONS.RECONNECTING_ALL'
    );

    debounce('reconnectAllPlayers', 500, async () => {
      for (const playerid of Object.keys(this.voiceService.nearbyPlayers)) {
        this.clearWebRTCSession(this.voiceService.nearbyPlayers[playerid]);
        // stagger reconnections, it should not reconnect all at the same time...
        await sleep(100);
      }
      this.mediaService.ownAudioStream = undefined;
      this.mediaService.prepareOwnAudioStream(this.voiceService.ownPlayer);
      if (isInputDeviceChange) {
        this.note.showNotification(
          'warning',
          undefined,
          'NOTIFICATIONS.INPUT_DEVICE_CHANGED'
        );
        console.warn(
          'input devices change detected, reconnecting all nearby players...'
        );
      }
    });
  }

  clearWebRTCSession(player: Player) {
    if (player?.rtc == undefined) {
      return;
    }

    if (
      player?.rtc?.state === WebRTCState.CALLING ||
      player?.rtc?.state === WebRTCState.CONNECTING
    ) {
      return;
    }

    this.voiceService.sendRTCClosing(player);

    if (!(player?.rtc?.dataSendChannel == undefined)) {
      const dataChannelRef = player.rtc.dataSendChannel;
      debounce('remove-dataChannelRef-' + player.id, 50, () => {
        dataChannelRef.close();
      });
    }

    this.mediaService.removeTrackFromHTMLElement(player);

    if (!(player?.rtc?.remoteAudioApi == undefined)) {
      const audioApi = player.rtc?.remoteAudioApi;
      audioApi?.mediaStreamSourceNode?.disconnect();
      audioApi?.analyserNode?.disconnect();
      audioApi?.gainNode?.disconnect();
      audioApi?.pannerNode?.disconnect();
      audioApi?.audioContext?.close();
      player.rtc.remoteAudioApi = undefined;
    }

    if (!(player?.rtc?.remoteAudioStream == undefined)) {
      const tracks = player?.rtc?.remoteAudioStream?.getAudioTracks();
      if (!(tracks == undefined)) {
        for (const track of tracks) {
          track.stop();
          player.rtc.remoteAudioStream.removeTrack(track);
        }
      }
      player.rtc.remoteAudioStream == undefined;
    }

    try {
      if (
        !(player?.rtc?.pc == undefined) &&
        player.rtc.pc.signalingState !== 'closed'
      ) {
        const senders = player.rtc.pc.getSenders();
        for (const sender of senders) {
          player.rtc.pc.removeTrack(sender);
        }
        player.rtc.pc.close();
        player.rtc.pc = undefined;
      }
    } catch (e) {
      console.error(e);
    }

    player.rtc = undefined;

    this.lastDisconnects[player.id] = new Date().valueOf();
  }

  hasValidWebRTCSession(player: Player) {
    const nowTime = new Date().valueOf();
    if (player?.rtc == undefined || player?.clientid == undefined) {
      return false;
    } else if (
      !(player?.rtc == undefined) &&
      player.rtc.state !== WebRTCState.CONNECTED &&
      player.rtc.state !== WebRTCState.CONNECTING &&
      nowTime - player.rtc.sessionStart > this.configService.retryDelayTime
    ) {
      this.configService.debugLog(
        'info',
        `rtc initVoice with state ${player.rtc.state} timed out, retrying...`
      );
      if (!(player?.rtc == undefined)) {
        player.rtc.state = WebRTCState.TIMEOUT;
      }
      this.clearWebRTCSession(player);
      return false;
    } else if (
      !(player?.rtc == undefined) &&
      player.rtc.state === WebRTCState.CONNECTING &&
      nowTime - player.rtc.sessionStart > this.configService.autoDisconnectTime
    ) {
      this.configService.debugLog(
        'info',
        `rtc connection attempt failed, disconnecting...`
      );
      if (!(player?.rtc == undefined)) {
        player.rtc.state = WebRTCState.TIMEOUT;
      }
      this.clearWebRTCSession(player);
      return false;
    }
    return true;
  }

  async startRTCHandshake(remotePlayer: Player): Promise<boolean> {
    let isCalling = false;
    await this.mediaService.prepareOwnAudioStream(this.voiceService.ownPlayer);

    if (remotePlayer?.rtc == undefined) {
      this.lastCoturnData = await this.configService.getCoturnData().catch();
      remotePlayer.rtc = new RTCSession();
      remotePlayer.rtc.state = WebRTCState.CALLING;
      remotePlayer.rtc.sessionStart = new Date().valueOf();
      remotePlayer.rtc.role = WebRTCRole.HOST;
      this.configService.debugLog('info', '** ASSUME HOST **');

      // sanitize out possible rtc circular obj references
      const rPlayer = copyObjFilterAttributes(remotePlayer, 'rtc');
      this.voiceService.sendTryInitRTCHandshake(
        rPlayer,
        this.voiceService.ownPlayer.id
      );
      isCalling = true;
    }

    if (this.lastCoturnData == undefined) {
      this.lastCoturnData = await this.configService.getCoturnData().catch();
    }
    return isCalling;
  }

  async receiveInitRTCHandshake(data: any) {
    const remoteData: Player = data.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    if (remotePlayer == undefined) {
      // when remoteData is invalid, it will not be able to find or create a remotePlayer object.
      // Since there is not remotePlayer object, it is not possible to set the WebRTCState, so bailing is the only option...
      return;
    }
    const ownData = data.remote;

    if (remotePlayer?.rtc == undefined) {
      remotePlayer.rtc = new RTCSession();
      remotePlayer.rtc.sessionStart = new Date().valueOf();
    }
    remotePlayer.rtc.state = WebRTCState.CONNECTING;
    remotePlayer.rtc.role = WebRTCRole.CLIENT;
    this.configService.debugLog('info', '** IS CLIENT **');

    const result =
      (await this.createPeerConnection(remotePlayer).catch()) || false;
    if (result) {
      const valid =
        (await this.registerWebRTCSignalingEvents(remotePlayer).catch()) ||
        false;
      if (valid) {
        await this.mediaService
          .attachOwnAudioTrack(remotePlayer, this.voiceService.ownPlayer)
          .catch();

        // sanitize out possible rtc circular obj references
        const rPlayer = copyObjFilterAttributes(remotePlayer, 'rtc');
        this.voiceService.sendResolveRTCHandshake(
          rPlayer,
          this.voiceService.ownPlayer.id
        );
      }
    }
  }

  async receiveResolveRTCHandshake(data: any) {
    const remoteData: Player = data?.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    const ownData = data.remote;

    const result =
      (await this.createPeerConnection(remotePlayer).catch()) || false;
    if (result) {
      const valid =
        (await this.registerWebRTCSignalingEvents(remotePlayer).catch()) ||
        false;
      if (valid) {
        await this.mediaService
          .attachOwnAudioTrack(remotePlayer, this.voiceService.ownPlayer)
          .catch();
        await this.createRTCDataChannel(remotePlayer).catch();
        this.hostStartRTCConnection(remotePlayer).catch();
      }
    }
  }

  async receiveIceCandidate(data: any) {
    const remoteData: Player = data?.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    const ownData = data?.remote;

    if (remotePlayer == undefined) {
      this.configService.debugLog(
        'warn',
        '[receiveIceCandidate] remote player is not valid, skipping...'
      );
      return;
    }

    this.configService.debugLog(
      'info',
      'receive icecandidate - (raw) targetSocketid: ' + remotePlayer.socketid
    );

    const iceCandidate = data.iceCandidate;
    await this.appendWebRTCIceCandidate(remotePlayer, iceCandidate).catch();
  }

  async receiveRtcOffer(data: any) {
    const remoteData: Player = data?.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    const ownData = data.remote;

    if (remotePlayer == undefined) {
      this.configService.debugLog(
        'warn',
        '[receiveRtcOffer] remotePlayer is invalid, skipping...'
      );
      return;
    }

    await this.attachRemoteWebRTCOfferDescription(
      remotePlayer,
      data.offer
    ).catch((err) => {
      this.configService.debugLog('error', err);
    });
    const answer = await this.createClientRTCAnswer(remotePlayer).catch();
    if (!(answer == undefined)) {
      // sanitize out possible rtc circular obj references
      const rPlayer = copyObjFilterAttributes(remotePlayer, 'rtc');
      await this.attachLocalWebRTCAnswerDescription(
        remotePlayer,
        answer
      ).catch();
      this.voiceService.sendRTCAnswer(rPlayer, answer).catch();
    }
  }

  async receiveRtcAnswer(data: any) {
    const remoteData: Player = data?.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    const ownData = data.remote;

    this.attachRemoteWebRTCAnswerDescription(remotePlayer, data.answer).catch();
  }

  async hostStartRTCConnection(remotePlayer: Player) {
    if (remotePlayer.rtc == undefined) {
      this.configService.debugLog(
        'warn',
        '[hostStartRTCConnection], remotePlayer.rtc obj is invalid!'
      );
    }

    if (remotePlayer.rtc.role !== WebRTCRole.HOST) {
      this.configService.debugLog(
        'warn',
        '[hostStartRTCConnection], bailing since role is not host!'
      );
      return;
    }
    if (remotePlayer.rtc.state !== WebRTCState.CONNECTING) {
      this.configService.debugLog(
        'info',
        `##### hostStartRTCConnection - changing state from ${remotePlayer.rtc.state} to ${WebRTCState.CONNECTING} #####`
      );
      remotePlayer.rtc.state = WebRTCState.CONNECTING;
    }

    const offer = await this.createHostRTCOffer(remotePlayer).catch();
    if (!(offer == undefined)) {
      // sanitize out possible rtc circular obj references
      const rPlayer = copyObjFilterAttributes(remotePlayer, 'rtc');
      await this.attachLocalWebRTCOfferDescription(remotePlayer, offer).catch();
      this.voiceService.sendRTCOffer(rPlayer, offer).catch();
    }
  }

  async createPeerConnection(remotePlayer: Player): Promise<boolean> {
    try {
      if (remotePlayer == undefined || remotePlayer.rtc == undefined) {
        this.configService.debugLog(
          'warn',
          '[createPeerConnection] remotePlayer or rtc obj is invalid, skipping...'
        );
        return false;
      }

      const pcConfig: RTCConfiguration = { iceServers: [] };
      const config = await this.getNextRandomIceServerConfig(
        this.lastCoturnData
      ).catch();

      if (config?.stun == undefined || config?.turn == undefined) {
        this.configService.debugLog(
          'warn',
          '[createPeerConnection] no iceServers available, skipping...'
        );
        return false;
      }

      pcConfig.iceServers.push(config.stun, config.turn);

      const pc = new RTCPeerConnection(pcConfig);
      remotePlayer.rtc.pc = pc;
      remotePlayer.rtc.pcConfig = pcConfig;

      // await sleep(10);
      return true;
    } catch (e) {
      this.configService.debugLog(
        'error',
        `[createPeerConnection] Error creating PeerConnection: ${e.message}`
      );
      return false;
    }
  }

  private async getNextRandomIceServerConfig(coturn: any) {
    if (coturn?.data == undefined || !coturn?.data?.length) {
      this.lastCoturnData = await this.configService.getCoturnData().catch();
      coturn = this.lastCoturnData;
      if (coturn == undefined) {
        this.configService.debugLog(
          'warn',
          '[getNextRandomIceServerConfig] mqtt is not connected, skipping...'
        );
        return;
      }
    }

    let rndIndex =
      Math.random() < 0.5
        ? Math.floor(coturn.data.length - Math.random() * coturn.data.length)
        : Math.floor(Math.random() * coturn.data.length);
    // try reselecting previous coturn, so that one client does not use multiple coturn connections
    // especially when the client cannot use p2p connections, but instead uses turn as a voice proxy
    if (!(this.lastSelectedCoturn == undefined)) {
      for (let i = 0; i < this.lastCoturnData.length; i++) {
        const entry = this.lastCoturnData[i];
        if (
          entry.ip === this.lastSelectedCoturn.ip &&
          entry.tlsport === this.lastSelectedCoturn.tlsport &&
          entry.port === this.lastCoturnData.port
        ) {
          rndIndex = i;
          break;
        }
      }
    }

    const rndEntry = coturn.data[rndIndex];
    this.lastSelectedCoturn = rndEntry;

    const stun: any = {
      urls: [],
    };
    const turn: any = {
      urls: [],
    };
    if (!(rndEntry?.ip == undefined) && rndEntry?.ip?.length) {
      if (!(rndEntry.tlsport == undefined) && rndEntry.tlsport?.length) {
        stun.urls.push(`stun:${rndEntry.ip}:${rndEntry.tlsport}`);
        turn.urls.push(`turn:${rndEntry.ip}:${rndEntry.tlsport}`);
      } else if (!(rndEntry.port == undefined) && rndEntry.port?.length) {
        stun.urls.push(`stun:${rndEntry.ip}:${rndEntry.port}`);
        turn.urls.push(`turn:${rndEntry.ip}:${rndEntry.port}`);
      } else {
        stun.urls.push(`stun:${rndEntry.ip}`);
        turn.urls.push(`turn:${rndEntry.ip}`);
      }
    }
    turn.username = `${rndEntry.user}@${rndEntry.realm}`;
    turn.credential = rndEntry.pw;

    return {
      stun,
      turn,
    };
  }

  async registerWebRTCSignalingEvents(remotePlayer: Player): Promise<boolean> {
    let hasValidPeerConnection = false;
    if (!(remotePlayer?.rtc == undefined)) {
      if (
        remotePlayer?.rtc?.pc == undefined &&
        remotePlayer.rtc.role === WebRTCRole.CLIENT
      ) {
        // fallback create pc if not existing
        hasValidPeerConnection = await this.createPeerConnection(
          remotePlayer
        ).catch();
      } else if (!(remotePlayer?.rtc?.pc == undefined)) {
        hasValidPeerConnection = true;
      }

      if (hasValidPeerConnection) {
        const pc: RTCPeerConnection = remotePlayer.rtc.pc;
        pc.onicecandidate = (e) =>
          this.handleIceCandidate(e, remotePlayer).catch();
        pc.ontrack = (e) =>
          this.mediaService.handleOnRemoteTrack(e, remotePlayer).catch();
        pc.ondatachannel = (e) =>
          this.handleOnDataChannel(e, remotePlayer).catch();
        pc.onconnectionstatechange = (e) =>
          this.handleConnectionStateChange(e, remotePlayer).catch();
      } else {
        this.configService.debugLog(
          'warn',
          '[registerWebRTCSignalingEvents] peer connection is invalid, resetting connection...'
        );
        if (!(remotePlayer?.rtc == undefined)) {
          remotePlayer.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(remotePlayer);
      }
    }
    return hasValidPeerConnection;
  }

  getRemotePlayer(remoteData: any) {
    let remotePlayer = this.voiceService.nearbyPlayers[remoteData.id];
    if (remotePlayer == undefined && !(remoteData?.id == undefined)) {
      remotePlayer = {
        id: remoteData.id,
        name: remoteData.name,
        lastUpdate: new Date().valueOf(),
      };
      this.voiceService.nearbyPlayers[remoteData.id] = remotePlayer;
    }
    return remotePlayer;
  }

  async appendWebRTCIceCandidate(
    remotePlayer: Player,
    iceCandidate: RTCIceCandidate
  ) {
    if (remotePlayer?.rtc?.pc == undefined) {
      this.configService.debugLog(
        'warn',
        '[appendWebRTCIceCandidate] webrtc obj has no valid RTCPeerConnection obj, skipping...'
      );
      return;
    }
    if (iceCandidate == undefined) {
      this.configService.debugLog('info', 'End of IceCandidates');
      return;
    }
    remotePlayer.rtc.pc
      .addIceCandidate(new RTCIceCandidate(iceCandidate))
      .catch((err: any) => this.configService.debugLog('error', err));
  }

  async createHostRTCOffer(remotePlayer: Player) {
    if (remotePlayer?.rtc?.pc == undefined) {
      throw new Error(
        '[createHostRTCOffer] cannot create offer, webrtc obj is invalid'
      );
    }
    if (remotePlayer?.rtc?.pc?.signalingState === 'closed') {
      throw new Error('cannot create offer, signalingState is closed');
    } else {
      const offer = (await remotePlayer.rtc.pc
        .createOffer(this.mediaService.audioOptions)
        .catch((err: any) => {
          this.configService.debugLog('error', err);
          this.configService.debugLog(
            'warn',
            '[createHostRTCOffer] resetting connection...'
          );
          if (!(remotePlayer?.rtc == undefined)) {
            remotePlayer.rtc.state = WebRTCState.FAILED;
          }
          this.clearWebRTCSession(remotePlayer);
        })) as RTCSessionDescriptionInit;
      return offer;
    }
  }

  async attachLocalWebRTCOfferDescription(
    remotePlayer: Player,
    offer: RTCSessionDescriptionInit
  ) {
    if (!(offer == undefined) && !(remotePlayer?.rtc == undefined)) {
      await remotePlayer.rtc.pc.setLocalDescription(offer).catch((err: any) => {
        this.configService.debugLog('error', err);
        this.configService.debugLog(
          'warn',
          '[attachLocalWebRTCOfferDescription] resetting connection...'
        );
        if (!(remotePlayer?.rtc == undefined)) {
          remotePlayer.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(remotePlayer);
      });
    }
  }

  async attachRemoteWebRTCOfferDescription(
    remotePlayer: Player,
    offer: RTCSessionDescriptionInit
  ) {
    if (remotePlayer?.rtc?.pc == undefined) {
      this.configService.debugLog(
        'warn',
        '[attachRemoteWebRTCOfferDescription] webrtc obj has no valid RTCPeerConnection obj, skipping...'
      );
      return;
    }
    if (offer == undefined) {
      throw new Error('offer obj is undefined');
    }

    await remotePlayer.rtc.pc
      .setRemoteDescription(new RTCSessionDescription(offer))
      .catch((err: any) => {
        this.configService.debugLog('error', err);
        this.configService.debugLog(
          'warn',
          '[attachRemoteWebRTCOfferDescription] resetting connection...'
        );
        if (!(remotePlayer?.rtc == undefined)) {
          remotePlayer.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(remotePlayer);
      });
  }

  async createClientRTCAnswer(remotePlayer: Player) {
    if (remotePlayer?.rtc?.pc == undefined) {
      this.configService.debugLog(
        'warn',
        '[createClientRTCAnswer] webrtc obj has no valid RTCPeerConnection obj, skipping...'
      );
      return;
    }

    const answer = (await remotePlayer.rtc.pc
      .createAnswer(this.mediaService.audioOptions)
      .catch((err: any) => {
        this.configService.debugLog('error', err);
        this.configService.debugLog(
          'warn',
          '[createClientRTCAnswer] resetting connection...'
        );
        if (!(remotePlayer?.rtc == undefined)) {
          remotePlayer.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(remotePlayer);
      })) as RTCSessionDescriptionInit;

    return answer;
  }

  async attachLocalWebRTCAnswerDescription(
    remotePlayer: Player,
    answer: RTCSessionDescriptionInit
  ) {
    if (remotePlayer?.rtc?.pc == undefined) {
      this.configService.debugLog(
        'warn',
        '[attachLocalWebRTCAnswerDescription] has no valid RTCPeerConnection obj, skipping...'
      );
      return;
    }
    await remotePlayer.rtc.pc.setLocalDescription(answer).catch((err: any) => {
      this.configService.debugLog('error', err);
      this.configService.debugLog(
        'warn',
        '[attachLocalWebRTCAnswerDescription] resetting connection...'
      );
      if (!(remotePlayer?.rtc == undefined)) {
        remotePlayer.rtc.state = WebRTCState.FAILED;
      }
      this.clearWebRTCSession(remotePlayer);
    });
  }

  async attachRemoteWebRTCAnswerDescription(
    remotePlayer: Player,
    answer: RTCSessionDescriptionInit
  ) {
    if (remotePlayer?.rtc?.pc == undefined) {
      this.configService.debugLog(
        'warn',
        '[attachRemoteWebRTCAnswerDescription] has no valid RTCPeerConnection obj, skipping...'
      );
      return;
    }
    await remotePlayer.rtc.pc
      .setRemoteDescription(new RTCSessionDescription(answer))
      .catch((err: any) => {
        this.configService.debugLog('error', err);
        this.configService.debugLog(
          'warn',
          '[attachRemoteWebRTCAnswerDescription] resetting connection...'
        );
        if (!(remotePlayer?.rtc == undefined)) {
          remotePlayer.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(remotePlayer);
      });
  }

  async receiveRtcClosing(data: any) {
    const remoteData: Player = data.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    const ownData = data.remote;

    if (!(remotePlayer == undefined)) {
      if (!(remotePlayer?.rtc == undefined)) {
        remotePlayer.rtc.state = WebRTCState.DISCONNECTED;
      }
      this.clearWebRTCSession(remotePlayer);
      delete this.voiceService.nearbyPlayers[remotePlayer.id];
    }
  }

  async receiveAudioCheck(data: any) {
    const remoteData: Player = data.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    if (
      this.voiceService.isOwnVoiceActive &&
      this.voiceService.ownPlayer?.rtc?.ownAudioStream?.getAudioTracks()?.[0]
        ?.readyState === 'live' &&
      remotePlayer?.rtc?.state === WebRTCState.CONNECTED
    ) {
      // auto trigger a reconnect form remote side, since audio didn't get attached as expected
      this.voiceService.sendReconnect(remotePlayer);
    }
  }

  async receiveReconnect(data: any) {
    const remoteData: Player = data.own || {};
    const remotePlayer = this.getRemotePlayer(remoteData);
    this.clearWebRTCSession(remotePlayer);
  }

  async createRTCDataChannel(
    player: Player,
    breakCounter = 0
  ): Promise<boolean> {
    if (
      !(player?.rtc == undefined) &&
      player?.rtc?.dataSendChannel == undefined
    ) {
      const retrying = async () => {
        if (breakCounter < 10) {
          // this.debugLog('warn',
          //   '[createRTCDataChannel] was not able to create a DataChannel, retrying...'
          // );
          await sleep(15);
          await this.createRTCDataChannel(player, breakCounter++).catch();
        }
      };
      try {
        if (!(player?.rtc?.pc == undefined)) {
          player.rtc.dataSendChannel = player.rtc.pc.createDataChannel(
            'audio-control',
            { ordered: false, maxRetransmits: 0 }
          );
          return true;
        } else {
          await retrying();
        }
      } catch (e) {
        await retrying();
      }

      if (breakCounter >= 3) {
        this.configService.debugLog(
          'warn',
          '[createRTCDataChannel] failed to create a DataChannel, resetting connection...'
        );
        if (!(player?.rtc == undefined)) {
          player.rtc.state = WebRTCState.FAILED;
        }
        this.clearWebRTCSession(player);
      }
    }
    return false;
  }

  sortNearbyPlayerIDsByClosestFirst(): string[] {
    if (!(this.voiceService?.nearbyPlayers == undefined)) {
      const sortedPlayerIds = Object.keys(this.voiceService.nearbyPlayers).sort(
        (a: string, b: string) => {
          const objA = this.voiceService.nearbyPlayers[a];
          const objB = this.voiceService.nearbyPlayers[b];
          const aDist = distance3DTo(objA.pos, this.voiceService.ownPlayer.pos);
          const bDist = distance3DTo(objB.pos, this.voiceService.ownPlayer.pos);
          if (aDist < bDist) return 1;
          if (aDist > bDist) return -1;
          return 0;
        }
      );
      return sortedPlayerIds;
    }
    return [];
  }

  /////// WebRTC Events ////////
  async handleIceCandidate(
    event: RTCPeerConnectionIceEvent,
    remotePlayer: Player
  ) {
    if (!(event?.candidate == undefined)) {
      // sanitize out possible rtc circular obj references
      const rPlayer = copyObjFilterAttributes(remotePlayer, 'rtc');
      this.voiceService.sendRTCIceCandidate({
        remote: rPlayer,
        own: this.voiceService.ownPlayer,
        iceCandidate: event.candidate,
      });
    } else {
      if (remotePlayer?.rtc == undefined) {
        this.configService.debugLog(
          'error',
          '[End of candidates] could not find webrtcObj'
        );
      } else {
        this.configService.debugLog('info', 'End of ice candidates');
      }
    }
  }

  async handleConnectionStateChange(event: Event, remotePlayer: Player) {
    if (event?.target?.['connectionState'] === 'connected') {
      remotePlayer.rtc.state = WebRTCState.CONNECTED;
    }
  }

  async handleOnDataChannel(event: RTCDataChannelEvent, player: Player) {
    if (
      !(player?.rtc == undefined) &&
      player?.rtc?.dataReceiveChannel == undefined
    ) {
      player.rtc.dataReceiveChannel = event.channel;
      player.rtc.dataReceiveChannel.onmessage = (e) =>
        this.handleReceiveDataChannelMessage(e, player);
    }
  }

  handleReceiveDataChannelMessage(e: any, player: Player) {
    const remotePlayer: Player = this.getRemotePlayer(player);

    if (!(remotePlayer == undefined)) {
      const data = JSON.parse(e.data);

      switch (data.type) {
        case 'keypress':
          if (remotePlayer?.inputKeys == undefined) {
            remotePlayer.inputKeys = {};
          }

          if (data.pressed) {
            stopDebounceRef('handleReceiveDataChannelMessage-muteRemoteAudio');
            this.mediaService
              .activateRemoteAudio(
                remotePlayer,
                this.voiceService.ownPlayer,
                this.voiceService.locallyKnownRemotePlayers,
                this.voiceService.lastLocallyKnownDataUpdate
              )
              .then((result: any) => {
                if (!(result == undefined)) {
                  for (const playerid of Object.keys(result)) {
                    result[playerid].time = new Date().valueOf();
                    this.voiceService.offsetCalculatedRemotePlayers[
                      result[playerid].id
                    ] = result[playerid];
                  }
                }
              });
            remotePlayer.inputKeys[data.key] = data.pressed;
            if (
              !(remotePlayer.lastKey == undefined) &&
              remotePlayer.lastKey !== data.key
            ) {
              remotePlayer.inputKeys[remotePlayer.lastKey] = false;
            }
            remotePlayer.lastKey = data.key;
            clearTimeout(remotePlayer.fallbackReleaseKeyRef);
            remotePlayer.fallbackReleaseKeyRef = setTimeout(() => {
              remotePlayer.inputKeys[remotePlayer.lastKey] = false;
            }, this.configService.pressedButtonReleaseDelay);
          } else {
            if (
              !hasDebounceRef('handleReceiveDataChannelMessage-muteRemoteAudio')
            ) {
              debounce(
                'handleReceiveDataChannelMessage-muteRemoteAudio',
                this.configService.pressedButtonReleaseDelay,
                () => {
                  this.mediaService.muteRemoteAudio(remotePlayer);
                  remotePlayer.inputKeys[data.key] = data.pressed;
                  clearTimeout(remotePlayer.fallbackReleaseKeyRef);
                }
              );
            }
          }
          break;
      }
    }
  }
}
