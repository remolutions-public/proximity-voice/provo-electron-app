import { Injectable } from '@angular/core';
import { MenuController } from '@ionic/angular';
import { Subject } from 'rxjs';

@Injectable()
export class SidebarService {
  public menuHidden = true;

  // is triggered by header.component; listened by sidebar.service and scene.component
  public nextMenuHiddenSub: Subject<boolean> = new Subject();

  constructor(private readonly menuController: MenuController) {
    this.nextMenuHiddenSub.subscribe(val => {
      this.menuHidden = val;
    });
  }

  disableMenuSwiping() {
    // disables drag in for sidebar in modes sm/is/xs
    this.menuController.swipeGesture(false, 'first');
  }
}
