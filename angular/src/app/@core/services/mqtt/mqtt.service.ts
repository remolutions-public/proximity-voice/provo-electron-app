import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounce } from '../../libraries/debounce.lib';
import { IPCService } from '../ipc/ipc.service';
import { Subject, firstValueFrom, catchError, Observable, of } from 'rxjs';
import * as protobufjs from 'protobufjs';
import * as mqtt from 'mqtt/dist/mqtt';
import { voiceBaseSettingsSchema } from '../../protobufModels/voice-base-settings';
import { NotificationService } from '../toast-notification/toast-notification.service';
import { StorageService } from '../storage/storage.service';
import { nearbyPlayersSchema } from '../../protobufModels/nearby-players';
import { playerDataSchema } from '../../protobufModels/player-data';
import { environment } from '../../../../environments/environment';
import { BackendConfig } from '../../models/backend.model';

export class ClientChannel {
  topicName: string;
  redererid: string;
  healthState: string = 'disconnected';
  latency = 999;
  lastLatencyTime: number;
}

@Injectable()
export class MqttService {
  private loggerContext = MqttService.name;
  private controllerMqttConnection: mqtt.MqttClient;
  private heartbeatIntervalMs = 2000;

  latencyTimeout = Math.max(5000, this.heartbeatIntervalMs * 2);
  healthState = 'disconnected';

  clientChannel: ClientChannel;

  controllerChannelName: string;

  playerHidingState: any = {};

  protobufRoot: protobufjs.Root;

  cachedMqttData: any;
  lastMqttDataPull: number;
  maxMqttDataCacheTime = 5000;

  voiceBaseSettingsSub: Subject<any> = new Subject();

  constructor(
    private readonly http: HttpClient,
    private readonly ipcService: IPCService,
    private readonly storageService: StorageService,
    private readonly note: NotificationService
  ) {
    this.loadProtobufSchemas();
    this.connectController();
    this.retryConnectController();

    this.ipcService.playerDataSub.subscribe((message) => {
      const data = JSON.parse(JSON.stringify(message.data));
      if (data == undefined) {
        return;
      }
      // sanitize obj, removing unnecessary attributes, to reduce network load
      if (!(data.vehicle == undefined)) {
        delete data.vehicle;
      }
      if (!(data.remotePlayers == undefined)) {
        delete data.remotePlayers;
      }
      if (!(data.rPlayer == undefined)) {
        delete data.rPlayer;
      }
      if (!(data.uVector == undefined)) {
        delete data.uVector;
      }
      if (!(data.audio == undefined)) {
        delete data.audio;
      }
      if (!(data.timestamp == undefined)) {
        delete data.timestamp;
      }
      if (!(data.isDebug == undefined)) {
        delete data.isDebug;
      }
      if (!(data.responseKey == undefined)) {
        delete data.responseKey;
      }

      if (data.type == undefined) {
        data.type = 'PlayerData';
      }

      data.version = this.ipcService.getAppVersion();
      data.lastPlayerHidingTime = this.ipcService.lastPlayerHidingTime;
      data.playerIsHidden = this.ipcService.playerIsHidden;

      const messageType = this.protobufRoot.lookupType('PlayerData');
      const protobufMsg = messageType.encode(data).finish();
      this.produceMessage({
        type: 'PlayerData',
        content: Buffer.from(protobufMsg).toString('base64'),
      });
    });
  }

  loadProtobufSchemas() {
    this.protobufRoot = protobufjs.parse(voiceBaseSettingsSchema).root;
    this.protobufRoot.add(protobufjs.parse(nearbyPlayersSchema).root);
    this.protobufRoot.add(protobufjs.parse(playerDataSchema).root);
  }

  async retryConnectController(forceConnect?: boolean) {
    if (forceConnect) {
      await this.connectController();
      this.retryConnectController();
    } else {
      debounce('retryConnectController', this.latencyTimeout, async () => {
        switch (this.healthState) {
          case 'healthy':
          case 'connecting':
          case 'connected':
            break;
          default:
            console.info(
              '[',
              this.loggerContext,
              '] connecting to mqtt broker...'
            );
            await this.connectController();
            this.retryConnectController();
        }
      });
    }
  }

  async connectController() {
    if (this.ipcService.clientid == undefined) {
      this.ipcService.retrieveRendererId();
      debounce('retry-mqtt-controller-connect', 250, () => {
        this.connectController();
      });
      return;
    }
    this.healthState = 'connecting';
    const mqttData = await this.getMqttData().catch();
    if (mqttData == undefined) {
      return;
    }

    const brokerTarget = mqttData.externalMqttHost;
    const mqttWsPort = mqttData.externalMqttWsPort;
    const user = mqttData.mqttUser;
    const pass = mqttData.mqttPass;
    this.controllerChannelName = mqttData.controllerChannel;

    if (this.controllerMqttConnection?.connected) {
      this.controllerMqttConnection.end();
    }

    const brokerUrl = `${brokerTarget}:${mqttWsPort}`;
    try {
      this.controllerMqttConnection = mqtt.connect(brokerUrl, {
        path: '/ws',
        username: user,
        password: pass,
      });
    } catch (e) {
      console.error(e);
      this.controllerMqttConnection = undefined;
      this.retryConnectController();
    }
    if (this.controllerMqttConnection == undefined) {
      return;
    }

    this.controllerMqttConnection.on('connect', () => {
      console.info('mqtt client connected');
      this.healthState = 'connected';
      this.connectClient();
    });
  }

  async connectClient() {
    if (this.ipcService.clientid == undefined) {
      this.ipcService.retrieveRendererId();
      debounce('retry-mqtt-client-connect', 250, () => {
        this.connectClient();
      });
      return;
    }
    if (this.controllerMqttConnection == undefined) {
      return;
    }
    switch (this.healthState) {
      case 'healthy':
      case 'connecting':
      case 'connected':
        break;
      default:
        return;
    }

    if (!(this.clientChannel == undefined)) {
      return;
    }

    const mqttData = await this.getMqttData().catch();
    if (mqttData == undefined) {
      return;
    }

    if (this.clientChannel == undefined) {
      this.clientChannel = <ClientChannel>{
        healthState: 'connecting',
      };
    }

    this.clientChannel.topicName = 'client-' + this.ipcService.clientid;
    this.controllerMqttConnection.on(
      'message',
      this.onControllerMqttConnectionMessage
    );
    this.controllerMqttConnection.subscribe(
      this.clientChannel.topicName,
      (err) => {
        if (!(err == undefined)) {
          console.error('[Creating Client Subscription]', err);
          this.healthState = 'failed';
          this.retryConnectController();
        } else {
          this.clientChannel.healthState = 'connected';
          this.sendClientHeartbeat(this.ipcService.clientid);
        }
      }
    );
  }

  onControllerMqttConnectionMessage: mqtt.OnMessageCallback = (
    topic,
    message
  ) => {
    if (topic === this.clientChannel?.topicName) {
      this.clientReceiver(topic, message);
    }
  };

  async clientReceiver(topic: string, msg: Buffer) {
    try {
      if (!(msg == undefined)) {
        let messageType: protobufjs.Type;
        let message: protobufjs.Message<{}>;
        let b64content: Uint8Array;
        let decodedData: any;
        const data = JSON.parse(msg.toString());
        switch (data.type) {
          case 'heartbeat':
            this.clientChannel.healthState = 'healthy';
            this.clientChannel.latency = new Date().valueOf() - data.time || 0;
            this.ipcService.lastLatency = this.clientChannel.latency
              ? this.clientChannel.latency * 0.5
              : -1;
            this.ipcService.latencySub.next(this.ipcService.lastLatency);
            break;
          case 'VoiceBaseSettings':
            messageType = this.protobufRoot.lookupType('VoiceBaseSettings');
            b64content = new Uint8Array(Buffer.from(data.content, 'base64'));
            message = messageType.decode(b64content);
            decodedData = messageType.toObject(message);
            if (!(decodedData.maxDistance == undefined)) {
              const voiceBaseSettings = {
                type: 'voiceBaseSettingsCache',
                maxDistance: decodedData.maxDistance,
                voiceModeKeys: decodedData.voiceModeKeys,
                velocityOffsets: decodedData.velocityOffsets,
              };
              this.voiceBaseSettingsSub.next(voiceBaseSettings);
              this.ipcService.sendApiMessage(voiceBaseSettings);
            }
            break;
          case 'NearbyPlayers':
            messageType = this.protobufRoot.lookupType('NearbyPlayers');
            b64content = new Uint8Array(Buffer.from(data.content, 'base64'));
            message = messageType.decode(b64content);
            decodedData = messageType.toObject(message);
            this.ipcService.nearbyPlayersSub.next({ data: decodedData });
            break;
          case 'receive-coturn-data':
            this.ipcService.receiveCotrunDataSub.next({ data });
            break;
          case 'receive-maskingConfig':
            this.ipcService.receiveMaskingconfigSub.next({ data });
            break;
          ///////////////////// webRTC routes /////////////////////
          case 'init-rtc-handshake':
            this.ipcService.initRtcHandshakeSub.next({ data });
            break;
          case 'resolve-rtc-handshake':
            this.ipcService.resolveRtcHandshakeSub.next({ data });
            break;
          case 'send-ice-candidate':
            this.ipcService.sendIceCandidateSub.next({ data });
            break;
          case 'send-rtc-offer':
            this.ipcService.sendRtcOfferSub.next({ data });
            break;
          case 'send-rtc-answer':
            this.ipcService.sendRtcAnswerSub.next({ data });
            break;
          case 'send-rtc-closing':
            this.ipcService.sendRtcClosingSub.next({ data });
            break;
          case 'audiocheck':
            this.ipcService.sendAudioCheckSub.next({ data });
            break;
          case 'reconnect':
            this.ipcService.sendReconnectSub.next({ data });
            break;
          default:
            console.warn('uncatched mqtt clientReceiver event:', data);
        }
      }
    } catch (err) {
      console.error('[Client Receiver]', err);
    }
  }

  sendClientHeartbeat(clientid: string) {
    try {
      if (this.controllerMqttConnection == undefined) {
        console.error(
          clientid,
          'heartbeat failed, mqtt connection is undefined'
        );
        this.healthState = 'disconnected';
        this.retryConnectController();
        return;
      }

      if (!(this.controllerMqttConnection == undefined)) {
        this.controllerMqttConnection
          .publishAsync(
            this.controllerChannelName,
            Buffer.from(
              JSON.stringify({
                type: 'heartbeat',
                time: new Date().valueOf(),
                clientid: this.clientChannel.topicName,
              })
            )
          )
          .catch((err) => {
            console.error(err);
          });
        if (
          this.clientChannel.lastLatencyTime > 0 &&
          new Date().valueOf() - this.clientChannel.lastLatencyTime >
            this.latencyTimeout
        ) {
          this.healthState = 'timeout';
          this.clientChannel.healthState = 'timeout';
          this.retryConnectController();
        } else {
          debounce(
            'sendClientHeartbeat-' + clientid,
            this.heartbeatIntervalMs + Math.random() * 10,
            () => {
              this.sendClientHeartbeat(clientid);
            }
          );
        }
      }
    } catch (err) {
      console.info('[Client Send Heartbeat] ', err);
    }
  }

  async produceMessage(data: any) {
    if (data == undefined) {
      return;
    }
    try {
      if (!(this.controllerMqttConnection == undefined)) {
        if (data.clientid == undefined) {
          data.clientid = this.ipcService.clientid;
        }
        this.controllerMqttConnection.publish(
          this.controllerChannelName,
          Buffer.from(JSON.stringify(data))
        );
      }
    } catch (err) {
      console.error(`[Client MQTT Signaling - ${data?.type}]`, err);
      this.failedMqttConnection();
    }
  }

  async getMqttData(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      if (
        !(this.cachedMqttData == undefined) &&
        new Date().valueOf() - this.lastMqttDataPull < this.maxMqttDataCacheTime
      ) {
        return resolve(this.cachedMqttData);
      }
      const backendConfig = await this.getBackendConfig();
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/json');
      headers = headers.set('secret', backendConfig?.BACKEND_SECRET);
      let errorState: any;
      let mqttData: any;
      const sub = this.http
        .get(backendConfig?.BACKEND_URL + '/api/v1/mqtt', {
          headers,
        })
        .subscribe({
          error: (err) => {
            sub.unsubscribe();
            errorState = err;
            if (!environment.production) {
              console.error(errorState);
            }
            this.failedMqttConnection();
            this.note.showNotification(
              'error',
              undefined,
              'NOTIFICATIONS.WRONG_BACKEND_CONFIG'
            );
            resolve(undefined);
          },
          next: (value) => {
            mqttData = value;
          },
          complete: () => {
            sub.unsubscribe();
            if (!(mqttData == undefined)) {
              this.lastMqttDataPull = new Date().valueOf();
              this.cachedMqttData = mqttData;
            }
            resolve(this.cachedMqttData);
          },
        });
    });
  }

  failedMqttConnection() {
    try {
      this.clientChannel = undefined;
      this.ipcService.lastLatency = -1;
      this.ipcService.latencySub.next(this.ipcService.lastLatency);
      this.healthState = 'broken';
      this.controllerMqttConnection?.end();
      this.controllerMqttConnection.off(
        'message',
        this.onControllerMqttConnectionMessage
      );
      this.controllerMqttConnection = undefined;
      this.retryConnectController();
      this.cachedMqttData = undefined;
    } catch (err) {
      console.error(err);
    }
  }

  async getBackendConfig(): Promise<BackendConfig> {
    return (
      this.storageService.get('backend') || {
        BACKEND_NAME: environment.backendName,
        BACKEND_URL: environment.backendUrl,
        BACKEND_SECRET: environment.guardSecret,
      }
    );
  }
}
