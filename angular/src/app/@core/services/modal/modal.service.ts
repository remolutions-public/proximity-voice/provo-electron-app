import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class ModalService {
  isBackdropActive = false;
  preventClosing = false;

  // triggered by modal.service; listened by backdrop.service
  closeModalSub: Subject<string> = new Subject();

  // triggered by transaction-note-input.component; listened by transaction-report-files.component
  closeModalResultSub: Subject<any> = new Subject();

  modalData: any = {};

  onCloseModal(className: string) {
    if (!this.preventClosing) {
      this.closeModalSub.next(className);
    }
  }
}
