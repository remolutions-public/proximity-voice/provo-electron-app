import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounce } from '../../libraries/debounce.lib';
import { Subject, firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { copyObjFilterAttributes } from '../../libraries/obj.lib';
import { ElectronService } from '../electron/electron.service';

export interface IWindow {
  eventSenderId: number;
  id: string;
  type: string;
}

@Injectable()
export class IPCService {
  deamon: any;
  ipc: Electron.IpcRenderer = this.electron.ipcRenderer;
  eventWereRegistered = false;

  lastLatency = -1;
  latencySub: Subject<any> = new Subject();
  // clientid === rendererid
  clientid: string;
  clientidSub: Subject<any> = new Subject();

  serverid: string;

  systemInfo: any = {};

  appVersion = '';
  reportedInitApiHealthcheckResult = false;

  lastPlayerHidingTime = 0;
  playerIsHidden = false;
  ownPlayerId: string;
  ownPlayerName: string;

  window: IWindow;
  windowSub: Subject<IWindow> = new Subject();
  downKeysSub: Subject<any> = new Subject();
  playerDataSub: Subject<any> = new Subject();
  nearbyPlayersSub: Subject<any> = new Subject();

  receiveCotrunDataSub: Subject<any> = new Subject();
  receiveVoiceBaseSettingsSub: Subject<any> = new Subject();
  receiveMaskingconfigSub: Subject<any> = new Subject();
  initRtcHandshakeSub: Subject<any> = new Subject();
  resolveRtcHandshakeSub: Subject<any> = new Subject();
  sendIceCandidateSub: Subject<any> = new Subject();
  sendRtcOfferSub: Subject<any> = new Subject();
  sendRtcAnswerSub: Subject<any> = new Subject();
  sendRtcClosingSub: Subject<any> = new Subject();
  sendAudioCheckSub: Subject<any> = new Subject();
  sendReconnectSub: Subject<any> = new Subject();

  constructor(
    private readonly electron: ElectronService,
    private readonly http: HttpClient
  ) {}

  async init() {
    if (this.ipc == undefined) {
      debounce('IPCService-init', 10, () => {
        this.ipc = this.electron.ipcRenderer;
        this.registerIpcEvents();
      });
    }
    this.registerIpcEvents();
    this.retrieveRendererId();
    this.clientApiHealthcheck();
  }

  async clientApiHealthcheck() {
    if (this.clientid == undefined) {
      debounce('localClientApiHealthcheck', 500, () => {
        this.clientApiHealthcheck();
      });
      return;
    }
    await firstValueFrom(
      this.http.get(environment.localClientApi, {
        withCredentials: true,
      })
    ).catch((err) => {
      console.error('local api healthcheck failed!');
      console.error('ClientApiHealthcheck', err);
    });
  }

  registerIpcEvents() {
    if (this.eventWereRegistered === false && !(this.ipc == undefined)) {
      this.eventWereRegistered = true;

      this.ipc.on('log', (event: any, msg: any) => {
        event;
        switch (msg['type']) {
          case 'error':
            console.error(...msg['msg']);
            break;
          case 'warn':
            console.warn(...msg['msg']);
            break;
          case 'info':
          default:
            console.info(...msg['msg']);
            break;
        }
      });

      this.ipc.on('downKeys', (event: any, msg: any) => {
        this.downKeysSub.next(msg);
      });

      this.ipc.on('PlayerData', (event: any, msg: any) => {
        if (!(msg?.data?.id == undefined)) {
          this.ownPlayerId = msg?.data?.id;
        }
        if (!(msg?.data?.name == undefined)) {
          this.ownPlayerName = msg?.data?.name;
        }
        this.playerDataSub.next(msg);
      });

      this.ipc.on('debugRenderer', (event: any, msg: any) => {
        if (msg.type === 'debug') {
          this.playerDataSub.next({ data: environment[msg.name] });
        }
      });

      this.ipc.on('healthcheck', (event: any, msg: any) => {
        // this is the ingame healthcheck
        if (
          !(environment.production || environment.staging) &&
          this.reportedInitApiHealthcheckResult === false
        ) {
          this.reportedInitApiHealthcheckResult = true;
          console.info('Game Client Healthcheck - local api healthy');
        }
      });

      this.ipc.on('apiProcessHealthcheck', (event: any, msg: any) => {
        // healthcheck from local api
        this.systemInfo = msg.data;
      });

      this.ipc.on('version', (event: any, msg: any) => {
        this.appVersion = msg.version;
      });

      this.ipc.on('rendererid', (event: any, msg: any) => {
        this.clientid = msg.rendererid;
        this.clientidSub.next(this.clientid);
      });

      this.ipc.on('latency', (event: any, msg: any) => {
        this.lastLatency = msg.data?.latency || -1;
        this.latencySub.next(this.lastLatency);
      });

    } else if (this.ipc == undefined) {
      debounce('registerIpcEvents', 250, () => {
        this.registerIpcEvents();
      });
    }
  }

  sendMinimize() {
    if (!(this.ipc?.send == undefined)) {
      this.ipc.send('minimize');
    }
  }

  sendClose() {
    if (!(this.ipc?.send == undefined)) {
      this.ipc.send('close');
    }
  }

  sendApiMessage(data: any) {
    if (!(this.ipc?.send == undefined)) {
      const sanitizedObj = copyObjFilterAttributes(data);
      this.ipc.send('apiData', sanitizedObj);
    }
  }

  getAppVersion(): string {
    if (!(this.ipc?.send == undefined)) {
      if (this.appVersion == undefined || !this.appVersion?.length) {
        this.ipc.send('version');
      }
    }
    return this.appVersion;
  }

  retrieveRendererId() {
    if (!(this.ipc?.send == undefined)) {
      this.ipc.send('retrieveRendererId');
    }
  }

  ////////////////// HELPER /////////////////////
  get isElectronApp(): boolean {
    return this.electron.isElectronApp;
  }

  get isWindows(): boolean {
    return this.electron.isWindows;
  }

  get isLinux(): boolean {
    return this.electron.isLinux;
  }

  get isMacOS(): boolean {
    return this.electron.isMacOS;
  }

  openExternalBrowserLink(link: string) {
    this.electron.shell.openExternal(link);
  }
}
