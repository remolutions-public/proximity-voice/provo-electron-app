import {
  Injectable,
  ComponentFactoryResolver,
  ViewContainerRef,
} from '@angular/core';
import { PlayerSettingComponent } from '../../../pages/dashboard/player-setting/player-setting.component';
import { AboutComponent } from '../../../pages/about/about.component';
import { ModalService } from '../modal/modal.service';
import { Subject } from 'rxjs';
import { debounce } from '../../libraries/debounce.lib';

@Injectable()
export class BackdropService {
  isBackdropActive = false;
  isVerticallyCentered = false;
  currentClassName: string;
  components: {
    type?: string;
    id?: string;
    slot?: string;
    class?: any;
    ref?: any;
    viewChild?: any;
    useBgShading?: boolean;
    useBackdropClose?: boolean;
    isActive?: boolean;
  } = {};
  // is triggered by backdrop.component; listened by backdrop.service
  backdropAfterViewInitSub: Subject<string> = new Subject();
  // is triggered by register.component; listened by backdrop.service
  openModalSub: Subject<string> = new Subject();
  // is triggered by legal.service, backdrop.service, backdrop.component; listened by backdrop.service
  closeModalSub: Subject<string> = new Subject();

  constructor(
    private readonly resolver: ComponentFactoryResolver,
    private readonly modalService: ModalService
  ) {
    setTimeout(() => {
      this.onInit();
    }, 0);

    this.backdropAfterViewInitSub.subscribe((className) => {
      debounce('backdropAfterViewInitSub', 100, () => {
        this.createComponent(className || this.currentClassName);
      });
    });

    this.openModalSub.subscribe((className) => {
      this.isBackdropActive = true;
      this.modalService.isBackdropActive = true;
      this.currentClassName = className;
    });

    this.closeModalSub.subscribe((className) => {
      if (!this.modalService.preventClosing) {
        this.destroyComponent(className);
        this.isBackdropActive = false;
        this.isVerticallyCentered = false;
        this.modalService.isBackdropActive = false;
      }
    });

    this.modalService.closeModalSub.subscribe((className) => {
      this.closeModalSub.next(className);
    });
  }

  onInit() {
    this.components['playerSetting'] = {
      type: 'overlay',
      id: 'backdrop-wrapper',
      slot: 'appBackdropModalSlot',
      class: PlayerSettingComponent,
      useBgShading: true,
      useBackdropClose: true,
      isActive: false,
    };
    this.components['about'] = {
      type: 'overlay',
      id: 'backdrop-wrapper',
      slot: 'appBackdropModalSlot',
      class: AboutComponent,
      useBgShading: true,
      useBackdropClose: true,
      isActive: false,
    };
  }

  updateComponentDirectiveSlot(viewChild: any) {
    if (!(this.components == undefined)) {
      for (const key of Object.keys(this.components)) {
        this.components[key].viewChild = viewChild;
      }
    }
  }

  createComponent(className: string) {
    if (
      !(this.components[className] == undefined) &&
      !this.components[className].isActive
    ) {
      const factory = this.resolver.resolveComponentFactory(
        this.components[className].class
      );
      const viewRef: ViewContainerRef =
        this.components[className].viewChild.viewContainerRef;
      if (viewRef) {
        viewRef.clear();
      }
      this.components[className].ref = viewRef.createComponent(factory);
      if (
        this.components[className].ref &&
        this.components[className].ref.instance.ngOnInit
      ) {
        this.components[className].ref.instance.ngOnInit();
      }
      this.components[className].isActive = true;
      if (this.components[className].useBgShading) {
        document
          .getElementById(this.components[className].id)
          .classList.add('content-shading');
      } else {
        document
          .getElementById(this.components[className].id)
          .classList.add('content-position');
      }
    }
  }

  destroyComponent(className: string) {
    if (
      !(this.components[className] == undefined) &&
      this.components[className].isActive &&
      this.components[className].viewChild
    ) {
      const viewRef = this.components[className].viewChild.viewContainerRef;
      viewRef.clear();
      const activeComp = this.components[className].ref;
      if (activeComp) {
        activeComp.destroy();
        this.components[className].ref = undefined;
      }
      const elem = document.getElementById(this.components[className].id);
      if (this.components[className].useBgShading) {
        elem.classList.remove('content-shading');
      } else {
        elem.classList.remove('content-position');
      }
      this.components[className].isActive = false;
    }
  }
}
