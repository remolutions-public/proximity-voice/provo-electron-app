import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { WebRTCService } from '../webrtc/webrtc.service';
import { VoiceService } from '../voice/voice.service';
import { IPCService } from '../ipc/ipc.service';
import { debounce } from '../../libraries/debounce.lib';
import { WebRTCState } from '../../enums/webrtc-state.enum';
import { ConfigService } from '../config/config.service';
import { MqttService } from '../mqtt/mqtt.service';
import { firstValueFrom } from 'rxjs';
import { environment } from '../../../../environments/environment';
import { Telemetry } from '../../models/telemetry.model';

@Injectable()
export class TelemetryService {
  collectorTimeout = 2500;
  reporterTimeout = 300 * 1000;

  clientid: string;
  playerid: string;
  playername: string;
  serverid: string;
  backendMqttHost: string;
  version: string;
  isMonoAudio: boolean;
  activeHandshakesCount: { [time: number]: number } = {};
  nearbyPlayerCount: { [time: number]: number } = {};
  activeVoiceSessionCount: { [time: number]: number } = {};
  avgVoiceSessionDuration: { [time: number]: number } = {};
  // how many packages did the client get within 2500, translated to avg time between packages
  keyDownPackageRate = 0;
  playerDataPackageRate = 0;
  mqttLatency = 999;
  apiLatency = 999;
  osVersion: string;
  uptimeSeconds = 0;
  maxMemory = 0;
  usedMemoryPercent = 0;
  cpuUsagePercent = 0;

  constructor(
    private readonly webrtcService: WebRTCService,
    private readonly voiceService: VoiceService,
    private readonly ipcService: IPCService,
    private readonly configService: ConfigService,
    private readonly mqttService: MqttService,
    private readonly http: HttpClient
  ) {}

  init() {
    this.telemetryCollector();
    this.telemetryReporter();
  }

  telemetryCollector() {
    debounce('TelemetryCollector', this.collectorTimeout, () => {
      const nowTime = new Date().valueOf();
      if (this.clientid == undefined) {
        this.clientid = this.ipcService.clientid;
      }
      this.playerid = this.ipcService.ownPlayerId;
      this.playername = this.ipcService.ownPlayerName;
      this.backendMqttHost = this.mqttService.cachedMqttData?.externalMqttHost;
      this.serverid = this.voiceService.ownPlayer?.realm;
      this.version = this.ipcService.getAppVersion();
      this.isMonoAudio = this.configService.isMonoAudio;
      this.activeHandshakesCount[nowTime] =
        this.webrtcService.activeConnectingCount;
      const nearbyPlayerIds = Object.keys(this.voiceService.nearbyPlayers);
      this.nearbyPlayerCount[nowTime] = nearbyPlayerIds.length;
      let sessionDurationSum = 0;
      let activeSessions = 0;
      for (const nearbyPlayerId of nearbyPlayerIds) {
        if (
          this.voiceService.nearbyPlayers[nearbyPlayerId]?.rtc?.state ===
            WebRTCState.CONNECTED &&
          this.voiceService.nearbyPlayers[nearbyPlayerId]?.rtc?.sessionStart > 0
        ) {
          activeSessions++;
          sessionDurationSum =
            sessionDurationSum +
            (nowTime -
              this.voiceService.nearbyPlayers[nearbyPlayerId]?.rtc
                ?.sessionStart);
        }
      }
      let averageSessionDuration = 0;
      if (sessionDurationSum > 0 && activeSessions > 0) {
        averageSessionDuration = Math.round(
          sessionDurationSum / activeSessions
        );
      }
      this.activeVoiceSessionCount[nowTime] = activeSessions;
      this.avgVoiceSessionDuration[nowTime] = averageSessionDuration;
      if (this.voiceService.telemetryKeyDownPackageCounter > 0) {
        this.keyDownPackageRate = Math.round(
          this.collectorTimeout /
            this.voiceService.telemetryKeyDownPackageCounter
        );
        this.voiceService.telemetryKeyDownPackageCounter = 0;
      }
      if (this.voiceService.telemetryPlayerDataPackageCounter > 0) {
        this.playerDataPackageRate = Math.round(
          this.collectorTimeout /
            this.voiceService.telemetryPlayerDataPackageCounter
        );
        this.voiceService.telemetryPlayerDataPackageCounter = 0;
      }
      this.mqttLatency = this.mqttService.cachedMqttData?.latency || 999;
      this.apiLatency = this.ipcService.lastLatency || 999;
      this.osVersion = this.ipcService.systemInfo?.osVersion;
      this.uptimeSeconds = this.ipcService.systemInfo?.uptime || 0;
      this.maxMemory = this.ipcService.systemInfo?.memory?.total || 0;
      if (
        !(this.ipcService.systemInfo?.memory?.total == undefined) &&
        this.ipcService.systemInfo?.memory?.total > 0 &&
        !(this.ipcService.systemInfo?.memory?.free == undefined)
      ) {
        this.usedMemoryPercent =
          Math.round(
            ((this.ipcService.systemInfo?.memory?.total -
              this.ipcService.systemInfo?.memory?.free) /
              this.ipcService.systemInfo?.memory?.total) *
              100
          ) / 100;
      }
      this.cpuUsagePercent =
        Math.round(
          (this.ipcService.systemInfo?.cpu?.percentCPUUsage || 0) * 100
        ) / 100;
      this.telemetryCollector();
    });
  }

  telemetryReporter() {
    debounce('telemetryReporter', this.reporterTimeout, async () => {
      let handshakeSum = 0;
      for (const timestamp of Object.keys(this.activeHandshakesCount)) {
        handshakeSum += this.activeHandshakesCount[timestamp];
      }
      let avgActiveHandshakes = 0;
      if (handshakeSum > 0) {
        avgActiveHandshakes =
          Math.round(
            (handshakeSum / Object.keys(this.activeHandshakesCount).length) *
              100
          ) / 100;
      }

      let nearbyPlayerSum = 0;
      for (const timestamp of Object.keys(this.nearbyPlayerCount)) {
        nearbyPlayerSum += this.nearbyPlayerCount[timestamp];
      }
      let avgNearbyPlayers = 0;
      if (nearbyPlayerSum > 0) {
        avgNearbyPlayers =
          Math.round(
            (nearbyPlayerSum / Object.keys(this.nearbyPlayerCount).length) * 100
          ) / 100;
      }

      let activeSessionSum = 0;
      for (const timestamp of Object.keys(this.activeVoiceSessionCount)) {
        activeSessionSum += this.activeVoiceSessionCount[timestamp];
      }
      let avgVoiceSessions = 0;
      if (activeSessionSum > 0) {
        avgVoiceSessions =
          Math.round(
            (activeSessionSum /
              Object.keys(this.activeVoiceSessionCount).length) *
              100
          ) / 100;
      }

      let sessionDurationSum = 0;
      for (const timestamp of Object.keys(this.avgVoiceSessionDuration)) {
        sessionDurationSum += this.avgVoiceSessionDuration[timestamp];
      }
      let avgSessionDuration = 0;
      if (sessionDurationSum > 0) {
        avgSessionDuration =
          Math.round(
            (sessionDurationSum /
              Object.keys(this.avgVoiceSessionDuration).length) *
              100
          ) / 100;
      }

      const backendConfig = await this.configService.getBackendConfig();
      let headers = new HttpHeaders();
      headers = headers.set('Content-Type', 'application/json');

      const payload: Telemetry = {
        clientid: this.clientid,
        playerid: this.playerid,
        playername: this.playername,
        serverid: this.serverid,
        backendMqttHost: this.backendMqttHost,
        backendUrl: backendConfig?.BACKEND_URL,
        backendName: backendConfig?.BACKEND_NAME,
        version: this.version,
        isMonoAudio: this.isMonoAudio,
        avgActiveHandshakes,
        avgNearbyPlayers,
        avgVoiceSessions,
        avgSessionDuration,
        keyDownPackageRate: this.keyDownPackageRate,
        playerDataPackageRate: this.playerDataPackageRate,
        mqttLatency: this.mqttLatency,
        apiLatency: this.apiLatency,
        osVersion: this.osVersion,
        uptimeSeconds: this.uptimeSeconds,
        maxMemory: this.maxMemory,
        usedMemoryPercent: this.usedMemoryPercent,
        cpuUsagePercent: this.cpuUsagePercent,
      };

      const promises = [];
      for (const provoApi of environment.provoApis) {
        promises.push(
          firstValueFrom(
            this.http.post(provoApi + '/api/v1/telemetry', payload, {
              headers,
            })
          ).catch((err) => {
            if (!environment.production) {
              console.error(err);
            }
          })
        );
      }
      await Promise.all(promises);

      // clear for next cycle
      this.activeHandshakesCount = {};
      this.nearbyPlayerCount = {};
      this.activeVoiceSessionCount = {};
      this.avgVoiceSessionDuration = {};
      this.telemetryReporter();
    });
  }
}
