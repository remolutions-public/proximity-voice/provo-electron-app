import { typeIsObject, typeIsArray } from './parser.lib';

export const copyObjFilterAttributes = (
  obj: any,
  filterCommaSeparatedList?: string
): any => {
  const newobj = {};
  for (const key of Object.keys(obj)) {
    if (
      (!(filterCommaSeparatedList == undefined) &&
        filterCommaSeparatedList?.length &&
        !(filterCommaSeparatedList.indexOf(key) > -1)) ||
      filterCommaSeparatedList == undefined
    ) {
      if (typeIsObject(obj[key]) || typeIsArray(obj[key])) {
        try {
          newobj[key] = JSON.parse(JSON.stringify(obj[key]));
        } catch (e) {}
      } else {
        newobj[key] = obj[key];
      }
    }
  }
  return newobj;
};
