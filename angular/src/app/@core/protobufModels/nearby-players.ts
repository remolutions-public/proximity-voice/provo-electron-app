export const nearbyPlayersSchema = `
syntax = "proto3";
package players;

message RemoteVector3D {
  double x = 1;
  double y = 2;
  double z = 3;
}

message RemotePlayer {
  string name = 1;
  string id = 2;
  string realm = 3;
  RemoteVector3D pos = 4;
  RemoteVector3D fVector = 5;
  string type = 6;
  string clientid = 7;
  string apiName = 8;
  string apiIp = 9;
  bool playerIsHidden = 10;
  string version = 11;
  string redisKey = 12;
}

message NearbyPlayers {
  repeated RemotePlayer players = 1 [packed=true];
}
`;
