export class Telemetry {
  clientid?: string;
  playerid?: string;
  playername?: string;
  serverid?: string;
  backendMqttHost?: string;
  backendUrl?: string;
  backendName?: string;
  version?: string;
  isMonoAudio?: boolean;
  avgActiveHandshakes?: number;
  avgNearbyPlayers?: number;
  avgVoiceSessions?: number;
  avgSessionDuration?: number;
  keyDownPackageRate?: number;
  playerDataPackageRate?: number;
  mqttLatency?: number;
  apiLatency?: number;
  osVersion?: string;
  uptimeSeconds?: number;
  maxMemory?: number;
  usedMemoryPercent?: number;
  cpuUsagePercent?: number;
}
