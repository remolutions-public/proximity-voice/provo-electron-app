import { Vector } from './vector.model';
import { RTCSession } from './rtc.model';

export class Player {
  name?: string;
  id?: string;
  pos?: Vector;
  fVector?: Vector;
  uVector?: Vector;
  vehicle?: { 
    velocity?: Vector;
  };
  realm?: string;
  serverid?: string;
  lastUpdate?: number;
  rtc?: RTCSession;
  clientid?: string;
  api?: string;
  inputKeys?: any;
  lastKey?: any;
  fallbackReleaseKeyRef?: any;
  type?: string; // optionally => debug
  audio?: string; // optionally => debug
  volumeLevelSetting?: number;
  randomPlayerName?: string;
  playerIsHidden?: boolean;
  version?: string;
}
