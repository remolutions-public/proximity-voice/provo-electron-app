import { WebRTCState } from '../enums/webrtc-state.enum';
import { WebRTCRole } from '../enums/webrtc-role.enum';

export class RTCSession {
  state?: WebRTCState;
  role?: WebRTCRole;
  sessionData?: any;
  sessionStart?: number;
  repeat?: number;
  pc?: RTCPeerConnection;
  pcConfig?: RTCConfiguration;
  ownAudioStream?: MediaStream;
  ownEncodedAudio?: any;
  ownAudioApi?: {
    audioContext?: AudioContext;
    mediaElementSourceNode?: MediaElementAudioSourceNode;
    mediaStreamDestinationNode?: MediaStreamAudioDestinationNode;
    gainNode?: GainNode;
  };
  remoteAudioStream?: MediaStream;
  remoteAudioApi?: {
    audioContext?: AudioContext;
    analyserNode?: AnalyserNode;
    gainNode?: GainNode;
    mediaStreamSourceNode?: MediaStreamAudioSourceNode;
    pcmData?: Float32Array;
    audioListener?: AudioListener;
    pannerNode?: PannerNode;
  };
  remoteAudioElement?: HTMLAudioElement;
  remoteAudioLevel?: {
    measuredVolume?: number,
    noVolumeCounter?: number,
  };
  dataSendChannel?: RTCDataChannel;
  dataReceiveChannel?: RTCDataChannel;
}
