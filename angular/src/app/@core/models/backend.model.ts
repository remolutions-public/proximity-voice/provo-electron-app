export class BackendConfig {
  BACKEND_NAME: string;
  BACKEND_URL: string;
  BACKEND_SECRET: string;
}
