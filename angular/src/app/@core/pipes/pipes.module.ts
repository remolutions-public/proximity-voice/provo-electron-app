import { NgModule } from '@angular/core';
import { SanitizeHtmlPipe } from './sanitize-html.pipe';
import { TranslateCutPipe } from './translate-cut.pipe';

const PIPES = [
  SanitizeHtmlPipe,
  TranslateCutPipe,
];

@NgModule({
  declarations: [...PIPES],
  exports: [...PIPES],
})
export class PipesModule {}
