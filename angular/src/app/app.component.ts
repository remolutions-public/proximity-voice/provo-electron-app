import { Component, OnInit } from '@angular/core';
import { SidebarService } from './@core/services/sidebar/sidebar.service';
import { ThemeService } from './@core/services/theme/theme.service';
import { LanguageService } from './@core/services/language/language.service';
import { IPCService } from './@core/services/ipc/ipc.service';
import { VoiceService } from './@core/services/voice/voice.service';
import { VoiceControllerService } from './@core/services/voice/voice-controller.service';
import { WebRTCService } from './@core/services/webrtc/webrtc.service';
import { BackdropService } from './@core/services/backdrop/backdrop.service';
import { TelemetryService } from './@core/services/telemetry/telemetry.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  wasInit = false;

  constructor(
    private readonly sidebarService: SidebarService,
    private readonly themeService: ThemeService,
    private readonly languageService: LanguageService,
    private readonly ipcService: IPCService,
    private readonly voiceService: VoiceService,
    private readonly voiceControllerService: VoiceControllerService,
    private readonly webRTCService: WebRTCService,
    private readonly telemetryService: TelemetryService,
    public readonly backdropService: BackdropService
  ) {}

  ngOnInit() {
    this.init();
  }

  ionViewDidEnter() {
    this.init();
  }

  init() {
    if (!this.wasInit) {
      this.wasInit = true;
      this.sidebarService.disableMenuSwiping();
      this.languageService.initLanguage();
      this.themeService.initTheme();
      this.ipcService.init();
      this.voiceService.init();
      this.webRTCService.init();
      this.telemetryService.init();
      this.voiceControllerService.registerRoutes();
    }
  }

  set menuHidden(val: boolean) {
    this.sidebarService.menuHidden = val;
  }

  get menuHidden(): boolean {
    return this.sidebarService.menuHidden;
  }

  isNativeApp(): boolean {
    return window.origin.includes('capacitor://');
  }

  onMenuStateChange(open: boolean) {
    this.sidebarService.nextMenuHiddenSub.next(open);
  }
}
