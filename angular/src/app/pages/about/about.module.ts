import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { LogoModule } from '../../@theme/components/logo/logo.module';

import { AboutComponent } from './about.component';

import { MatTooltipModule } from '@angular/material/tooltip';

const ANGULAR_MATERIAL = [MatTooltipModule];

const COMPONENTS = [AboutComponent];

const MODULES = [CommonModule, IonicModule, TranslateModule, LogoModule];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, ...ANGULAR_MATERIAL],
  exports: [...COMPONENTS],
})
export class AboutModule {}
