import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { IPCService } from '../../@core/services/ipc/ipc.service';
import { BackdropService } from '../../@core/services/backdrop/backdrop.service';
import { ModalService } from '../../@core/services/modal/modal.service';
import { UpdateService } from '../../@core/services/update/update.service';
import {
  ThemeService,
} from '../../@core/services/theme/theme.service';
import { environment } from '../../../environments/environment';
import { debounce } from '../../@core/libraries/debounce.lib';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit {
  authors = ['Impulse'];
  projectName = 'proximity-voice';
  projectLink = 'https://gitlab.com/remolutions-public/proximity-voice';

  get appVersion(): string {
    return this.ipcService.getAppVersion();
  }

  get hasUpdateAvailable(): boolean {
    return this.updateService.hasUpdateAvailable;
  }

  get latestAvailableVersion(): string {
    return this.updateService.latestAvailableVersion;
  }

  get isTooltipsDisabled(): boolean {
    return this.themeService.isTooltipsDisabled;
  }

  constructor(
    private readonly ipcService: IPCService,
    private readonly translate: TranslateService,
    private readonly backdropService: BackdropService,
    private readonly modalService: ModalService,
    private readonly updateService: UpdateService,
    private readonly themeService: ThemeService,
    public readonly alertController: AlertController
  ) {}

  ngOnInit() {}

  onSourceClick() {
    debounce('onSourceClick', 100, () => {
      this.ipcService.openExternalBrowserLink(this.projectLink);
    });
  }

  onDownloadUpdate() {
    this.updateService.openUpdateLink();
  }

  onClose() {
    this.modalService.modalData = {};
    this.onCloseModal(false);
  }

  onCloseModal(result: boolean = false) {
    this.modalService.closeModalResultSub.next(result);
    this.modalService.onCloseModal('about');
  }
}
