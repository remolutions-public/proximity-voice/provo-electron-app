import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { OverlayModule } from '@angular/cdk/overlay';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { PagesRoutingModule } from './pages-routing.module';
import { PlayerSettingModule } from './dashboard/player-setting/player-setting.module';
import { AboutModule } from './about/about.module';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TranslateModule } from '@ngx-translate/core';

import { MatTooltipModule } from '@angular/material/tooltip';

const ANGULAR_MATERIAL = [MatTooltipModule];

const COMPONENTS = [PagesComponent, DashboardComponent];

const MODULES = [
  CommonModule,
  BrowserModule,
  PagesRoutingModule,
  IonicModule,
  TranslateModule,
  PlayerSettingModule,
  AboutModule,
  OverlayModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, ...ANGULAR_MATERIAL],
  exports: [...COMPONENTS],
})
export class PagesModule {}
