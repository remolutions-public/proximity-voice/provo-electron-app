import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { ConfigService } from '../../@core/services/config/config.service';
import { VoiceService } from '../../@core/services/voice/voice.service';
import { IPCService } from '../../@core/services/ipc/ipc.service';
import { Subscription } from 'rxjs';
import { Player } from '../../@core/models/player.model';
import { WebRTCState } from '../../@core/enums/webrtc-state.enum';
import { distance3DTo } from '../../@core/libraries/distance.lib';
import { MediaService } from '../../@core/services/media/media.service';
import { WebRTCService } from '../../@core/services/webrtc/webrtc.service';
import { BackdropService } from '../../@core/services/backdrop/backdrop.service';
import { ModalService } from '../../@core/services/modal/modal.service';
import { ThemeService } from '../../@core/services/theme/theme.service';
import { UpdateService } from '../../@core/services/update/update.service';
import { debounce, hasDebounceRef } from '../../@core/libraries/debounce.lib';
import { environment } from '../../../environments/environment';
import { parseBoolean } from '../../@core/libraries/parser.lib';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  private subs: Subscription[] = [];

  get canTriggerReconnectToAll(): boolean {
    if (
      new Date().valueOf() - this.voiceService.lastReconnectAllTriggerTime >
      this.voiceService.reconnectAllTriggerCooldown
    ) {
      return true;
    }
    return false;
  }

  get reconnectToAllCooldown(): number {
    const result = Math.ceil(
      Math.max(
        0,
        this.voiceService.reconnectAllTriggerCooldown -
          (new Date().valueOf() - this.voiceService.lastReconnectAllTriggerTime)
      ) * 0.001
    );
    return isNaN(result) ? 0 : result;
  }

  get isOwnVoiceActive(): boolean {
    return this.voiceService.isOwnVoiceActive;
  }

  get lastInput(): string {
    return this.voiceService.lastVoiceToggleKey;
  }

  get playerID(): string {
    return this.voiceService.ownPlayer.id;
  }

  get playerName(): string {
    return this.voiceService.ownPlayer.name;
  }

  get remotePlayers(): Player[] {
    const players: Player[] = [];
    for (const playerid of Object.keys(this.voiceService.nearbyPlayers)) {
      players.push(this.voiceService.nearbyPlayers[playerid]);
    }
    return players;
  }

  get hasUpdateAvailable(): boolean {
    return this.updateService.hasUpdateAvailable;
  }

  get appVersion(): string {
    return this.ipcService.getAppVersion();
  }

  get latestAvailableVersion(): string {
    return this.updateService.latestAvailableVersion;
  }

  get isProduction(): boolean {
    return environment.production;
  }

  get allowPlayerHiding(): boolean {
    return parseBoolean(this.configService.maskingConfig?.allowPlayerHiding);
  }

  get maxPlayerHidingSeconds(): number {
    return parseFloat(this.configService.maskingConfig?.maxPlayerHidingSeconds);
  }

  get playerHidingCooldownSeconds(): number {
    return parseFloat(
      this.configService.maskingConfig?.playerHidingCooldownSeconds
    );
  }

  get hidePlayerCooldown(): number {
    const result = Math.ceil(
      this.playerHidingCooldownSeconds -
        (new Date().valueOf() - this.ipcService.lastPlayerHidingTime) / 1000
    );
    return isNaN(result) ? 0 : result;
  }

  get canHidePlayer(): boolean {
    return this.hidePlayerCooldown <= 0;
  }

  get isPlayerHidden(): boolean {
    return this.ipcService.playerIsHidden;
  }

  get hideAllPlayerNames(): boolean {
    return parseBoolean(this.configService.maskingConfig?.hideAllRemotePlayers);
  }

  get showMaskedPlayerIdentification(): boolean {
    return parseBoolean(
      this.configService.maskingConfig?.maskPlayerIdentification
    );
  }

  get showPlayerNamesInsteadOfIdentification(): boolean {
    return parseBoolean(this.configService.maskingConfig?.showPlayerNames);
  }

  get isTooltipsDisabled(): boolean {
    return this.themeService.isTooltipsDisabled;
  }

  constructor(
    private readonly ipcService: IPCService,
    private readonly mediaService: MediaService,
    private readonly voiceService: VoiceService,
    private readonly webRTCService: WebRTCService,
    private readonly changeDetectorRef: ChangeDetectorRef,
    private readonly backdropService: BackdropService,
    private readonly modalService: ModalService,
    private readonly updateService: UpdateService,
    private readonly themeService: ThemeService,
    private readonly configService: ConfigService
  ) {}

  async ngOnInit() {
    // init own input device, mainly to load cached input selection
    debounce('delayed-prepareOwnAudioStream', 500, () => {
      this.mediaService.prepareOwnAudioStream(this.voiceService.ownPlayer);
    });

    this.subs.push(
      this.voiceService.updateOwnStateSub.subscribe(() => {
        this.scheduleNextChangeDetection();
      })
    );
    this.subs.push(
      this.voiceService.updatedNearbyPlayersSub.subscribe(() => {
        this.scheduleNextChangeDetection();
      })
    );

    debounce('dashboard-getApiHealthcheck', 300, () => {
      this.voiceService.getApiHealthcheck().catch((err) => {
        console.error('controller api healthcheck ping failed!');
        console.error(err);
      });
    });

    this.updateService.clientUpdateCheckCycle();
  }

  scheduleNextChangeDetection() {
    if (!hasDebounceRef('scheduleNextChangeDetection')) {
      debounce('scheduleNextChangeDetection', 100, () => {
        this.changeDetectorRef.detectChanges();
      });

      debounce('slow-default-change-detection', 250, () => {
        this.scheduleNextChangeDetection();
      });
    }
  }

  getRandomPlayerName(player: Player): string {
    if (
      this.showMaskedPlayerIdentification &&
      !(player == undefined) &&
      player?.randomPlayerName == undefined
    ) {
      player.randomPlayerName = `Player #${this.voiceService
        .lastPlayerRandomID++}`;
    }
    return player?.randomPlayerName;
  }

  isRemoteVoiceActive(player: Player): boolean {
    let voiceActive = false;
    if (!(player.inputKeys == undefined)) {
      for (const key of Object.keys(player.inputKeys)) {
        if (player.inputKeys[key] === true) {
          voiceActive = true;
        }
      }
    }
    return voiceActive;
  }

  isRemoteVoiceMalfunctioning(player: Player): boolean {
    if (!(player == undefined) && player.rtc == undefined) {
      return false;
    }
    if (player.rtc.state === WebRTCState.CONNECTED) {
      if (player.rtc.remoteAudioStream == undefined) {
        return true;
      }

      const remoteAudioTrack = player.rtc.remoteAudioStream.getAudioTracks()[0];
      if (
        remoteAudioTrack == undefined ||
        remoteAudioTrack.readyState !== 'live'
      ) {
        return true;
      }
    }
    return false;
  }

  isRemoteVoiceDisconnected(player: Player): boolean {
    if (!(player == undefined) && player.rtc == undefined) {
      return true;
    }
    if (
      !(player == undefined) &&
      (player.rtc.state === WebRTCState.DISCONNECTED ||
        player.rtc.state === WebRTCState.FAILED ||
        player.rtc.state === WebRTCState.TIMEOUT)
    ) {
      return true;
    }
    return false;
  }

  isRemoteVoiceConnecting(player: Player): boolean {
    if (
      !(player == undefined) &&
      !(player.rtc == undefined) &&
      (player.rtc.state === WebRTCState.CALLING ||
        player.rtc.state === WebRTCState.CONNECTING)
    ) {
      return true;
    }
    return false;
  }

  getPlayerDistance(player: Player): number {
    return (
      Math.round(
        distance3DTo(player.pos, this.voiceService.ownPlayer.pos) * 100
      ) / 100
    );
  }

  getPlayerVoiceMode(player: Player): string {
    for (const key of Object.keys(player.inputKeys || {})) {
      if (player.inputKeys[key]) {
        return ' | ' + key;
      }
    }
  }

  // TESTING
  // getPlayerAudioLevel(player: Player): number {
  //   if (player?.rtc?.remoteAudioLevel?.measuredVolume == undefined) {
  //     return 0;
  //   }
  //   if (player?.rtc?.remoteAudioLevel?.measuredVolume === Infinity) {
  //     return 0;
  //   }
  //   return player?.rtc?.remoteAudioLevel?.measuredVolume * 10;
  // }

  onReconnect(player: Player) {
    if (this.voiceService.lastRemotePlayerReconnectTriggerTime == undefined) {
      this.voiceService.lastRemotePlayerReconnectTriggerTime = {};
    }
    this.voiceService.lastRemotePlayerReconnectTriggerTime[player.id] =
      new Date().valueOf();
    this.webRTCService.clearWebRTCSession(player);
  }

  onReconnectAll() {
    this.voiceService.lastReconnectAllTriggerTime = new Date().valueOf();
    this.webRTCService.reconnectAllPlayers();
  }

  onPlayerSettings(player: Player) {
    this.modalService.modalData = player;
    this.backdropService.isVerticallyCentered = true;
    this.backdropService.openModalSub.next('playerSetting');
  }

  onHideYourself() {
    if (!this.ipcService.playerIsHidden) {
      this.ipcService.playerIsHidden = true;
      this.ipcService.lastPlayerHidingTime = new Date().valueOf();
      debounce('onHideYourself', this.maxPlayerHidingSeconds * 1000, () => {
        this.ipcService.playerIsHidden = false;
        this.scheduleNextChangeDetection();
      });
    } else {
      this.ipcService.playerIsHidden = false;
    }
    this.ipcService.ownPlayerId = this.voiceService.ownPlayer.id;
    this.scheduleNextChangeDetection();
  }

  volume(player: Player): number {
    if (player?.volumeLevelSetting == undefined) {
      return 1;
    }
    return parseFloat(player?.volumeLevelSetting as any);
  }

  onDownloadUpdate() {
    this.updateService.openUpdateLink();
  }

  canTriggerReconnectToPlayer(player: Player): boolean {
    if (
      this.voiceService.lastRemotePlayerReconnectTriggerTime[player.id] ==
      undefined
    ) {
      return true;
    }
    if (
      new Date().valueOf() -
        this.voiceService.lastRemotePlayerReconnectTriggerTime[player.id] >
      this.voiceService.reconnectAllTriggerCooldown
    ) {
      return true;
    }
    return false;
  }

  getReconnectToPlayerCooldown(player: Player): number {
    if (
      this.voiceService.lastRemotePlayerReconnectTriggerTime[player.id] ==
      undefined
    ) {
      return 0;
    }
    const result = Math.ceil(
      Math.max(
        0,
        this.voiceService.reconnectAllTriggerCooldown -
          (new Date().valueOf() -
            this.voiceService.lastRemotePlayerReconnectTriggerTime[player.id])
      ) * 0.001
    );
    return isNaN(result) ? 0 : result;
  }

  ngOnDestroy() {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
