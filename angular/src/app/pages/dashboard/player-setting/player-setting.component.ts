import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Player } from '../../../@core/models/player.model';
import { ConfigService } from '../../../@core/services/config/config.service';
import { NotificationService } from '../../../@core/services/toast-notification/toast-notification.service';
import { BackdropService } from '../../../@core/services/backdrop/backdrop.service';
import { ModalService } from '../../../@core/services/modal/modal.service';
import { StorageService } from '../../../@core/services/storage/storage.service';
import { VoiceService } from '../../../@core/services/voice/voice.service';
import { parseBoolean } from '../../../@core/libraries/parser.lib';

@Component({
  selector: 'app-player-setting',
  templateUrl: './player-setting.component.html',
  styleUrls: ['./player-setting.component.scss'],
})
export class PlayerSettingComponent implements OnInit {
  playerObj: any;
  playerStorageObj: any;

  get showMaskedPlayerIdentification(): boolean {
    return parseBoolean(
      this.configService.maskingConfig?.maskPlayerIdentification
    );
  }

  get showPlayerNamesInsteadOfIdentification(): boolean {
    return parseBoolean(this.configService.maskingConfig?.showPlayerNames);
  }

  constructor(
    private readonly configService: ConfigService,
    private readonly note: NotificationService,
    private readonly translate: TranslateService,
    private readonly storageService: StorageService,
    private readonly backdropService: BackdropService,
    private readonly modalService: ModalService,
    private readonly voiceService: VoiceService,
    public readonly alertController: AlertController
  ) {}

  ngOnInit() {
    this.playerObj = this.modalService.modalData;

    this.playerStorageObj = this.storageService.get(this.playerObj.id);
    if (!(this.playerStorageObj == undefined)) {
      if (this.playerStorageObj.volumeLevelSetting == undefined) {
        this.playerObj.volumeLevelSetting = 1;
      } else {
        this.playerObj.volumeLevelSetting =
          this.playerStorageObj.volumeLevelSetting;
      }
    }
  }

  getRandomPlayerName(player: Player): string {
    if (
      this.showMaskedPlayerIdentification &&
      !(player == undefined) &&
      player?.randomPlayerName == undefined
    ) {
      player.randomPlayerName = `Player #${this.voiceService
        .lastPlayerRandomID++}`;
    }
    return player?.randomPlayerName;
  }

  onVolumeLevelChange(event: any) {
    this.playerObj.volumeLevelSetting = event.target.value * 0.01;
    if (this.playerStorageObj == undefined) {
      this.playerStorageObj = {};
    }
    this.playerStorageObj.volumeLevelSetting =
      this.playerObj.volumeLevelSetting;
    this.storageService.set(this.playerObj.id, this.playerStorageObj);
  }

  volume(): number {
    if (this.playerObj?.volumeLevelSetting == undefined) {
      return 1;
    }
    return parseFloat(this.playerObj.volumeLevelSetting);
  }

  onClose() {
    this.modalService.modalData = {};
    this.onCloseModal(false);
  }

  onCloseModal(result: boolean = false) {
    this.modalService.closeModalResultSub.next(result);
    this.modalService.onCloseModal('playerSetting');
  }
}
