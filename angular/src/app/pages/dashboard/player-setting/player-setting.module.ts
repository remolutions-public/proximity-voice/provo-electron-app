import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PlayerSettingComponent } from './player-setting.component';

const COMPONENTS = [PlayerSettingComponent];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  FormsModule,
  ReactiveFormsModule,
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES],
  exports: [...COMPONENTS],
})
export class PlayerSettingModule {}
