import { NgModule, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';

import { CoreModule } from './@core/@core.module';
import { ThemeModule } from './@theme/@theme.module';
import { HeaderModule } from './@theme/components/header/header.module';
import { SidebarModule } from './@theme/components/sidebar/sidebar.module';
import { PagesModule } from './pages/pages.module';
import { FooterModule } from './@theme/components/footer/footer.module';
import { BackdropModule } from './@theme/components/backdrop/backdrop.module';

import { AppComponent } from './app.component';
import * as Sentry from '@sentry/angular-ivy';

import { MatTooltipModule } from '@angular/material/tooltip';

const ANGULAR_MATERIAL = [MatTooltipModule];

// required for AOT compilation
export const createTranslateLoader = (http: HttpClient) => {
  return new TranslateHttpLoader(http, 'assets/language/', '.json');
};

const COMPONENTS = [AppComponent];

const MODULES = [
  CommonModule,
  BrowserModule,
  BrowserAnimationsModule,
  IonicModule.forRoot(),
  CoreModule,
  ThemeModule,
  HeaderModule,
  FooterModule,
  SidebarModule,
  PagesModule,
  HttpClientModule,
  TranslateModule.forRoot({
    defaultLanguage: 'en',
    loader: {
      provide: TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [HttpClient],
    },
  }),
  BackdropModule,
];

const PROVIDERS = [
  { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  {
    provide: ErrorHandler,
    useValue: Sentry.createErrorHandler({
      showDialog: false,
    }),
  },
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, ...ANGULAR_MATERIAL],
  providers: [...PROVIDERS],
  bootstrap: [AppComponent],
})
export class AppModule {}
