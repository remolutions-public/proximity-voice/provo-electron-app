import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.scss']
})
export class LogoComponent {
  @Input() width = '50';
  @Input() height = '50';

  selectedSize: any;
  sources = [
    { size: 32, url: 'assets/logo/Provo_32p.png' },
    { size: 64, url: 'assets/logo/Provo_64p.png' },
    { size: 128, url: 'assets/logo/Provo_128p.png' },
    { size: 192, url: 'assets/logo/Provo_192p.png' }
  ];

  get imgUrl(): string {
    if (!this.selectedSize) {
      for (const source of this.sources) {
        if (
          source.size > parseInt(this.width, 10) ||
          source.size > parseInt(this.height, 10)
        ) {
          this.selectedSize = source;
          break;
        }
      }
    }
    return this.selectedSize.url;
  }
}
