import { Component, OnInit, OnDestroy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import { SidebarService } from '../../../@core/services/sidebar/sidebar.service';
import { ThemeService } from '../../../@core/services/theme/theme.service';
import { MediaService } from '../../../@core/services/media/media.service';
import { IPCService } from '../../../@core/services/ipc/ipc.service';
import { NotificationService } from '../../../@core/services/toast-notification/toast-notification.service';
import { Subscription } from 'rxjs';
import {
  debounce,
  stopDebounceRef,
} from '../../../@core/libraries/debounce.lib';
import { StorageService } from '../../../@core/services/storage/storage.service';
import { BackdropService } from '../../../@core/services/backdrop/backdrop.service';
import { ModalService } from '../../../@core/services/modal/modal.service';
import { ConfigService } from '../../../@core/services/config/config.service';
import { BackendService } from '../../../@core/services/backend/backend.service';
import { MqttService } from '../../../@core/services/mqtt/mqtt.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
})
export class SidebarComponent implements OnInit, OnDestroy {
  private subs: Subscription[] = [];
  isLoading = false;

  availableDevices: any[] = [];

  configObj: any;

  showSecretInput = false;
  secretInputValue: string;
  backendLimitRecords = 6;
  backendDropdownOpen = false;
  selectedBackend: any;
  backendObjs: any[] = [];
  filteredBackendObjs: any[] = [];
  filter: string = '';

  get headerThemeColor(): string {
    return this.themeService.activeTheme.header;
  }

  get isSelectedDeviceValid(): boolean {
    const device =
      this.mediaService?.lastKnownAvailableDevices?.[
        this.mediaService?.lastKnownSelectedDevice?.deviceId
      ];
    if (this.mediaService?.lastKnownSelectedDevice == undefined) {
      return true;
    }
    if (device == undefined) {
      return false;
    }
    return true;
  }

  get useMonoAudio(): boolean {
    return this.configService.getUseMonoAudio();
  }

  set useMonoAudio(val: boolean) {
    this.configService.setUseMonoAudio(val);
    this.mediaService.inputDevicesChangedSub.next({
      availableAudioObjs: this.mediaService.lastKnownAvailableDevices,
      communicationAudioObj: this.mediaService.lastKnownCommunicationDevice,
      defaultAudioObj: this.mediaService.lastKnownDefaultDevice,
    });
  }

  get selectedDevice(): any {
    const device = this.configService.getSelectedDevice();
    if (!(device == undefined) && device?.constraints == undefined) {
      device.constraints = {
        audio: {
          deviceId: device?.deviceId,
        },
        video: false,
      };
    }
    return device;
  }

  get isMqttConnectionHealthy(): boolean {
    return !(this.mqttService.cachedMqttData == undefined);
  }

  constructor(
    private readonly routing: RoutingService,
    private readonly translate: TranslateService,
    private readonly sidebarService: SidebarService,
    private readonly themeService: ThemeService,
    private readonly mediaService: MediaService,
    private readonly ipcService: IPCService,
    private readonly storageService: StorageService,
    private readonly backdropService: BackdropService,
    private readonly modalService: ModalService,
    private readonly note: NotificationService,
    private readonly configService: ConfigService,
    private readonly backendService: BackendService,
    private readonly mqttService: MqttService
  ) {
    this.subs.push(
      this.sidebarService.nextMenuHiddenSub.subscribe((val) => {
        stopDebounceRef('fallback-isloading-reset');
        this.isLoading = true;
        const devices = [];
        if (!(this.mediaService.lastKnownAvailableDevices == undefined)) {
          for (const deviceid of Object.keys(
            this.mediaService.lastKnownAvailableDevices
          )) {
            devices.push(this.mediaService.lastKnownAvailableDevices[deviceid]);
          }
        }
        this.availableDevices = devices;
        if (!val) {
          this.fetchConfig();
          this.loadStoredConfig();
        }
        debounce('fallback-isloading-reset', 500, () => {
          this.isLoading = false;
        });
      })
    );
  }

  async ngOnInit() {
    this.fetchConfig();
    this.loadStoredConfig();
  }

  async loadStoredConfig() {
    this.showSecretInput = false;
    const backendConfig = await this.configService.getBackendConfig();
    this.selectedBackend = {
      backendUrl: backendConfig.BACKEND_URL,
      backendName: backendConfig.BACKEND_NAME,
      isPublic: !backendConfig.BACKEND_SECRET?.length,
    };
  }

  async fetchConfig() {
    const backendResult = await this.backendService.getBackends(
      this.backendLimitRecords,
      undefined,
      this.filter
    );
    if (!(backendResult == undefined)) {
      this.backendObjs = backendResult.backends;
    }
    this.filterBackends();
  }

  isRouteActive(route: string) {
    return this.routing.isRouteActive(route);
  }

  onNavigateTo(path: string[]) {
    this.routing.navigate(path);
  }

  onDeviceChange(event: any) {
    if (this.isLoading) {
      return;
    }
    const device = event?.target?.value;
    if (!(device == undefined)) {
      this.configService.setSelectedDevice(device);
      this.mediaService.inputDevicesChangedSub.next({
        availableAudioObjs: this.mediaService.lastKnownAvailableDevices,
        communicationAudioObj: this.mediaService.lastKnownCommunicationDevice,
        defaultAudioObj: this.mediaService.lastKnownDefaultDevice,
      });
    }
  }

  onUseMonoAudioChange(event: any) {
    this.useMonoAudio = event?.detail?.checked || false;
  }

  filterBackends() {
    const filteredObjs = [];
    for (const backendObj of this.backendObjs) {
      if (!this.filter?.length) {
        filteredObjs.push(backendObj);
      } else if (
        (!(backendObj.backendName == undefined) &&
          backendObj.backendName.toLowerCase().indexOf(this.filter) > -1) ||
        (!(backendObj.backendUrl == undefined) &&
          backendObj.backendUrl.toLowerCase().indexOf(this.filter) > -1)
      ) {
        filteredObjs.push(backendObj);
      }
    }
    this.filteredBackendObjs = filteredObjs;
  }

  onBackendInput(event: any) {
    const inputVal = event.detail?.value;
    this.filter = inputVal;
    this.filterBackends();
    debounce('onBackendInput', 250, () => {
      for (const entry of this.backendObjs) {
        if (
          !(entry.backendName == undefined) &&
          !(inputVal == undefined) &&
          inputVal?.length &&
          entry.backendName.trim().toLowerCase() ===
            inputVal.trim().toLowerCase()
        ) {
          this.onBackendSelect(entry);
          return;
        }
      }
      if (!(this.selectedBackend == undefined)) {
        this.showSecretInput = false;
        this.selectedBackend = undefined;
        this.filter = '';
      }
      this.fetchConfig();
    });
  }

  onBackendSelect(backendObj: any) {
    this.selectedBackend = backendObj;
    if (!this.selectedBackend.isPublic) {
      this.secretInputValue = '';
      this.showSecretInput = true;
      debounce('onBackendSelect-setFocus', 200, () => {
        const secretInputElem = document.getElementById('backend-secret-input');
        secretInputElem.focus();
      });
    } else {
      this.storageService.set('backend', {
        BACKEND_URL: backendObj.backendUrl,
        BACKEND_SECRET: '',
        BACKEND_NAME: backendObj.backendName,
      });
      this.mqttService.cachedMqttData = undefined;
      this.mqttService.failedMqttConnection();
      this.mqttService.retryConnectController(true);
    }
  }

  onSecretInput(event: any) {
    this.secretInputValue = event.detail?.value;
  }

  onSecretChange(event: any) {
    if (event.which === 13) {
      this.onBackendSave();
    }
  }

  onBackendSave() {
    debounce('onBackendSave', 250, () => {
      this.storageService.set('backend', {
        BACKEND_URL: this.selectedBackend.backendUrl,
        BACKEND_SECRET: this.secretInputValue,
        BACKEND_NAME: this.selectedBackend.backendName,
      });
      this.showSecretInput = false;
      this.mqttService.cachedMqttData = undefined;
      this.mqttService.failedMqttConnection();
      this.note.showNotification(
        'success',
        undefined,
        'NOTIFICATIONS.SAVE_SUCCESS'
      );
      this.mqttService.retryConnectController(true);
    });
  }

  onBackendDropdown(state: boolean) {
    debounce('onBackendDropdown-state-change', 250, () => {
      this.backendDropdownOpen = state;
      if (state) {
        debounce('onBackendDropdown', 0, () => {
          const inputElem = document.getElementById('backend-input');
          const offset = inputElem.getBoundingClientRect();
          const autocompleteMenu = document.getElementById(
            'backend-autocomplete-menu'
          );
          const verticalOffset = inputElem.offsetTop + inputElem.offsetHeight;
          autocompleteMenu.setAttribute(
            'style',
            `top:${verticalOffset}px;left:${offset.left}px;opacity:1;width:${offset.width}px;`
          );
        });
      }
    });
  }

  onAboutClick() {
    this.backdropService.isVerticallyCentered = true;
    this.backdropService.openModalSub.next('about');
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
