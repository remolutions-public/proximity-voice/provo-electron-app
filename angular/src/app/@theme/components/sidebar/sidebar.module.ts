import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { SidebarComponent } from './sidebar.component';

const COMPONENTS = [
  SidebarComponent,
];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES],
    exports: [...COMPONENTS],
    providers: []
})
export class SidebarModule { }
