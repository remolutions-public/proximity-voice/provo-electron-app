import { Component, AfterViewInit, ViewChild } from '@angular/core';
import { BackdropService } from '../../../@core/services/backdrop/backdrop.service';
import { BackdropDirective } from '../../../@core/directives/backdrop.directive';
import { debounce } from '../../../@core/libraries/debounce.lib';

@Component({
  selector: 'app-backdrop',
  templateUrl: './backdrop.component.html',
  styleUrls: ['./backdrop.component.scss'],
})
export class BackdropComponent implements AfterViewInit {
  @ViewChild(BackdropDirective, { static: true })
  backdropModalSlot: BackdropDirective;

  get isVerticallyCentered(): boolean {
    return this.backdropService.isVerticallyCentered;
  }

  constructor(private readonly backdropService: BackdropService) {}

  ngAfterViewInit() {
    debounce('BackdropComponent', 10, () => {
      this.backdropService.updateComponentDirectiveSlot(this.backdropModalSlot);
      this.backdropService.backdropAfterViewInitSub.next(
        this.backdropService.currentClassName
      );
    });
  }

  onBackdrop(event: any) {
    if (
      (event.target.classList.contains('backdrop-wrapper') ||
        event.target.classList.contains('backdrop-center')) &&
      this.backdropService.components[this.backdropService.currentClassName]
        .useBackdropClose
    ) {
      this.backdropService.closeModalSub.next(
        this.backdropService.currentClassName
      );
    }
  }

  isBackdropShading(): boolean {
    if (
      this.backdropService.components[this.backdropService.currentClassName]
        .useBgShading
    ) {
      return true;
    }
    return false;
  }
}
