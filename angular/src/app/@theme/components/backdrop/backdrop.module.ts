import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { BackdropComponent } from './backdrop.component';
import { BackdropDirective } from '../../../@core/directives/backdrop.directive';

const COMPONENTS = [
  BackdropComponent,
];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
];

const DIRECTIVES = [
  BackdropDirective
];

@NgModule({
    declarations: [...COMPONENTS, ...DIRECTIVES],
    imports: [...MODULES],
    exports: [...COMPONENTS]
})
export class BackdropModule { }
