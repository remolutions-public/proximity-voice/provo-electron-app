import { Component } from '@angular/core';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import { debounce } from '../../../@core/libraries/debounce.lib';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent {
  constructor(
    private readonly routing: RoutingService,
  ) {}

  onLegal() {
    // this.routing.navigate(['/legal', 'notice']);
    // debounce('onLegal', 25, () => {
    //   this.landingService.scrollTo('marker-legal-notice');
    // });
  }

  onDataprotection() {
    // this.routing.navigate(['/legal', 'data']);
    // debounce('onDataprotection', 25, () => {
    //   this.landingService.scrollTo('marker-data-protection');
    // });
  }
}
