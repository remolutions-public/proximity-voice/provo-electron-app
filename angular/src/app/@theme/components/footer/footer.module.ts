import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { FooterComponent } from './footer.component';
import { LogoModule } from '../logo/logo.module';

const COMPONENTS = [FooterComponent];

const MODULES = [
  CommonModule,
  IonicModule,
  TranslateModule,
  LogoModule
];

@NgModule({
    declarations: [...COMPONENTS],
    imports: [...MODULES],
    exports: [...COMPONENTS],
    providers: []
})
export class FooterModule {}
