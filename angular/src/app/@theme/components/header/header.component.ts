import {
  Component,
  OnDestroy,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
} from '@angular/core';
import { SidebarService } from '../../../@core/services/sidebar/sidebar.service';
import { LanguageService } from '../../../@core/services/language/language.service';
import { RoutingService } from '../../../@core/services/routing/routing.service';
import {
  Theme,
  ThemeService,
} from '../../../@core/services/theme/theme.service';
import { IPCService } from '../../../@core/services/ipc/ipc.service';
import { ConfigService } from '../../../@core/services/config/config.service';
import { Subscription } from 'rxjs';
import { environment } from '../../../../environments/environment';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnDestroy {
  public userMenuActive = false;
  latency = -1;

  private subs: Subscription[] = [];

  get isProduction(): boolean {
    return environment.production;
  }

  get isTooltipsDisabled(): boolean {
    return this.themeService.isTooltipsDisabled;
  }

  get isGlobalMuted(): boolean {
    return this.configService.isGlobalMuted;
  }

  constructor(
    private readonly routing: RoutingService,
    private readonly sidebarService: SidebarService,
    private readonly languageService: LanguageService,
    private readonly themeService: ThemeService,
    private readonly ipcService: IPCService,
    private readonly configService: ConfigService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {
    this.subs.push(
      this.ipcService.latencySub.subscribe((latency) => {
        this.latency = latency || -1;
        this.changeDetectorRef.detectChanges();
      })
    );
    this.subs.push(
      this.ipcService.clientidSub.subscribe(() => {
        this.changeDetectorRef.detectChanges();
      })
    );
  }

  get menuHidden(): boolean {
    return this.sidebarService.menuHidden;
  }

  get currentTheme(): Theme {
    return this.themeService.currentTheme;
  }

  get currentLanguage(): string {
    return this.languageService.currentLanguage
      ? this.languageService.currentLanguage.toUpperCase()
      : 'DE';
  }

  get availableLanguages(): string[] {
    return this.languageService.supportedLanguages;
  }

  get availableThemes(): Theme[] {
    return this.themeService.availableThemes;
  }

  get clientid(): string {
    return this.ipcService.clientid;
  }

  isRouteActive(route: string) {
    return this.routing.isRouteActive(route);
  }

  onMenuClick() {
    this.sidebarService.nextMenuHiddenSub.next(!this.menuHidden);
  }

  onHome() {
    this.routing.navigate(['/']);
  }

  onDashboard() {
    this.routing.navigate(['/', 'dashboard']);
  }

  onSetLanguage(lang: string) {
    this.languageService.switchLanguage(lang);
  }

  onSwitchTheme(theme: any) {
    this.themeService.switchTheme(theme.name);
  }

  onMute() {
    this.configService.isGlobalMuted = !this.isGlobalMuted;
  }

  onMinimize() {
    this.ipcService.sendMinimize();
  }

  onClose() {
    this.ipcService.sendClose();
  }

  ngOnDestroy(): void {
    this.subs.forEach((sub) => sub.unsubscribe());
  }
}
