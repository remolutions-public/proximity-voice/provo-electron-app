import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderComponent } from './header.component';
import { LogoModule } from '../logo/logo.module';

import { MatTooltipModule } from '@angular/material/tooltip';

const ANGULAR_MATERIAL = [MatTooltipModule];

const COMPONENTS = [HeaderComponent];

const MODULES = [CommonModule, IonicModule, TranslateModule, LogoModule];

const PROVIDERS = [];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES, ...ANGULAR_MATERIAL],
  exports: [...COMPONENTS],
  providers: [...PROVIDERS],
})
export class HeaderModule {}
