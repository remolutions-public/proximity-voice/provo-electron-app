# What is this?
This is a little client GUI to manage the electron app


# How to upgrade angular & ionic
```shell
# you can only update one major version at a time!
# ng update @angular/cli @angular/core --force
ANGULAR_MAJOR_VERSION=16
ng update @angular/cli@$ANGULAR_MAJOR_VERSION @angular/core@$ANGULAR_MAJOR_VERSION --force

# manually upgraded:
# - in dependencies:
"@angular/material": "^16.2.11",
# - in dev dependencies:
"@angular-builders/custom-webpack": "^16.0.1",


# https://ionicframework.com/docs/updating/7-0
npm install rxjs@7.5.3
npm install @ionic/angular@7 @ionic/angular-toolkit@9

```
