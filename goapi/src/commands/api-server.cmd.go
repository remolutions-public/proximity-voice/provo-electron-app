package commands

import (
	"log"
	"provo_api/src/routes"
	"provo_api/src/services/ipc"
	"strconv"
	"time"

	sentrygin "github.com/getsentry/sentry-go/gin"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cobra"
)

func ProvoApiInit() *cobra.Command {
	var port int = 0

	cmdRef := &cobra.Command{
		Use:     "launch",
		PreRun:  executePreRun,
		PostRun: executePostRun,
		Run: func(cmd *cobra.Command, args []string) {
			if port > 0 {
				ApiPort = port
			}

			// register our route modules
			gin.SetMode(gin.ReleaseMode)
			router := gin.New()
			router.Use(gin.Recovery())
			router.Use(cors.New(cors.Config{
				AllowOrigins:     []string{"http://localhost:3471", "http://localhost:4242"},
				AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"},
				AllowHeaders:     []string{"Access-Control-Allow-Origin", "Origin", "Accept", "X-Requested-With", "Content-Type", "Access-Control-Request-Method", "Access-Control-Request-Headers", "Authorization"},
				AllowCredentials: true,
				ExposeHeaders:    []string{"Access-Control-Allow-Origin", "Content-Length"},
				AllowWildcard:    true,
			}))
			// https://chris-se.sentry.io/projects/provo-go-api/getting-started/
			router.Use(sentrygin.New(sentrygin.Options{}))
			routes.RootRoutes(router)
			routes.PlayerRoutes(router)

			// start api process
			go launchHttpServer(router)
			time.Sleep(1 * time.Second)
			ipc.InitListener()
		},
	}

	cmdRef.Flags().IntVar(&port, "port", 0, "api port")

	return cmdRef
}

func launchHttpServer(router *gin.Engine) {
	routerErr := router.Run("0.0.0.0:" + strconv.Itoa(ApiPort))
	if routerErr != nil {
		log.Fatal(routerErr)
	}
}
