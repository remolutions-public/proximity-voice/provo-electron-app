package commands

import (
	"log"
	"os"
	"runtime"

	"github.com/spf13/cobra"
)

var Version string

func InitVersion(newVersion string) {
	if len(Version) == 0 {
		if len(newVersion) == 0 {
			Version = os.Getenv("VERSION")
			if len(Version) == 0 {
				Version = "0.0.0"
			}
		} else {
			Version = newVersion
		}
	}
}

func AppVersion() *cobra.Command {
	cmdRef := &cobra.Command{
		Use:     "version",
		Short:   "v",
		PreRun:  executePreRun,
		PostRun: executePostRun,
		Run: func(cmd *cobra.Command, args []string) {
			log.Printf("version: %s, Go Version: %s", Version, runtime.Version())
		},
	}
	return cmdRef
}
