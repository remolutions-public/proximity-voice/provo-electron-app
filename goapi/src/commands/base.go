package commands

import (
	"github.com/spf13/cobra"
)

var ApiPort int = 3741

var preRunCommands = []cobra.Command{}
var postRunCommands = []cobra.Command{}

var executePreRun = func(cmd *cobra.Command, args []string) {
	for _, c := range preRunCommands {
		c.Run(cmd, args)
	}
}

var executePostRun = func(cmd *cobra.Command, args []string) {
	for _, c := range postRunCommands {
		c.Run(cmd, args)
	}
}

var RootCmd = &cobra.Command{
	Use:     "",
	Short:   "",
	Long:    ``,
	PreRun:  executePreRun,
	PostRun: executePostRun,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Help()
		}
	},
}
