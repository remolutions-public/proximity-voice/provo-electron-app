package ipc

import (
	"bufio"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"time"
)

var OffsetCalculatedRemotePlayers = map[string]interface{}{}
var VoiceBaseSettingsCache = map[string]interface{}{}

func InitListener() {
	for {
		reader := bufio.NewReader(os.Stdin)
		input, _ := reader.ReadString('\n')

		if len(input) > 0 {
			sanitizedStr := strings.Replace(input, "\n", "", -1)
			inputArr := strings.Split(sanitizedStr, ",")
			for _, inputElem := range inputArr {
				if len(inputElem) == 0 {
					continue
				}
				inputBytes, inputErr := base64.StdEncoding.DecodeString(inputElem)
				if inputErr != nil {
					IpcSelfLogger(inputErr.Error(), "error")
					continue
				}
				var inputObj map[string]interface{}
				parseErr := json.Unmarshal(inputBytes, &inputObj)
				if parseErr != nil {
					IpcSelfLogger(inputErr.Error(), "error")
					continue
				}

				var data interface{}
				dataRaw, dataOk := inputObj["data"]
				if dataOk {
					data = dataRaw
				}
				var typeStr string
				typeRaw, typeOk := data.(map[string]interface{})["type"]
				if typeOk {
					typeStr = typeRaw.(string)
				}

				switch typeStr {
				case "apiProcessHealthcheck":
					Send("apiProcessHealthcheck", map[string]interface{}{}, "", "")
					continue
				case "debugRemotePositions":
					if data != nil {
						subDataRaw, subDataOk := data.(map[string]interface{})["data"]
						if subDataOk {
							OffsetCalculatedRemotePlayers = subDataRaw.(map[string]interface{})
						}
					}
					continue
				case "voiceBaseSettingsCache":
					if data != nil {
						VoiceBaseSettingsCache = data.(map[string]interface{})
					}
					continue
				}
			}

		}
	}
}

func Send(channel string, dataObj any, rendererid string, requestid string) bool {
	var payloadData = map[string]interface{}{
		"type":  channel,
		"stamp": time.Now().UnixMilli(),
	}
	if len(rendererid) > 0 {
		payloadData["rendererid"] = rendererid
	}
	if len(requestid) > 0 {
		payloadData["requestid"] = requestid
	}
	if dataObj != nil {
		switch dataVal := dataObj.(type) {
		case []byte:
			var parsedObj interface{}
			parsedErr := json.Unmarshal(dataVal, &parsedObj)
			if parsedErr != nil {
				parsedObj = string(dataVal[:])
			}
			payloadData["data"] = parsedObj
		default:
			payloadData["data"] = dataVal
		}
	}

	payloadBytes, payloadErr := json.Marshal(payloadData)
	if payloadErr != nil {
		os.Stderr.WriteString(payloadErr.Error())
		return false
	}
	fmt.Println("[d]:" + base64.StdEncoding.EncodeToString(payloadBytes))
	return true
}

// this function is only used by the ipc service itself, because it cannot import the logger service due to circular dependency
func IpcSelfLogger(msg any, logType string) {
	if len(logType) == 0 {
		logType = "info"
	}
	var loggerData = map[string]interface{}{
		"type": logType,
	}
	switch msgData := msg.(type) {
	case error:
		loggerData["msg"] = "api: " + msgData.Error()
	case string:
		loggerData["msg"] = "api: " + msgData
	case []byte:
		loggerData["msg"] = "api: " + string(msgData[:])
	default:
		loggerData["msg"] = msgData
	}
	Send("logging", loggerData, "", "")
}
