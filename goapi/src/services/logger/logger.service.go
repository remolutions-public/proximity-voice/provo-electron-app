package logger

import (
	"github.com/labstack/gommon/log"

	"provo_api/src/services/ipc"
)

var LogLevelMap = map[string]log.Lvl{
	"DEBUG": log.DEBUG,
	"INFO":  log.INFO,
	"WARN":  log.WARN,
	"ERROR": log.ERROR,
	"OFF":   log.OFF,
}

func Info(rendererid string, msg any) {
	var loggerData = map[string]interface{}{
		"type": "info",
	}
	switch msgData := msg.(type) {
	case error:
		loggerData["msg"] = "api: " + msgData.Error()
	case string:
		loggerData["msg"] = "api: " + msgData
	case []byte:
		loggerData["msg"] = "api: " + string(msgData[:])
	default:
		loggerData["msg"] = msgData
	}
	ipc.Send("logging", loggerData, rendererid, "")
}

func Log(rendererid string, msg any) {
	Info(rendererid, msg)
}

func Debug(rendererid string, msg any) {
	Info(rendererid, msg)
}

func Warn(rendererid string, msg any) {
	var loggerData = map[string]interface{}{
		"type": "warn",
	}
	switch msgData := msg.(type) {
	case error:
		loggerData["msg"] = "api: " + msgData.Error()
	case string:
		loggerData["msg"] = "api: " + msgData
	case []byte:
		loggerData["msg"] = "api: " + string(msgData[:])
	default:
		loggerData["msg"] = msgData
	}
	ipc.Send("logging", loggerData, rendererid, "")
}

func Error(rendererid string, msg any) {
	var loggerData = map[string]interface{}{
		"type": "error",
	}
	switch msgData := msg.(type) {
	case error:
		loggerData["msg"] = "api: " + msgData.Error()
	case string:
		loggerData["msg"] = "api: " + msgData
	case []byte:
		loggerData["msg"] = "api: " + string(msgData[:])
	default:
		loggerData["msg"] = msgData
	}
	ipc.Send("logging", loggerData, rendererid, "")
}
