package routes

import (
	"encoding/json"
	"net/http"
	"provo_api/src/services/ipc"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/labstack/gommon/log"
)

func PlayerRoutes(router *gin.Engine) {
	rg := router.Group("player")

	rg.POST("", UpdatePlayerData())
}

func UpdatePlayerData() gin.HandlerFunc {
	return func(c *gin.Context) {
		var bodyData map[string]interface{}
		parseErr := c.Bind(&bodyData)
		if parseErr != nil {
			var errMsg string = "Could not parse request body"
			log.Error(errMsg)
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
				"message": errMsg,
			})
			return
		}
		bodyData["timestamp"] = time.Now().UnixMilli()
		var isDebug bool = false
		isDebugRaw, ok := bodyData["isDebug"]
		if ok {
			isDebug = isDebugRaw.(bool)
		}
		var clientid string = ""
		clientidRaw, ok := bodyData["clientid"]
		if ok {
			clientid = clientidRaw.(string)
		}

		byteData, err := json.Marshal(bodyData)
		if err != nil {
			log.Error(err)
		}

		ipc.Send("PlayerData", byteData, clientid, "")

		if isDebug {
			var responseKey string
			responseKeyRaw, ok := bodyData["responseKey"]
			if ok {
				responseKey = responseKeyRaw.(string)
			}
			var offsets = make([]interface{}, 0)
			for _, val := range ipc.OffsetCalculatedRemotePlayers {
				offsets = append(offsets, val)
			}
			c.JSON(http.StatusOK, gin.H{
				"responseKey":       responseKey,
				"offsets":           offsets,
				"voiceBaseSettings": ipc.VoiceBaseSettingsCache,
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"message": "healthy",
		})
	}
}
