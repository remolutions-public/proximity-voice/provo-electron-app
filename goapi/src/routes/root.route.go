package routes

import (
	"io"
	"net/http"
	"provo_api/src/services/ipc"

	"github.com/gin-gonic/gin"
	"github.com/labstack/gommon/log"
)

func RootRoutes(router *gin.Engine) {
	rg := router.Group("")

	rg.GET("", GetHealthcheck())
	rg.POST("", ReceiveKeyInput())
}

func GetHealthcheck() gin.HandlerFunc {
	return func(c *gin.Context) {
		ipc.Send("healthcheck", nil, "", "")
		c.JSON(http.StatusOK, gin.H{
			"message": "healthy",
		})
	}
}

func ReceiveKeyInput() gin.HandlerFunc {
	return func(c *gin.Context) {
		byteData, err := io.ReadAll(c.Request.Body)
		if err != nil {
			log.Error(err)
		}
		ipc.Send("downKeys", byteData, "", "")
		c.JSON(http.StatusOK, nil)
	}
}
