package main

import (
	"fmt"
	"os"
	"provo_api/src/commands"

	"github.com/getsentry/sentry-go"
	"github.com/labstack/gommon/log"
)

func init() {
	if err := sentry.Init(sentry.ClientOptions{
		Dsn: "https://840a59180dee9138d7efee218c50631b@o4506245893324800.ingest.sentry.io/4506246165823488",
	}); err != nil {
		fmt.Printf("Sentry initialization failed: %v", err)
	}

	commands.InitVersion("0.0.0")
	commands.RootCmd.AddCommand(commands.AppVersion())
	commands.RootCmd.AddCommand(commands.ProvoApiInit())
}

func main() {
	if err := commands.RootCmd.Execute(); err != nil {
		log.Error(err)
		os.Exit(1)
	}
}
