# What is this?
a basic go api that receives the player info from the game client via http calls


# Quick Start; How to get started with golang development?
- in the root directory execute following:
```shell
# if you want to build only
sh goapi/scripts/build.sh

# if you want to build and run the app
sh goapi/scripts/run.sh version
sh goapi/scripts/run.sh launch


# just run the build app
goapi/win_amd64/provo_api version
goapi/win_amd64/provo_api launch

```



# How to track git lfs files
```shell
git lfs track goapi/win_amd64/*
git lfs track goapi/linux_amd64/*
git lfs track goapi/linux_arm64/*

# check if files are lfs tracked:
git lfs ls-files

# when the file is not tracked by git lfs, then remove it, commit and track it again -> after another 'git add .', it should be tracked...


```
