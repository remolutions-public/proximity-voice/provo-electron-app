#!/bin/sh


BASE_PATH=$(dirname \"$0\" | tr -d '"')

APP_NAME=provo_api

cd $BASE_PATH/..


rm -rf win_amd64/$APP_NAME.exe


echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Checking out if you have go installed..."
go version > /dev/null
if [ "$?" != "0" ]; then
    echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Looks like you dont have go installed! Please install it before running the application!"
    exit 1
fi

echo "[$(date '+%Y-%m-%dT%H:%M:%S')] go installation checked!"



echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Downloading dependencies..."
go get
go mod tidy
echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Dependencies downloaded!"



echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Building application..."
env GOOS=windows GOARCH=amd64 go build -o win_amd64/$APP_NAME.exe main.go
if [ "$?" != 0 ]; then
    echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Could not build application! Exiting..."
    exit 1
fi
# if [ "$1" != "--dev-only" ]; then
#     env GOOS=linux GOARCH=amd64 go build -o linux_amd64/$APP_NAME main.go
#     env GOOS=linux GOARCH=arm64 go build -o linux_arm64/$APP_NAME main.go
# fi




echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Application built!"
