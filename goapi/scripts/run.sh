#!/bin/sh



BASE_PATH=$(dirname \"$0\" | tr -d '"')

APP_NAME=provo_api

sh $BASE_PATH/build.sh --dev-only
if [ "$?" != "0" ]; then
  exit 1
fi


echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Running the application..."
$BASE_PATH/.././win_amd64/$APP_NAME $@
echo "[$(date '+%Y-%m-%dT%H:%M:%S')] Application finished running!"
exit 0
