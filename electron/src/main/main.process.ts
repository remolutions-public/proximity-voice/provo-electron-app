import { app } from 'electron';
import { IPCService } from './services/ipc.service';
import { RendererService } from './services/renderer.service';
import { APIService } from './services/api.service';
import { env } from '../environments/environment';
import { debounce } from './libraries/debounce.lib';
import { sleep } from './libraries/sleep.lib';

export class MainProcess {
  private static self: MainProcess;
  private static wasInitDone: boolean = false;
  private app: Electron.App = app;

  private rendererService: RendererService;
  private ipcService: IPCService;
  private apiService: APIService;

  public static getInstance(): MainProcess {
    if (!MainProcess.self) {
      MainProcess.self = new MainProcess();
    }
    return MainProcess.self;
  }

  public init() {
    if (!MainProcess.wasInitDone) {
      MainProcess.wasInitDone = true;

      this.app.commandLine.appendSwitch(
        'autoplay-policy',
        'no-user-gesture-required'
      );
      // https://kapeli.com/cheat_sheets/Chromium_Command_Line_Switches.docset/Contents/Resources/Documents/index
      this.app.commandLine.appendSwitch('disable-volume-adjust-sound', 'true');
      this.app.commandLine.appendSwitch(
        'enable-features',
        'disable-volume-adjust-sound'
      );
      // app.commandLine.appendSwitch('ignore-certificate-errors');
      // this.app.allowRendererProcessReuse = true;
      this.app.on('window-all-closed', () => this.onAllWindowsClosed());
      // this.app.on('before-quit', () => {
      //   console.info('destroying processes...');
      //   this.apiService.killApiProcess();
      //   this.apiService = undefined;
      //   this.ipcService.destroy();
      //   this.ipcService = undefined;
      //   this.rendererService.destroy();
      //   this.rendererService = undefined;
      //   this.app = undefined;
      //   MainProcess.self = undefined;
      // });
      this.app.on('ready', () => {
        setTimeout(() => {
          this.onReady();
        }, 50);
      });

      this.ipcService = IPCService.getInstance();
      this.ipcService.init();

      this.apiService = APIService.getInstance();
      this.apiService.init();

      this.rendererService = RendererService.getInstance();
      this.rendererService.init();
    }
  }

  private onAllWindowsClosed() {
    // IOS should not close, since process management is handled differently
    if (process.platform !== 'darwin') {
      this.app.quit();
      return;
    }
  }

  private async onReady() {
    debounce('openRendererWindows', 250, () => {
      this.openRendererWindows();

      // tslint:disable-next-line: no-console
      console.info('electron main.process is ready!');
    });
  }

  openRendererWindows() {
    debounce(
      'init-rendererService',
      100 * (env.rendererWindows?.length || 1),
      async () => {
        if (!this.apiService.hasApiInitHealthcheckDone) {
          return this.openRendererWindows();
        }

        for (const renderer of env.rendererWindows) {
          this.rendererService.createRenderer(renderer.name, renderer.type);
          this.rendererService.registerRendererEvents(renderer);
          if (renderer.type === 'debug') {
            renderer.intervalRef = setInterval(() => {
              this.ipcService.sendIsDebugWebRenderer(renderer.name);
            }, 500);
          }
          await sleep(1000);
        }
      }
    );
  }
}

export default MainProcess.getInstance();
