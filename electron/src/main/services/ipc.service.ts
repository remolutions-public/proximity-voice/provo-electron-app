import { ipcMain, IpcMain } from 'electron';
import { RendererService } from './renderer.service';
import { env } from '../../environments/environment';
import { APIService } from '../services/api.service';
import os from 'os';

export class IPCService {
  private static self: IPCService;
  private static wasInitDone: boolean = false;
  private static eventWereRegistered = false;

  private rendererService: RendererService;
  get rendererServiceRef(): RendererService {
    this.rendererService = RendererService.getInstance();
    return this.rendererService;
  }
  private apiService = APIService.getInstance();

  ipc: IpcMain = ipcMain;

  hostname: string = os.hostname();

  public static getInstance(): IPCService {
    if (!IPCService.self) {
      IPCService.self = new IPCService();
    }
    return IPCService.self;
  }

  init() {
    if (!IPCService.wasInitDone) {
      IPCService.wasInitDone = true;
      setTimeout(() => {
        this.rendererService = RendererService.getInstance();
      }, 0);
    }
    this.registerIpcEvents();
  }

  sendMessage(obj: any) {
    try {
      this.ipc.emit('main', obj);
    } catch (e) {
      console.error(e);
    }
  }

  sendLog(type: string, rendererid: string, data: any) {
    let renderer = this.rendererServiceRef?.getRenderer(
      env.rendererWindows[0].name
    );
    if (!(rendererid == undefined)) {
      renderer = this.rendererServiceRef.getRendererById(rendererid);
    }
    if (!(renderer?.window?.webContents?.send == undefined)) {
      try {
        renderer.window.webContents.send('log', { type, msg: [data] });
      } catch (e) {
        console.error(e);
      }
    }
  }

  sendMessageToWindow(data: any, rendererid: string, eventName: string) {
    let renderer = this.rendererServiceRef.getRenderer(
      env.rendererWindows[0].name
    );
    if (!(rendererid == undefined)) {
      renderer = this.rendererServiceRef.getRendererById(rendererid);
    }
    if (
      !(renderer?.window?.webContents?.send == undefined) &&
      !(data == undefined)
    ) {
      renderer.window.webContents.send(eventName, data);
    }
  }

  sendIsDebugWebRenderer(name: string) {
    const renderer = this.rendererServiceRef.getRenderer(name);
    if (!(renderer?.window?.webContents?.send == undefined)) {
      try {
        renderer.window.webContents.send('debugRenderer', {
          name,
          type: renderer.type,
        });
      } catch (e) {
        console.error(e);
      }
    }
  }

  registerIpcEvents() {
    if (!IPCService.eventWereRegistered) {
      IPCService.eventWereRegistered = true;

      this.ipc.on('minimize', (event: any) => {
        const window = this.rendererServiceRef.getWebContentsWindow(
          event['sender']
        );
        if (!(window?.webContents?.send == undefined)) {
          window.minimize();
        }
      });

      this.ipc.on('close', (event: any) => {
        const window = this.rendererServiceRef.getWebContentsWindow(
          event['sender']
        );
        if (!(window?.webContents?.send == undefined)) {
          window.close();
          window.destroy();
        }
      });

      this.ipc.on('version', (event: any) => {
        const window = this.rendererServiceRef.getWebContentsWindow(
          event['sender']
        );
        if (!(window?.webContents?.send == undefined)) {
          window.webContents.send('version', {
            version: env.version,
          });
        }
      });

      this.ipc.on('version', (event: any) => {
        const window = this.rendererServiceRef.getWebContentsWindow(
          event['sender']
        );
        if (!(window?.webContents?.send == undefined)) {
          window.webContents.send('version', {
            version: env.version,
          });
        }
      });

      this.ipc.on('retrieveRendererId', (event: any) => {
        const renderer = this.rendererServiceRef.getWebContentsRenderer(
          event['sender']
        );
        const window = renderer?.window;
        if (!(window?.webContents?.send == undefined)) {
          window.webContents.send('rendererid', {
            rendererid: renderer.id,
            renderertype: renderer.type,
          });
        }
      });

      this.ipc.on('apiData', (event: any, msg: any) => {
        // this flow goes to the local client api!
        const renderer = this.rendererServiceRef.getWebContentsRenderer(
          event['sender']
        );
        this.apiService.queueApiMessage(msg, renderer?.id, renderer?.type);
      });
    }
  }

  destroy() {
    this.ipc.removeAllListeners();
    this.ipc = undefined;
    IPCService.self = undefined;
    this.rendererService = undefined;
    this.apiService = undefined;
  }
}
