import { fork, spawn, ChildProcess, StdioOptions } from 'child_process';
import { IPCService } from './ipc.service';
import path from 'path';
import { env } from '../../environments/environment';
import util from 'util';
import { debounce } from '../libraries/debounce.lib';
import kill from 'kill-port';
import { encodeBase64, decodeBase64 } from '../libraries/base64.lib';
import { typeIsString, typeIsJSON } from '../libraries/parser.lib';

export class APIService {
  private static self: APIService;
  private static wasInitDone: boolean = false;

  apiProcess: ChildProcess;
  ipc: IPCService;

  apiProcessHealthcheckTime: number = new Date().valueOf();
  hasApiInitHealthcheckDone = false;
  processHealthTimeout = 20000;

  isStdinPipeUnhealthy: any;

  private payloadQueue: string[] = [];

  constructor() {
    setTimeout(() => {
      this.init();
    }, 100);
  }

  public static getInstance(): APIService {
    if (!APIService.self) {
      APIService.self = new APIService();
    }
    return APIService.self;
  }

  init() {
    if (!APIService.wasInitDone) {
      APIService.wasInitDone = true;
      this.ipc = IPCService.getInstance();
      this.ipc.init();

      let apiBinPath = path.join(
        __dirname,
        '/../../../../goapi/win_amd64/provo_api.exe'
      );
      if (env.isProd || env.isStaging) {
        apiBinPath = path.join(__dirname, '/../../../../api/provo_api.exe');
      }

      try {
        this.apiProcess = spawn(apiBinPath, ['launch'], {
          stdio: ['pipe', 'pipe', 'pipe'],
        });
        this.isStdinPipeUnhealthy = false;

        this.registerGoAPIProcessEvents();
        this.crashRecoveryCheck();
      } catch (e) {
        debounce('reinitApiService', 100, () => {
          APIService.wasInitDone = false;
          this.init();
        });
      }
    }
  }

  registerGoAPIProcessEvents() {
    if (!(this.apiProcess == undefined)) {
      this.apiProcess.stdin.on('close', this.onStdinCloseEvent);
      this.apiProcess.stdin.on('error', this.onStdinErrorEvent);
      this.apiProcess.stdout?.on('data', this.onStdoutDataEvent);
      this.apiProcess.stderr?.on('data', this.onStderrDataEvent);
    }
  }

  onStdinCloseEvent = () => {
    this.isStdinPipeUnhealthy = true;
  };

  onStdinErrorEvent = (err: any) => {
    this.isStdinPipeUnhealthy = true;
  };

  onStderrDataEvent = (err: any) => {
    if (this.ipc == undefined) {
      this.ipc = IPCService.getInstance();
    }
    let errMsg = util.inspect(err);
    try {
      errMsg = util.inspect(String(err));
    } catch (e) {
      console.error(e);
    }
    this.ipc.sendLog('error', undefined, 'local api process error: ' + errMsg);
  };

  onStdoutDataEvent = (data: any) => {
    let msgObj = String(data);
    if (typeIsString(msgObj) && msgObj.startsWith('[d]:')) {
      const msgArr = msgObj.split('\n').filter((val: string) => val?.length > 0);
      for (const msgEntry of msgArr) {
        let handleMsg: any;
        const decodedMsg = decodeBase64(msgEntry.replace(/\[d\]:/, ''));
        if (typeIsJSON(decodedMsg)) {
          try {
            handleMsg = JSON.parse(decodedMsg);
          } catch (e) {
            this.ipc.sendLog(
              'error',
              undefined,
              'api json parse error: ' + util.inspect(e)
            );
            return;
          }
        }
        this.handleProcessEventData(handleMsg);
      }
    } else {
      if (this.ipc == undefined) {
        this.ipc = IPCService.getInstance();
      }
      this.ipc.sendLog(
        'warn',
        undefined,
        'local api process non data stdout event: ' + util.inspect(msgObj)
      );
    }
  };

  handleProcessEventData(msgObj: any) {
    if (msgObj == undefined) {
      return;
    }
    switch (msgObj.type) {
      case 'error':
        this.ipc.sendLog(
          'error',
          msgObj.rendererid,
          'electron: ' + JSON.stringify(msgObj)
        );
        break;
      case 'logging':
        const msg = msgObj.data?.msg || JSON.stringify(msgObj);
        this.ipc.sendLog(msgObj.type || 'info', msgObj.rendererid, msg);
      case 'apiProcessHealthcheck':
        this.apiProcessHealthcheckTime = new Date().valueOf();
        this.hasApiInitHealthcheckDone = true;
        if (!(msgObj?.data == undefined)) {
          // msgObj.data.cpuUsage = process.cpuUsage();
          msgObj.data.cpu = process.getCPUUsage();
          // msgObj.data.memory = process.memoryUsage();
          msgObj.data.memory = process.getSystemMemoryInfo();
          msgObj.data.uptime = process.uptime();
          // msgObj.data.resources = process.resourceUsage();
          // msgObj.data.io = process.getIOCounters();
          msgObj.data.osVersion = process.getSystemVersion();
        }
        this.ipc.sendMessageToWindow(msgObj, msgObj.rendererid, msgObj.type);
        break;
      default:
        this.ipc.sendMessageToWindow(msgObj, msgObj.rendererid, msgObj.type);
    }
  }

  async crashRecoveryCheck() {
    debounce('crashRecoveryCheck', 2000, async () => {
      this.queueApiMessage(
        { type: 'apiProcessHealthcheck' },
        undefined,
        undefined
      );
      if (this.apiProcessHealthcheckTime == undefined) {
        this.apiProcessHealthcheckTime = new Date().valueOf();
      }
      if (
        new Date().valueOf() - this.apiProcessHealthcheckTime >
          this.processHealthTimeout ||
        !(this.apiProcess.exitCode == undefined)
      ) {
        APIService.wasInitDone = false;
        this.apiProcess.stdin?.off('close', this.onStdinCloseEvent);
        this.apiProcess.stdin?.off('error', this.onStdinErrorEvent);
        this.apiProcess.stdout?.off('data', this.onStdoutDataEvent);
        this.apiProcess.stderr?.off('data', this.onStderrDataEvent);
        try {
          this.ipc.sendLog(
            'warn',
            undefined,
            'local api process crashed, restarting...'
          );
          await this.killApiProcess();
          this.apiProcessHealthcheckTime = new Date().valueOf();
        } catch (e) {
          console.error(e);
        }
        this.init();
      }
      this.crashRecoveryCheck();
    });
  }

  async killApiProcess() {
    this.apiProcess.kill();
    await kill(3741, 'tcp').catch();
  }

  queueApiMessage(data: any, rendererid: string, renderertype: string) {
    if (
      this.apiProcess?.stdin == undefined ||
      this.apiProcess?.stdin?.closed ||
      this.isStdinPipeUnhealthy
    ) {
      return;
    }
    if (!(data == undefined)) {
      const payload = encodeBase64(
        JSON.stringify({ data, rendererid, renderertype })
      );
      this.payloadQueue.push(payload);
      this.sendQueuedApiMessages();
    }
  }

  sendQueuedApiMessages(breakcounter?: number) {
    if (!this.payloadQueue?.length) {
      return;
    }
    try {
      // need to debounce to catch cases where node.js does try to simultanously write to stdin with its 8 event loop threads
      debounce('sendQueuedApiMessages', 5, () => {
        if (
          this.apiProcess?.stdin == undefined ||
          this.apiProcess?.stdin?.closed ||
          this.isStdinPipeUnhealthy
        ) {
          return;
        }

        if (
          this.apiProcess.stdin.writable &&
          !this.apiProcess.stdin.writableCorked
        ) {
          this.apiProcess.stdin.cork();
          this.apiProcess.stdin.write(
            this.payloadQueue.join(',') + '\n',
            (err) => {
              if (!(err == undefined)) {
                console.error(err);
              }
            }
          );
          this.apiProcess.stdin.uncork();
          this.payloadQueue = [];
        } else {
          if (breakcounter == undefined) {
            breakcounter = 0;
          } else if (breakcounter > 10) {
            return;
          }
          // retry
          this.sendQueuedApiMessages(breakcounter++);
        }
      });
    } catch (e) {
      console.error(e);
    }
  }
}

export default APIService.getInstance();
