import { screen, BrowserWindow, WebContents } from 'electron';
import { IRenderer } from '../../interfaces/renderer.interface';
import { IPCService } from './ipc.service';
import { FileLogger } from '../../environments/filelogger';

import * as uuid from 'uuid';
import path from 'path';
import { env } from '../../environments/environment';
import os from 'os';

export class RendererService {
  private static self: RendererService;
  private static wasInitDone: boolean = false;

  private rendererObjs: { [name: string]: IRenderer } = {};

  get rendererKeys(): string[] {
    return Object.keys(this.rendererObjs);
  }

  ipc: IPCService;

  public static getInstance(): RendererService {
    if (!RendererService.self) {
      RendererService.self = new RendererService();
    }
    return RendererService.self;
  }

  init() {
    if (!RendererService.wasInitDone) {
      RendererService.wasInitDone = true;
      this.ipc = IPCService.getInstance();
      this.ipc.init();
    }
  }

  ///////////////////// Main Renderer //////////////////////////
  createRenderer(name: string, type: 'main' | 'debug' = 'main') {
    if (this.rendererObjs[name] == undefined) {
      this.rendererObjs[name] = {
        id: uuid.v4(),
        type,
        name,
        window: new BrowserWindow({
          frame: false,
          width: env.isProd && !env.showWindowDebuggingTool ? 400 : 800,
          height: 600,
          icon: path.join(__dirname, '/../../assets/images/favicon.png'),
          webPreferences: {
            nodeIntegration: true,
            backgroundThrottling: false,
            contextIsolation: false,
          },
        }),
      };
      this.setWasOpen(name, true);
      this.setDevelopmentPosition(this.rendererObjs[name]);
      this.loadRendererTarget(this.rendererObjs[name], env.maintarget);
    }
  }

  registerRendererEvents(rendererRef: IRenderer) {
    const window = this.getRenderer(rendererRef.name).window;
    window.on('close', () => {
      if (!(rendererRef.intervalRef == undefined)) {
        clearInterval(rendererRef.intervalRef);
      }
      this.clearRenderer(rendererRef.name);
    });
  }

  setWasOpen(name: string, state: boolean) {
    if (!(this.rendererObjs[name] == undefined)) {
      this.rendererObjs[name]['isOpen'] = state;
    }
  }

  getIsnOpen(name: string): boolean {
    return this.rendererObjs[name]['isOpen'];
  }

  getRenderer(name: string): IRenderer {
    return this.rendererObjs[name];
  }

  getRendererById(rendererid: string): IRenderer {
    for (const name of this.rendererKeys) {
      if (this.rendererObjs[name].id === rendererid) {
        return this.rendererObjs[name];
      }
    }
  }

  clearRenderer(name: string) {
    delete this.rendererObjs[name];
  }

  destroy() {
    for (const renderername of Object.keys(this.rendererObjs)) {
      this.rendererObjs[renderername]?.window?.close();
      this.clearRenderer(renderername);
    }
    RendererService.self = undefined;
    this.ipc = undefined;
  }

  ////////////// HELPER ///////////////
  private loadRendererTarget(renderer: any, target: string) {
    if (!(renderer == undefined)) {
      if (env.isProd || env.isStaging) {
        renderer.window.loadFile(target);
      } else {
        renderer.window.loadURL(target);
      }
    }
  }

  private setDevelopmentPosition(renderer: IRenderer) {
    setTimeout(() => {
      if (!(env.isProd || env.isStaging)) {
        const screens = screen.getAllDisplays();
        const mon = screens[1]; // screens[screens.length - 1];
        renderer.window.setPosition(mon.bounds.x, mon.bounds.y);
        renderer.window.center();
      }
      if (env.showWindowDebuggingTool) {
        renderer.window.webContents.openDevTools();
      }
    }, 0);
  }

  getWebContentsWindow(webContents: WebContents): BrowserWindow {
    const renderer = this.getWebContentsRenderer(webContents);
    if (!(renderer == undefined)) {
      return renderer.window;
    }
  }

  getWebContentsRenderer(webContents: WebContents): IRenderer {
    for (const rendererKey of this.rendererKeys) {
      const renderer = this.getRenderer(rendererKey);
      const window = renderer ? renderer.window : undefined;
      if (!(window == undefined) && window.webContents === webContents) {
        return renderer;
      }
    }
  }

  getWebContentsWindowById(rendererid: string): BrowserWindow {
    for (const name of this.rendererKeys) {
      if (this.rendererObjs[name].id === rendererid) {
        return this.rendererObjs[name].window;
      }
    }
  }
}

export default RendererService.getInstance();
