import { MainProcess } from './main/main.process';
import * as Sentry from "@sentry/electron";
import { env } from './environments/environment';

if (env.isProd || env.isStaging) {
  Sentry.init({
    dsn: "https://05a06f78b2a6a5f41011c8e51574b780@o4506245893324800.ingest.sentry.io/4506246068830208",
  });
}

process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = String(!(env.isProd || env.isStaging));
MainProcess.getInstance().init();
