export const env: any = {
  isProd: true,
  isStaging: false,
  version: 'v0.0.0',
  maintarget: './www/index.html',
  showWindowDebuggingTool: false,
  rendererWindows: [
    {
      name: 'main',
      type: 'main'
    }
  ]
};
