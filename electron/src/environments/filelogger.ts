import fs from 'fs';
import path from 'path';

export class FileLogger {
  constructor(
    private readonly location: string,
    private readonly filename?: string,
  ) { }

  log(msg: string) {
    this.info(msg);
  }

  info(msg: string) {
    const file = this.getFile('/' + (this.filename ? this.filename + '.' : '') + 'stdout.log');
    if (file) file.write(msg + '\n');
  }

  error(msg: string) {
    const file = this.getFile('/' + (this.filename ? this.filename + '.' : '') + 'stderr.log');
    if (file) file.write(msg + '\n');
  }

  private getFile(filename: string): fs.WriteStream {
    const fileLoc = path.join(this.location, filename);
    try {
      return fs.createWriteStream(fileLoc, { flags: 'a' });
    } catch (e) {
      return;
    }
  }
}
