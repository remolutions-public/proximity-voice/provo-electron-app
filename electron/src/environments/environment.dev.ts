export const env: any = {
  isProd: false,
  isStaging: false,
  version: 'v0.0.0',
  maintarget: 'http://localhost:4242',
  showWindowDebuggingTool: true,
  rendererWindows: [
    {
      name: 'main',
      type: 'main'
    },
    // {
    //   name: 'debug1',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug2',
    //   type: 'debug'
    // },
    {
      name: 'debug3',
      type: 'debug'
    },
    // {
    //   name: 'debug4',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug5',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug6',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug7',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug8',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug9',
    //   type: 'debug'
    // },
    // {
    //   name: 'debug10',
    //   type: 'debug'
    // }
  ]
};
