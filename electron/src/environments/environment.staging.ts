export const env: any = {
  isProd: false,
  isStaging: true,
  version: 'v0.0.0',
  maintarget: './www/index.html',
  showWindowDebuggingTool: true,
  rendererWindows: [
    {
      name: 'main',
      type: 'main'
    }
  ]
};
