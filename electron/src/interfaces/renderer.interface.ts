import { BrowserWindow } from 'electron';

export interface IRenderer {
  id: string;
  type: string;
  name: string;
  window: BrowserWindow;
  intervalRef?: any;
}
