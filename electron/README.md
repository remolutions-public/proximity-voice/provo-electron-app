# How to sign the installer/executable
- https://www.funtoimagine.com/blog/electron-windows-code-signing/
- you need to get a certificate and prepopulate following env vars
```shell
# This should contain the path of the .pfx file you downloaded.
CSC_LINK='Cert:\CurrentUser\My'
# This should have the .pfx password you configured while downloading from IE.
CSC_PASSWORD=


# sign installer after release build
# signtool sign /f "{signing}.pfx" /p {PASSWORD} "{App}.exe"
signtool sign "Provo Setup 0.3.2 (STAGING).exe"

```



# Electron Versions with Chromium Version mapping
- https://github.com/electron/releases
- https://www.theregister.com/2022/07/05/chrome_webrtc_zero_day/
