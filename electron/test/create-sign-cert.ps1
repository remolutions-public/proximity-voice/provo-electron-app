# $cert = New-SelfSignedCertificate -Type Custom -Subject "CN=Impulse,DC=remolutions,DC=com" -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.2","2.5.29.17={text}upn=impulse@remolutions.com") -KeyAlgorithm RSA -KeyLength 2048 -SmimeCapabilities -CertStoreLocation "Cert:\CurrentUser\My"

$cert = New-SelfSignedCertificate -DNSName "remolutions.com" -CertStoreLocation "Cert:\CurrentUser\My" -Type CodeSigningCert -Subject "Example Code Signing Certificate"


$CertPassword = ConvertTo-SecureString -String "test" -Force -AsPlainText


# https://stackoverflow.com/questions/69388954/create-a-code-signing-certificate-for-electron-and-add-it-to-nsis-installer
# Export-PfxCertificate -Cert "Cert:\CurrentUser\My\$($cert.Thumbprint)" -FilePath ".\Cert$certificate.pfx" -Password $CertPassword
Export-PfxCertificate -Cert "cert:\CurrentUser\My\$($cert.Thumbprint)" -FilePath ".\certificate.pfx" -Password $CertPassword


pause
