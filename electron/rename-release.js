const path = require('path');
const fs = require('fs');
const electronPackageJson = require('./package.json');

const version = electronPackageJson.version;
const projectName = electronPackageJson.build.productName;
const prefix = projectName + ' Setup ';

const env = process.argv[2]?.toLowerCase();
if (env == undefined) {
  console.error('no environment param provided!');
  return;
}

if (env !== 'staging') {
  console.info('renaming output skipped!');
  return;
}

const envSuffix = ' (' + env.toUpperCase() + ')';
const installerFile = path.join(__dirname, 'dist', prefix + version + '.exe');


if (fs.existsSync(installerFile)) {
  const newFileName = prefix + version + envSuffix + '.exe'
  const newFilePath = path.join(__dirname, 'dist', newFileName);
  if (fs.existsSync(newFilePath)) {
    fs.unlinkSync(newFilePath);
  }
  fs.renameSync(installerFile, newFilePath);
}


console.info('renamed output!');
